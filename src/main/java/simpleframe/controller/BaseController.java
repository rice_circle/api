package simpleframe.controller;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@Controller("baseController")
@RequestMapping("/")
public class BaseController {

	/** "验证结果"参数名称 */
	private static final String CONSTRAINT_VIOLATIONS_ATTRIBUTE_NAME = "constraintViolations";

	@Resource(name = "validator")
	private Validator validator;

	@RequestMapping(value = "index", method = RequestMethod.GET)
	private String index() {
		return "index";
	}

	/**
	 * 数据验证
	 * 
	 * @param target 验证对象
	 * @param groups 验证组
	 * @return 验证结果
	 */
	protected boolean isValid(Object target, Class<?>... groups) {
		Set<ConstraintViolation<Object>> constraintViolations = getValidResult(target, groups);
		if (constraintViolations.isEmpty()) {
			return true;
		} else {
			RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
			requestAttributes.setAttribute(CONSTRAINT_VIOLATIONS_ATTRIBUTE_NAME, constraintViolations, RequestAttributes.SCOPE_REQUEST);
			return false;
		}
	}

	/**
	 * 数据验证
	 * 
	 * @param target 验证对象
	 * @param groups 验证组
	 * @return 验证结果
	 */
	protected Set<ConstraintViolation<Object>> getValidResult(Object target, Class<?>... groups) {
		return getValidResult(target, null, groups);
	}

	/**
	 * 数据验证
	 * 
	 * @param target 验证对象
	 * @param groups 验证组
	 * @return 验证结果
	 */
	protected Set<ConstraintViolation<Object>> getValidResult(Object target, String[] properties, Class<?>... groups) {
		Set<ConstraintViolation<Object>> constraintViolations = new HashSet<ConstraintViolation<Object>>();
		if (properties != null && properties.length > 0) {
			for (String property : properties) {
				constraintViolations.addAll(validator.validateProperty(target, property, groups));
			}
		} else {
			constraintViolations = validator.validate(target, groups);
		}
		return constraintViolations;
	}

	/**
	 * 获取验证结果的第一条提示信息
	 * @param target
	 * @param properties
	 * @param groups
	 * @return
	 */
	protected String getValidFirstMessage(Object target, String[] properties, Class<?>... groups) {
		Set<ConstraintViolation<Object>> constraintViolations = getValidResult(target, properties, groups);
		String message = null;
		if (!constraintViolations.isEmpty()) {

			for (ConstraintViolation<Object> constraintViolation : constraintViolations) {
				// String modelName = target.getClass().getSimpleName();
				// String propertyName = constraintViolation.getPropertyPath().toString();
				// String annotationName = constraintViolation.getConstraintDescriptor().getAnnotation().annotationType().getSimpleName();
				message = constraintViolation.getMessage();
				break;
			}
		}
		return message;
	}
}
