package simpleframe.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alipay.api.internal.util.AlipaySignature;

import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.PayType;
import simpleframe.service.PayService;
import simpleframe.utils.StringUtils;

/**
 * 支付controller
 * @author sunjichang
 * @date 2017年7月17日下午9:39:06
 * @version 1.0
 * @upate 
 */
@Controller("payContoller")
@RequestMapping("/api/pay")
public class PayController extends BaseController{
	private static final Logger logger = Logger.getLogger(PayController.class);
	@Autowired
	private PayService payService;
	
	/**
	 * 获取订单签名信息
	 * @param orderId 订单id
	 * @return
	 */
	@RequestMapping("/obtainOrderDefaultSignInfo")
	@ResponseBody
	public OutApiModel obtainOrderDefaultSignInfo(Integer orderId){
		return obtainOrderSignInfo(PayType.ALIPAY.getText(), orderId);
	}
	
	/**
	 * 获取订单签名信息
	 * @param payType 支付类型(微信、支付宝等)
	 * @param orderId 订单id
	 * @return
	 */
	@RequestMapping("/obtainOrderSignInfo")
	@ResponseBody
	public OutApiModel obtainOrderSignInfo(String payType,Integer orderId){
		OutApiModel model = new OutApiModel();
		if(StringUtils.isBlank(payType)){
			model.init(false, OutApiCode.API_020500002, null);
			return model;
		}
		String data = payService.obtainOrderSingInfo(payType, orderId);
		model.init(false, OutApiCode.API_0000000, data);
		return model;
	}
	
	/**
	 * 支付宝支付异步回调
	 * @param request
	 */
	@RequestMapping("/alipayNotify")
	public String alipayNotify(HttpServletRequest request){
		logger.log(Level.DEBUG, "alipayNotify");
		return payService.verifyAlipayNotify(request);
	}
}
