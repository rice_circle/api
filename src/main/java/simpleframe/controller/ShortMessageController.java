package simpleframe.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import simpleframe.config.PropertyPlaceholder;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.DeleteType;
import simpleframe.model.base.ShortMessageType;
import simpleframe.model.core.Member;
import simpleframe.model.core.ShortMessage;
import simpleframe.model.result.ShortMessageResult;
import simpleframe.service.MemberService;
import simpleframe.service.ShortMessageService;
import simpleframe.utils.StringUtils;

/**
 * 短信controller
 * @author sunjichang
 * @date 2017年7月15日下午8:28:21
 * @version 1.0
 * @upate 
 */
@Controller("shortMessageContoller")
@RequestMapping("/api/shortMessage")
public class ShortMessageController extends BaseController{
	private static final Logger logger = Logger.getLogger(ShortMessageController.class);
	@Autowired
    private ShortMessageService shortMessageService;
	
	@Autowired
    private MemberService memberService;
	
	/**
	 * 发送验证码
	 * @param phone 手机号
	 * @param type 验证码类型
	 * @return
	 */
	@RequestMapping("/sendVerifyCode")
	@ResponseBody
	public OutApiModel sendVerifyCode(String phone,Integer type){
		logger.debug("sendVerifyCode phone="+phone);
//		return null;
		OutApiModel result = new OutApiModel();
		if(StringUtils.isBlank(phone)){
			result.init(false, OutApiCode.API_0000002, null);
			return result;
		}
		//如果是发送注册验证码，先判断当前手机号对应的账号是否已存在
		if(type == ShortMessageType.REGISTER.getValue()){
			Member model = new Member();
			model.setMobile(phone);
			model.setIsDeleted(DeleteType.NORMAL.getValue());
			model = memberService.selectModel(model);
			if(null != model){
				result.init(false, OutApiCode.API_0203000);
				return result;
			}
		}else if(type == ShortMessageType.UPDATE_PASSWORD.getValue()){
			Member model = new Member();
			model.setMobile(phone);
			model.setIsDeleted(DeleteType.NORMAL.getValue());
			model = memberService.selectModel(model);
			if(null == model){
				result.init(false, OutApiCode.API_0203001);
				return result;
			}
		}
		ShortMessage msg = shortMessageService.sendShortMessage(phone, type);
		if(msg == null){
			result.init(false, OutApiCode.API_0000001, null);
			return result;
		}
		if(StringUtils.isBlank(msg.getContent())){
			result.init(false, OutApiCode.API_0102008);
			return result;
		}
		ShortMessageResult msgResult = new ShortMessageResult();
		msgResult.setMobile(msg.getMobile());
		msgResult.setVerifyCode(msg.getContent());
		msgResult.setValidateActiveTime(Integer.valueOf(PropertyPlaceholder.getProperty("sms.effective.time").toString()) * 60);
		result.init(true, OutApiCode.API_0000000, msgResult);
		return result;
	}
}
