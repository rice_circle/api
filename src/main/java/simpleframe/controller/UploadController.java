package simpleframe.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.third.qiniu.QiniuUploadManager;
import simpleframe.utils.StringUtils;

/**
 * 图片、文件等上传管理
 * @author sunjichang
 * @date 2017年8月1日下午8:56:09
 * @version 1.0
 * @upate 
 */
@Controller("uploadController")
@RequestMapping("/api/upload")
public class UploadController extends BaseController{
	private static final Logger logger = Logger.getLogger(BaseController.class);
	//默认文件格式
	private final String DEFAULT_IMAGE_FORT = "png";
	
	/**
	 * 图片上传
	 * @param request
	 */
	@RequestMapping("/uploadImage")
	@ResponseBody
	public OutApiModel uploadImage(HttpServletRequest request){
		//上传文件的解析器  
        CommonsMultipartResolver mutiparRe=new CommonsMultipartResolver();
        OutApiModel result = new OutApiModel();
        //如果是文件类型的请求 
        if(!mutiparRe.isMultipart(request)){  
        	result.init(true, OutApiCode.API_0000003);
        	return result;
        }
    	MultipartHttpServletRequest mhr = (MultipartHttpServletRequest) request; 
    	//迭代文件所有的文件名称
    	Iterator<String> fileNamesIterator = mhr.getFileNames();
    	ArrayList<String> urlList = new ArrayList<String>();
    	while (fileNamesIterator.hasNext()) {
    		//获取文件
    		MultipartFile file = mhr.getFile(fileNamesIterator.next());
    		//获取文件后缀名
    		String suffix = getFileSuffix(file.getOriginalFilename());
    		try {
				String url = QiniuUploadManager.getInstance().uploadImage(file.getBytes(), suffix);
				logger.log(Level.DEBUG, "url="+url);
				urlList.add(url);
			} catch (IOException e) {
				e.printStackTrace();
			}  
    	}
    	result.init(true, OutApiCode.API_0000000,urlList);
    	return result;
	}
	
	/**
	 * 获取文件后缀名
	 * @param fileName
	 * @return
	 */
	private String getFileSuffix(String fileName){
		if(StringUtils.isBlank(fileName)){
			return DEFAULT_IMAGE_FORT;
		}
		if(fileName.endsWith(".png") || fileName.endsWith(".jpeg") || fileName.endsWith(".jpg")){
			return DEFAULT_IMAGE_FORT;
		}
		return DEFAULT_IMAGE_FORT;
	}
}
