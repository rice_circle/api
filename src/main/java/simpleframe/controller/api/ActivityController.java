package simpleframe.controller.api;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import simpleframe.controller.BaseController;
import simpleframe.controller.ShortMessageController;
import simpleframe.model.api.InApiModel;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.*;
import simpleframe.model.core.*;
import simpleframe.model.param.ActivityParam;
import simpleframe.model.result.ActivityResult;
import simpleframe.service.*;
import simpleframe.utils.EnumUtils;
import simpleframe.utils.PageUtils.PageInfo;

/**
 * Created by Administrator on 2017/7/5.
 */
@Controller("apiActivityController")
@RequestMapping("/api/activity/")
public class ActivityController extends BaseController {
	private static final Logger logger = Logger.getLogger(ActivityController.class);
    @Autowired
    private ActivityService activityService;

    @Autowired
    private AttentionService attentionService;

    @Autowired
    private StarService starService;

    @Autowired
    private ProductService productService;

    @Autowired
    private ClubService clubService;

    /**
     * 获取最新的活动数据
     *
     * @param pageSize     数据条数
     * @param activityType 活动类型
     * @return
     * @author wuxianbing
     */
    @ResponseBody
    @RequestMapping("getLastList")
    public OutApiModel getLastList(Integer pageNum, Integer pageSize, Integer activityType, Integer starId,Integer loginId) {

        OutApiModel result = new OutApiModel();

        if (pageNum == null) {
            pageNum = 1;
        }
        if (pageSize == null) {
            return result.init(OutApiCode.API_0200000);
        }

        if (pageSize > 100) {
            return result.init(OutApiCode.API_0200001);
        }

        ActivityParam param = new ActivityParam();
        param.setLoginId(loginId);
        param.setActivityType(activityType);
        param.setIsDeleted(DeleteType.NORMAL.getValue());
        param.setStarId(starId);
        param.setAuditState(AuditState.AUDITYES.getValue());
        PageInfo<ActivityResult> data = activityService.getLastList(0, pageSize, param);

        if (data == null) {
            return result.init(OutApiCode.API_0000001);
        } else {
            return result.init(true, OutApiCode.API_0000000, data);
        }
    }

    /**
     * 获取活动详情
     *
     * @param activityId 活动Id
     * @return
     * @author wuxianbing
     */
    @ResponseBody
    @RequestMapping("getDetails")
    public OutApiModel getDetails(Integer activityId, Integer loginId) {
        //获取排名
        OutApiModel result = new OutApiModel();
        if (activityId == null) {
            return result.init(OutApiCode.API_0100000);
        }

        //获取活动信息
        ActivityResult data = activityService.getDetails(activityId);
        if (data == null) {
            return result.init(OutApiCode.API_0000001);
        }
 
        //获取活动关注数
        Attention paramAttention = new Attention();
        paramAttention.setRelationType(AttentionRelationType.STAR.getValue());
        paramAttention.setRelationId(activityId);
        paramAttention.setIsEnabled(AttentionEnabled.YES.getValue());
        int activityAttentionNum = attentionService.selectCount(paramAttention);
        data.setActivityAttentionNum(activityAttentionNum);

        //获取粉丝圈关注数
        paramAttention.setRelationType(AttentionRelationType.CLUB.getValue());
        paramAttention.setRelationId(data.getClubId());
        paramAttention.setIsEnabled(AttentionEnabled.YES.getValue());
        int clubAttentionNum = attentionService.selectCount(paramAttention);
        data.setClubAttentionNum(clubAttentionNum);

        //获取用户是否关注活动
        if (loginId != null) {
            paramAttention.setMemberId(loginId);
            paramAttention.setRelationType(AttentionRelationType.ACTIVITY.getValue());
            paramAttention.setRelationId(data.getId());
            paramAttention.setIsEnabled(AttentionEnabled.YES.getValue());
            int memberAttentionNum = attentionService.selectCount(paramAttention);
            AttentionEnabled attentionEnabled = memberAttentionNum > 0 ? AttentionEnabled.YES : AttentionEnabled.NO;
            data.setAttentioned(attentionEnabled.getValue());
        }

        //获取关联明星
        List<Star> relationStar = starService.getRelationList(data.getStarId());
        data.setRelationStar(relationStar);

        //获取关联商品
        Product paramProduct = new Product();
        paramProduct.setActivityId(activityId);
        paramProduct.setIsDeleted(DeleteType.NORMAL.getValue());
        paramProduct.setOrder("price");
        List<Product> productList = productService.selectList(paramProduct);
        data.setProductList(productList);

        return result.init(true, OutApiCode.API_0000000, data);
    }

    /**
     * 发布活动
     * @param param 活动详情参数
     * @param loginId 用户id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "addAcitivity",method= RequestMethod.POST)
    public OutApiModel addAcitivity(ActivityParam param, Integer loginId) {
        OutApiModel result = new OutApiModel();
        if (loginId == null) {
            return result.init(OutApiCode.API_0215000);
        }

        if (param.getStarId() == null) {
            return result.init(OutApiCode.API_0215001);
        }

        if (StringUtils.isBlank(param.getName())) {
            return result.init(OutApiCode.API_0215002);
        }

        if (StringUtils.isBlank(param.getCover())) {
            return result.init(OutApiCode.API_0215003);
        }

        if (StringUtils.isBlank(param.getDetails())) {
            return result.init(OutApiCode.API_0215004);
        }

        if (param.getActivityType() == null) {
            return result.init(OutApiCode.API_0215005);
        }

        Activity model = new Activity();
        ActivityType type = EnumUtils.getEnumItem(ActivityType.class, param.getActivityType());
        if (param.getActivityType().equals(ActivityType.YINGYUAN) || param.getActivityType().equals(ActivityType.GONGYI)) {
            if (param.getTargetMoney() == null) {
                return result.init(OutApiCode.API_0215006);
            }

            if (param.getStartTime() == null) {
                return result.init(OutApiCode.API_0215007);
            }

            if (param.getEndTime() == null) {
                return result.init(OutApiCode.API_0215008);
            }

            model.setTargetMoney(param.getTargetMoney());
            model.setStartTime(param.getStartTime());
            model.setEndTime(param.getEndTime());
        }
        Club paramClub = new Club();
        paramClub.setMemberId(loginId);
        paramClub.setAuditState(AuditState.AUDITYES.getValue());
        Club club = clubService.selectModel(paramClub);

        if(club==null)
        {
            return result.init(OutApiCode.API_0215013);
        }

        model.setMemberId(loginId);
        model.setClubId(club.getId());
        model.setStarId(param.getStarId());
        model.setName(param.getName());
        model.setCover(param.getCover());
        model.setDetails(param.getDetails());
        model.setAttentionNum(0);
        model.setPurchaseNum(0);
        model.setActivityType(param.getActivityType());
        model.setRaisedMoney(BigDecimal.valueOf(0));

        Activity data = activityService.insertReturnModel(model);
        if (data == null) {
            return result.init(OutApiCode.API_0000001);
        }

        if (param.getProductList() != null&& !param.getProductList().isEmpty()) {
            for (Product product : param.getProductList()) {
                if (StringUtils.isBlank(product.getName())) {
                    return result.init(OutApiCode.API_0215009);
                }
                if (product.getPrice() == null || product.getPrice().doubleValue() < 0) {
                    return result.init(OutApiCode.API_0215010);
                }

                if (product.getTotalStock() == null || product.getTotalStock() < 0) {
                    return result.init(OutApiCode.API_0215011);
                }

                if (StringUtils.isBlank(product.getImage())) {
                    return result.init(OutApiCode.API_0215012);
                }

                product.setMemberId(loginId);
                product.setClubId(club.getId());
                product.setActivityId(data.getId());

                product.setUsableStock(product.getTotalStock());
                product.setLockStock(0);

                boolean productResult = productService.insert(product);

                if (!productResult) {
                    return result.init(OutApiCode.API_0000001);
                }
            }
        }

        return result.init(true, OutApiCode.API_0000000, data);
    }
    
    /**
     * 发布活动
     * @param param 活动详情参数
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "addActivityJson",consumes = "application/json",method= RequestMethod.POST)
    public OutApiModel addAcitivityJson(@RequestBody InApiModel<ActivityParam> inParam) {
        OutApiModel result = new OutApiModel();
        ActivityParam param = inParam.getData();
        if(param == null){
        	return result.init(OutApiCode.API_0215014);
        }
        if (param.getLoginId() == null) {
            return result.init(OutApiCode.API_0215000);
        }

        if (param.getStarId() == null) {
            return result.init(OutApiCode.API_0215001);
        }

        if (StringUtils.isBlank(param.getName())) {
            return result.init(OutApiCode.API_0215002);
        }

        if (StringUtils.isBlank(param.getCover())) {
            return result.init(OutApiCode.API_0215003);
        }

        if (StringUtils.isBlank(param.getDetails())) {
            return result.init(OutApiCode.API_0215004);
        }

        if (param.getActivityType() == null) {
            return result.init(OutApiCode.API_0215005);
        }

        Activity model = new Activity();
        int activityType = param.getActivityType();
        if (activityType==ActivityType.YINGYUAN.getValue() ||activityType==ActivityType.GONGYI.getValue()) {
            if (param.getTargetMoney() == null) {
                return result.init(OutApiCode.API_0215006);
            }

            if (param.getStartTime() == null) {
                return result.init(OutApiCode.API_0215007);
            }

            if (param.getEndTime() == null) {
                return result.init(OutApiCode.API_0215008);
            }

            model.setTargetMoney(param.getTargetMoney());
            model.setStartTime(param.getStartTime());
            model.setEndTime(param.getEndTime());
        }
        Club paramClub = new Club();
        paramClub.setMemberId(param.getLoginId());
        paramClub.setAuditState(AuditState.AUDITYES.getValue());
        Club club = clubService.selectModel(paramClub);

        if(club==null) {
            return result.init(OutApiCode.API_0215013);
        }

        model.setMemberId(param.getLoginId());
        model.setClubId(club.getId());
        model.setStarId(param.getStarId());
        model.setName(param.getName());
        model.setCover(param.getCover());
        model.setDetails(param.getDetails());
        model.setAttentionNum(0);
        model.setPurchaseNum(0);
        model.setActivityType(param.getActivityType());
        model.setRaisedMoney(BigDecimal.valueOf(0));
        model.setAuditState(AuditState.AUDITING.getValue());
        Activity data = activityService.insertReturnModel(model);
        if (data == null) {
            return result.init(OutApiCode.API_0000001);
        }

        if (param.getProductList() != null&& !param.getProductList().isEmpty()) {
            for (Product product : param.getProductList()) {
                if (StringUtils.isBlank(product.getName())) {
                    return result.init(OutApiCode.API_0215009);
                }
                if (product.getPrice() == null || product.getPrice().doubleValue() < 0) {
                    return result.init(OutApiCode.API_0215010);
                }

                if (product.getTotalStock() == null || product.getTotalStock() < 0) {
                    return result.init(OutApiCode.API_0215011);
                }

                if (StringUtils.isBlank(product.getImage())) {
                    return result.init(OutApiCode.API_0215012);
                }

                product.setMemberId(param.getLoginId());
                product.setClubId(club.getId());
                product.setActivityId(data.getId());

                product.setUsableStock(product.getTotalStock());
                product.setLockStock(0);

                boolean productResult = productService.insert(product);

                if (!productResult) {
                    return result.init(OutApiCode.API_0000001);
                }
            }
        }

        return result.init(true, OutApiCode.API_0000000, data);
    }
    
    /**
     * 获取发布的活动列表
     * @param inParam
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "getPublishActivityList",consumes = "application/json",method= RequestMethod.POST)
    public OutApiModel getPublishActivityList(@RequestBody InApiModel<ActivityParam> inParam){
    	OutApiModel result = new OutApiModel();
        ActivityParam param = inParam.getData();
        Integer loginId = param.getLoginId();
        Integer type = param.getActivityType();
        logger.log(Level.DEBUG, "type="+type);
        if(loginId == null){
        	return result.init(OutApiCode.API_0213000);
        }  
        param.setIsDeleted(DeleteType.NORMAL.getValue());
        param.setMemberId(loginId);
        PageInfo<ActivityResult> data = activityService.getPublishActivityList(param.getPageNum(), param.getPageSize(), param);
        return result.init(true, OutApiCode.API_0000000, data);
    }
}
