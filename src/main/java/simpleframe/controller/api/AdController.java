package simpleframe.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import simpleframe.controller.BaseController;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.core.Ad;
import simpleframe.service.AdService;
import simpleframe.utils.StringUtils;

import java.util.List;

/**
 * Created by Administrator on 2017/7/5.
 */
@Controller("apiAdController")
@RequestMapping("/api/ad/")
public class AdController extends BaseController {

    @Autowired
    private AdService adService;

    /**
     * 根据广告位获取最新的广告
     *
     * @param adPosition 广告位
     * @return
     * @author wuxianbing
     */
    @ResponseBody
    @RequestMapping("getAdByPosition")
    public OutApiModel getAdByPosition(String adPosition) {
        OutApiModel result = new OutApiModel();

        if (StringUtils.isBlank(adPosition)) {
            return result.init(false, OutApiCode.API_0101000, null);
        }

        Ad ad = new Ad();
        ad.setAdPosition(adPosition);
        List<Ad> data = adService.selectList(ad);
        if (data == null) {
            return result.init(false, OutApiCode.API_0000001, null);
        } else {
            return result.init(true, OutApiCode.API_0000000, data);
        }
    }
}
