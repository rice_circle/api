package simpleframe.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import simpleframe.controller.BaseController;
import simpleframe.model.base.BaseModel;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Star;
import simpleframe.service.StarService;

/**
 * Created by Administrator on 2017/7/5.
 */
@Controller("apiContoller")
@RequestMapping("/api/") 
public class ApiContoller extends BaseController {

    @Autowired
    private StarService starService;

    @ResponseBody
    @RequestMapping("/get")
    public Star getAdmin()
    {
    	
    	Star model=new Star();
		model.setName("我去2");

		String message=getValidFirstMessage(model,null,BaseModel.Save.class);

    	int count=starService.selectCount(model);
    	
    	Star star1=starService.select(1);
    	List<Star> modelList=starService.selectCertainList(1,2);
     	
    	modelList=null;
    	modelList=starService.selectList(model);

    	PageInfo<Star> pageStar=starService.selectPage(model, 1, 10);

    	boolean resultExists=starService.selectExists(model);

    	model=starService.selectModel(model);

    	model.setTeamId(1);
    	boolean resultUpdate1=starService.update(model);
    	
    	model.setTeamId(null);
    	boolean resultUpdate2=starService.update(model);
    	
    	model.setCover("cover2");
    	model.setName(null);
    	boolean resultUpdateTrim=starService.updateTrim(model);
    	 
    	model.setId(null);
    	model.setName("我去2");
    	boolean resultInsert=starService.insert(model);
    	
    	model.setId(null);
    	model.setName("我去3");
    	Star star=starService.insertReturnModel(model);

		boolean resultDeleteSoft = starService.deleteSoft(2);
		boolean resultDeleteHard = starService.deleteHard(2);

		return null;
    }
}
