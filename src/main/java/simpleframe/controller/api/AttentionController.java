/*
 * 天虹商场股份有限公司版权所有.
 */
package simpleframe.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.AttentionEnabled;
import simpleframe.model.core.Attention;
import simpleframe.service.AttentionService;

/**
 * 关注接口
 *
 * @author:wuxianbing
 * @date:2017/7/28
 */
@Controller("apiAttentionController")
@RequestMapping("/api/attention/")
public class AttentionController {

    @Autowired
    private AttentionService attentionService;

    @ResponseBody
    @RequestMapping("changeAttention")
    public OutApiModel changeAttention(Integer loginId, Integer relationType, Integer relationId) {
        OutApiModel result = new OutApiModel();

        if (loginId == null) {
            return result.init(false, OutApiCode.API_0210000);
        }
        if (relationType == null) {
            return result.init(false, OutApiCode.API_0210001);
        }
        if (relationId == null) {
            return result.init(false, OutApiCode.API_0210002);
        }

        Attention model = new Attention();
        model.setMemberId(loginId);
        model.setRelationId(relationId);
        model.setRelationType(relationType);

        model = attentionService.selectModel(model);

        Attention data = null;
        if (model != null) {//有数据就切换关注
            model.setIsEnabled(model.getIsEnabled()==AttentionEnabled.YES.getValue()?AttentionEnabled.NO.getValue():AttentionEnabled.YES.getValue());
            data = attentionService.updateReturnModel(model);
        }
        else {//没有数据就添加关注
        	model = new Attention();
            model.setIsEnabled(AttentionEnabled.YES.getValue());
            model.setMemberId(loginId);
            model.setRelationId(relationId);
            model.setRelationType(relationType);
            data = attentionService.insertReturnModel(model);
        }
        if (data == null) {
            return result.init(false, OutApiCode.API_0000001, model);
        }
        return result.init(true, OutApiCode.API_0000000, model);
    }
}
