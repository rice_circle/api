package simpleframe.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import simpleframe.controller.BaseController;
import simpleframe.model.api.InApiModel;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.AuditState;
import simpleframe.model.base.OfficialType;
import simpleframe.model.core.Club;
import simpleframe.model.param.ClubParam;
import simpleframe.model.param.LoginIdParam;
import simpleframe.model.result.ClubResult;
import simpleframe.service.ClubService;
import simpleframe.utils.RegexUtils;
import simpleframe.utils.StringUtils;
import simpleframe.utils.PageUtils.PageInfo;

/**
 * 粉丝圈接口
 *
 * @author:axi
 * @date:2017/7/31
 */
@Controller("apiClubController")
@RequestMapping("api/club/")
public class ClubController extends BaseController {

    @Autowired
    private ClubService clubService;

    /**
     * 粉丝团分页列表
     *
     * @param starId
     * @param loginId
     * @return
     */
    @ResponseBody
    @RequestMapping("getExtendList")
    public OutApiModel getExtendList(Integer pageNum, Integer pageSize, Integer starId, Integer loginId) {
        OutApiModel result = new OutApiModel();
        if (pageNum == null) {
            pageNum = 1;
        }
        if (pageSize == null) {
            return result.init(false, OutApiCode.API_0212000);
        }

        if (pageSize > 100) {
            return result.init(false, OutApiCode.API_0212001);
        }

        ClubParam param = new ClubParam();
        param.setLoginId(loginId);
        param.setStarId(starId);
        PageInfo<ClubResult> data = clubService.getExtendList(param, pageNum, pageSize);
        if (data == null) {
            return result.init(false, OutApiCode.API_0000001);
        }
        return result.init(true, OutApiCode.API_0000000, data);

    }

    @ResponseBody
    @RequestMapping("addClub")
    public OutApiModel addClub(Integer loginId, Integer clubType, Integer certifyType
            , String name, String principalName, String principalPosition
            , String mobile, String weibo, String weixin, String qq
            , String idcardFront, String idcardBack, Integer starId) {
        OutApiModel result = new OutApiModel();
        Club club = new Club();

        if(loginId==null)
        {
            return result.init(false,OutApiCode.API_0213000);
        }
        if(clubType==null)
        {
            return result.init(false,OutApiCode.API_0213001);
        }
        if(certifyType==null)
        {
            return result.init(false,OutApiCode.API_0213002);
        }

        if(StringUtils.isBlank(name))
        {
            return result.init(false,OutApiCode.API_0213003);
        }

        if(StringUtils.isBlank(principalName))
        {
            return result.init(false,OutApiCode.API_0213004);
        }

        if(StringUtils.isBlank(principalPosition))
        {
            return result.init(false,OutApiCode.API_0213005);
        }

        if(StringUtils.isBlank(mobile))
        {
            return result.init(false,OutApiCode.API_0213006);
        }

        if(!RegexUtils.checkMobile(mobile))
        {
            return result.init(false,OutApiCode.API_0213007);
        }

        if(starId==null)
        {
            return result.init(false,OutApiCode.API_0213008);
        }

        if(StringUtils.isBlank(idcardFront))
        {
            return result.init(false,OutApiCode.API_0213009);
        }

        if(StringUtils.isBlank(idcardBack))
        {
            return result.init(false,OutApiCode.API_0213010);
        }

        if(StringUtils.isBlank(weibo))
        {
            return result.init(false,OutApiCode.API_0213011);
        }

        club.setMemberId(loginId);
        club.setType(clubType);
        club.setCertifyType(certifyType);
        club.setName(name);
        club.setPrincipalName(principalName);
        club.setPrincipalPosition(principalPosition);
        club.setMobile(mobile);
        club.setStarId(starId);
        club.setWeibo(weibo);
        club.setWeixin(weixin);
        club.setQq(qq);
        club.setIdcardFront(idcardFront);
        club.setIdcardBack(idcardBack);
        club.setAuditState(AuditState.AUDITING.getValue());
        club.setIsOfficial(OfficialType.NO.getValue());

        Club data=clubService.insertReturnModel(club);

        if (data == null) {
            return result.init(false, OutApiCode.API_0000001);
        }
        return result.init(true, OutApiCode.API_0000000, data);
    }
    
    /**
     * 根据用户id获取粉丝团信息
     * @param loginId
     * @return
     */
    @RequestMapping(value="getClubInfo",consumes = "application/json")
    @ResponseBody
    public OutApiModel getClubInfo(@RequestBody InApiModel<LoginIdParam> inParam){
    	OutApiModel result = new OutApiModel();
    	Integer loginId = inParam.getData().getLoginId();
    	if(loginId==null) {
            return result.init(false,OutApiCode.API_0213000);
        }
    	List<Club> clubs = clubService.selectByMemberId(loginId);
    	Club club = null;
    	if(clubs != null && clubs.size() > 0){
    		club = clubs.get(0);
    	}
    	result.init(true, OutApiCode.API_0000000, club);
    	return result;
    }
}
