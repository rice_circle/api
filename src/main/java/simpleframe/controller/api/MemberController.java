package simpleframe.controller.api;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import simpleframe.model.api.InApiModel;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.*;
import simpleframe.model.core.Attention;
import simpleframe.model.core.Member;
import simpleframe.model.param.MemberParam;
import simpleframe.model.result.MemberResult;
import simpleframe.service.AttentionService;
import simpleframe.service.MemberService;
import simpleframe.service.ShortMessageService;
import simpleframe.utils.RegexUtils;
import simpleframe.utils.StringUtils;

/**
 * 会员接口
 *
 * @author:axi
 * @date:2017/7/24
 */
@Controller("apiMemberController")
@RequestMapping("api/member/")
public class MemberController {

    @Autowired
    private MemberService memberService;
    
    @Autowired
    private ShortMessageService shortMessageService;

    @Autowired
    private AttentionService attentionService;

    /**
     * 注册接口
     *
     * @param mobile    手机号码
     * @param password  密码
     * @param checkCode 验证码
     * @return
     */
    @ResponseBody
    @RequestMapping("regist")
    public OutApiModel regist(String mobile, String password, String checkCode) {
        OutApiModel result = new OutApiModel();
        result.init(OutApiCode.API_0000001);
        if (StringUtils.isBlank(mobile)) {
            return result.init(OutApiCode.API_0102000);
        }
        if (!RegexUtils.checkMobile(mobile)) {
            return result.init(OutApiCode.API_0102001);
        }
        if (StringUtils.isBlank(password)) {
            return result.init(OutApiCode.API_0102002);
        }
        if (StringUtils.isBlank(checkCode)) {
            return result.init(OutApiCode.API_0102003);
        }
        
        ShortMessageState state = shortMessageService.validShortMessage(mobile,checkCode);
        //验证码不存在
        if(state == ShortMessageState.UNCREATE){
        	return result.init(OutApiCode.API_0102004);
        }
        //验证码已使用
        if(state == ShortMessageState.USED){
        	return result.init(OutApiCode.API_0102006);
        }
        //验证码已使用
        if(state == ShortMessageState.EXPIRED){
        	return result.init(OutApiCode.API_0102005);
        }
        //验证码已使用
        if(state == ShortMessageState.ERROR){
        	return result.init(OutApiCode.API_0102007);
        }
        Member param = new Member();
        param.setMobile(mobile);
        param.setIsDeleted(DeleteType.NORMAL.getValue());
        Member oldMember = memberService.selectModel(param);
        if (oldMember != null) {
            return result.init(OutApiCode.API_0203000);
        }

        param.setIsCertificated(CetificatedType.NO.getValue());
        param.setIsDeleted(DeleteType.NORMAL.getValue());
        param.setPassword(password);

        Member data = memberService.insertReturnModel(param);
        data.setPassword(null);
        if (data != null && data.getId() > 0) {
            return result.init(true, OutApiCode.API_0000000, data);
        } else {
            return result.init(OutApiCode.API_0000001);
        }
    }

    /**
     * 登录接口
     *
     * @param mobile   手机号码
     * @param password 密码
     * @return
     */
    @ResponseBody
    @RequestMapping("login")
    public OutApiModel login(String mobile, String password) {
        OutApiModel result = new OutApiModel();
        result.init(OutApiCode.API_0000001);
        if (StringUtils.isBlank(mobile)) {
            return result.init(OutApiCode.API_0102000);
        }
        if (!RegexUtils.checkMobile(mobile)) {
            return result.init(OutApiCode.API_0102001);
        }
        if (StringUtils.isBlank(password)) {
            return result.init(OutApiCode.API_0102002);
        }

        Member param = new Member();
        param.setMobile(mobile);
        param.setPassword(password);
        param.setIsDeleted(DeleteType.NORMAL.getValue());
        Member data = memberService.selectModel(param);
        if (data == null || data.getId() <= 0) {
            return result.init(OutApiCode.API_0204000);
        }


        Attention attention=new Attention();
        attention.setIsEnabled(AttentionEnabled.YES.getValue());
        attention.setMemberId(data.getId());
        attention.setRelationType(AttentionRelationType.STAR.getValue());
        Integer attentedCount= attentionService.selectCount(attention);

        MemberResult dataResult=new MemberResult();
        BeanUtils.copyProperties(data,dataResult);
        dataResult.setIsAttendStar(0);
        if(attentedCount>0)
        {
            dataResult.setIsAttendStar(1);
        }
        dataResult.setPassword(null);
        return result.init(true, OutApiCode.API_0000000, dataResult);
    }

    /**
     * 重置密码
     * @param mobile
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "resetPassword",consumes = "application/json",method= RequestMethod.POST)
    public OutApiModel resetPassword(@RequestBody InApiModel<MemberParam> inMemberParam)
    {
        OutApiModel result = new OutApiModel();
        if(inMemberParam.getData()==null)
        {
            return result.init(OutApiCode.API_0219009);
        }

        MemberParam param=inMemberParam.getData();
        if (StringUtils.isBlank(param.getMobile())) {
            return result.init(OutApiCode.API_0219000);
        }
        if (!RegexUtils.checkMobile(param.getMobile())) {
            return result.init(OutApiCode.API_0219001);
        }
        if (StringUtils.isBlank(param.getPassword())) {
            return result.init(OutApiCode.API_0219002);
        }
        if (StringUtils.isBlank(param.getCheckCode())) {
            return result.init(OutApiCode.API_0219003);
        }

        ShortMessageState state = shortMessageService.validShortMessage(param.getMobile(),param.getCheckCode());
        //验证码不存在
        if(state == ShortMessageState.UNCREATE){
            return result.init(OutApiCode.API_0219004);
        }
        //验证码已使用
        if(state == ShortMessageState.USED){
            return result.init(OutApiCode.API_0219005);
        }
        //验证码已使用
        if(state == ShortMessageState.EXPIRED){
            return result.init(OutApiCode.API_0219006);
        }
        //验证码已使用
        if(state == ShortMessageState.ERROR){
            return result.init(OutApiCode.API_0219007);
        }
        Member model = new Member();
        model.setMobile(param.getMobile());
        model.setIsDeleted(DeleteType.NORMAL.getValue());

        Member oldMember = memberService.selectModel(model);
        if (oldMember == null) {
            return result.init(OutApiCode.API_0219008);
        }

        oldMember.setPassword(param.getPassword());

        if(!memberService.update(oldMember))
        {
            return result.init(OutApiCode.API_0000001);
        }


        return result.init(true,OutApiCode.API_0000000,null);

    }

    /**
     * 修改用户资料
     * @param param
     * @return
     */
    @ResponseBody
    @RequestMapping(value="updateInfo",consumes = "application/json",method= RequestMethod.POST)
    public OutApiModel updateInfo(@RequestBody InApiModel<Member> inParam)
    {
        OutApiModel result = new OutApiModel();
        if(inParam==null||inParam.getData()==null)
        {
            return result.init(OutApiCode.API_0220000);
        }

        if(inParam.getLoginId() == null){
            return result.init(OutApiCode.API_0218000);
        }

        Member param=inParam.getData();

        Member member=new Member();
        member.setId(inParam.getLoginId());
        member.setNickName(param.getNickName());
        member.setBirthday(param.getBirthday());
        member.setPortrait(param.getPortrait());
        member.setMotto(param.getMotto());
        member.setSex(param.getSex());

        if(!memberService.updateTrim(member))
        { return result.init(OutApiCode.API_0000001);

        }

        member=memberService.select(inParam.getLoginId());
        if(member!=null)
        {
            member.setPassword(null);
        }
        return result.init(true,OutApiCode.API_0000000,member);
    }
}
