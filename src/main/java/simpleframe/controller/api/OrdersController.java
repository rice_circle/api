package simpleframe.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import simpleframe.controller.BaseController;
import simpleframe.model.api.InOrderParam;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.Constant;
import simpleframe.model.base.DeleteType;
import simpleframe.model.base.OrdersStatus;
import simpleframe.model.core.*;
import simpleframe.model.result.OrderResult;
import simpleframe.model.result.OrdersResult;
import simpleframe.service.*;
import simpleframe.utils.PageUtils.PageInfo;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author:axi
 * @date:2017/7/23
 */
@Controller("apiOrderController")
@RequestMapping("/api/order/")
public class OrdersController extends BaseController {


    @Autowired
    private OrdersService ordersService;
    @Autowired
    private ProductService productService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private ReceiverService receiverService;


    /**
     * 获取购买排名
     *
     * @param activityId 活动Id
     * @return
     * @author wuxianbing
     */
    @ResponseBody
    @RequestMapping("getPurchaseRank")
    public OutApiModel getPurchaseRank(Integer activityId)
    {
        //获取排名
        OutApiModel result = new OutApiModel();
        if (activityId == null) {
            return result.init(false, OutApiCode.API_0100000, null);
        }

        Orders param=new Orders();
        param.setActivityId(activityId);
        param.setIsDeleted(DeleteType.NORMAL.getValue());
        List<OrderResult> data= ordersService.getPurchaseRank(param);
        if (data == null) {
            return result.init(false, OutApiCode.API_0000001, null);
        }

        return result.init(true, OutApiCode.API_0000000, data);
    }

    /**
     * 提交订单
     * @param loginId 用户id
     * @param productId 商品id
     * @param num 购买数量
     * @return
     */
    @ResponseBody
    @RequestMapping("submitOrder")
    public OutApiModel submitOrder(Integer loginId, Integer productId, Integer num)
    {
        OutApiModel result = new OutApiModel();
        if(loginId ==null)
        {
            return result.init(false,OutApiCode.API_0206000,null);
        }
        if(productId==null)
        {
            return result.init(false,OutApiCode.API_0206001,null);
        }
        if(num ==null)
        {
            return result.init(false,OutApiCode.API_0206002,null);
        }

        Product product= productService.select(productId);

        if(!productService.lockStock(productId,num))
        {
            return result.init(false,OutApiCode.API_0206002,null);
        }

        Member member= memberService.select(loginId);
        Activity activity= activityService.select(product.getActivityId());



        String code=ordersService.generateCode(activity.getActivityType());

        Orders orders=new Orders();
        orders.setCode(code);
        orders.setActivityId(activity.getId());
        orders.setActivityName(activity.getName());
        orders.setIsDeleted(DeleteType.NORMAL.getValue());
        orders.setMemberId(member.getId());
        orders.setNum(num);
        orders.setProductId(product.getId());
        orders.setPrice(product.getPrice());
        orders.setProductName(product.getName());
        orders.setStatus(OrdersStatus.PAYING.getValue());
        orders.setTotalProductMoney(product.getPrice().multiply(BigDecimal.valueOf(num)));
        orders.setTotalOrderMoney(orders.getTotalProductMoney().add(Constant.DELIVERY_FEE));

        Orders data=ordersService.insertReturnModel(orders);

        if(orders!=null)
        {
            return result.init(true,OutApiCode.API_0000000,data);
        }
        else
        {
            return result.init(false,OutApiCode.API_0000001,null);
        }

    }

    /**
     * 确认订单接口
     *
     * @param orderId
     * @param receiverId
     * @return
     */
    @ResponseBody
    @RequestMapping("confirmOrder")
    public OutApiModel confirmOrder(Integer loginId, Integer orderId, Integer receiverId) {
        OutApiModel result = new OutApiModel();
        if(loginId ==null)
        {
            return result.init(false,OutApiCode.API_0208002,null);
        }
        if(orderId==null)
        {
            return result.init(false,OutApiCode.API_0208000);
        }
        if(receiverId ==null)
        {
            return result.init(false,OutApiCode.API_0208001);
        }
        Orders data = ordersService.select(orderId);
        Receiver receiver = receiverService.select(receiverId);
        data.setAddress(receiver.getAddress());
        data.setPositionCode(receiver.getPositionCode());
        data.setMobile(receiver.getMobile());
        data.setReceiverName(receiver.getName());
        boolean updateResult = ordersService.update(data);
        if (!updateResult) {
            return result.init(false, OutApiCode.API_0000001);
        }

        data=ordersService.select(orderId);

        return result.init(true, OutApiCode.API_0000000, data);

    }

    @ResponseBody
    @RequestMapping("getExtendList")
    public OutApiModel getExtendList(InOrderParam inParam)
    {
        OutApiModel result=new OutApiModel();

        if(inParam.getData()==null)
        {
            return result.init(OutApiCode.API_0217000);
        }
        if(inParam.getLoginId()==null){
            return result.init(OutApiCode.API_0217001);
        }

        if (inParam.getPageNum() == null) {
            inParam.setPageNum(1);
        }
        if (inParam.getPageSize() == null) {
            return result.init(OutApiCode.API_0217002);
        }

        if (inParam.getPageSize() > 100) {
            return result.init(OutApiCode.API_0217003);
        }

        Orders param=inParam.getData();
        if(param.getStatus()!=null&&param.getStatus()==-1)
        {
            param.setStatus(null);
        }

        PageInfo<OrdersResult> data=ordersService.getExtendList(param,inParam.getPageNum(),inParam.getPageSize());

        if(data==null)
        {
            return result.init(OutApiCode.API_0000001);
        }

        return result.init(true,OutApiCode.API_0000000,data);
    }

    /**
     * 获取订单详情
     * @param inParam
     * @return
     */
    @ResponseBody
    @RequestMapping("getDetails")
    public OutApiModel getDetails(InOrderParam inParam)
    {
        OutApiModel result=new OutApiModel();
        if(inParam.getLoginId()==null){
            return result.init(OutApiCode.API_0218000);
        }
        if(inParam.getData()==null)
        {
            return result.init(OutApiCode.API_0218001);
        }

        if (inParam.getData().getId() == null) {
            return result.init(OutApiCode.API_0218002);
        }

        OrderResult data=ordersService.getDetails(inParam.getData());

        if(data==null)
        {
            return result.init(OutApiCode.API_0000001);
        }

        return result.init(true,OutApiCode.API_0000000,data);

    }
}
