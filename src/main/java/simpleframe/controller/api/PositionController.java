package simpleframe.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import simpleframe.controller.BaseController;
import simpleframe.model.api.InApiModel;
import simpleframe.model.api.InPositionParam;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.core.Position;
import simpleframe.service.PositionService;
import simpleframe.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 位置接口
 * @author:axi
 * @date:2017/8/6
 */
@Controller("apiPositionController")
@RequestMapping("api/position/")
public class PositionController extends BaseController {

    @Autowired
    private PositionService positionService;

    @ResponseBody
    @RequestMapping("getSubPosition")
    public OutApiModel getSubPosition(InPositionParam inParam)
    {
        OutApiModel result=new OutApiModel();

        if(inParam.getData()==null)
        {
            return result.init(OutApiCode.API_0216000);
        }

        if(StringUtils.isBlank(inParam.getData().getParentCode()))
        {
            return result.init(OutApiCode.API_0216001);
        }

        Position model=new Position();
        model.setParentCode(inParam.getData().getParentCode());
        List<Position> data=positionService.selectList(model);

        if(data==null)
        {
            return result.init(OutApiCode.API_0000001);
        }
        return result.init(true,OutApiCode.API_0000000,data);
    }
}
