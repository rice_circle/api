/*
 * 天虹商场股份有限公司版权所有.
 */
package simpleframe.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import simpleframe.controller.BaseController;
import simpleframe.model.api.InApiModel;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.model.base.AddressDefault;
import simpleframe.model.base.DeleteType;
import simpleframe.model.core.Receiver;
import simpleframe.service.ReceiverService;
import simpleframe.utils.RegexUtils;

/**
 * 地址接口
 *
 * @author:wuxianbing
 * @date:2017/7/28
 */
@Controller("apiReceiverController")
@RequestMapping("/api/receiver/")
public class ReceiverController extends BaseController {

    @Autowired
    private ReceiverService receiverService;

    /**
     * 获取会员收获地址
     *
     * @param loginId
     * @return
     */
    @ResponseBody
    @RequestMapping("getReceiver")
    public OutApiModel getAddress(Integer loginId) {
        OutApiModel outApiModel = new OutApiModel();
        if (loginId == null) {
            return outApiModel.init(false, OutApiCode.API_0207000, null);
        }

        Receiver param = new Receiver();
        param.setMemberId(loginId);
        param.setIsDeleted(DeleteType.NORMAL.getValue());
        param.setOrder("is_default desc");
        List<Receiver> data = receiverService.selectList(param);

        if (data == null) {
            return outApiModel.init(false, OutApiCode.API_0000001, null);
        } else {
            return outApiModel.init(true, OutApiCode.API_0000000, data);
        }
    }

    /**
     * 添加收获地址
     *
     * @param address
     * @return
     */
    @ResponseBody
    @RequestMapping("addReceiver")
    public OutApiModel addAddress(Integer loginId, String name, String mobile, String positionCode, String address, Integer isDefault) {
        OutApiModel result = new OutApiModel();

        if (name == null) {
            return result.init(false, OutApiCode.API_0209000);
        }
        if (mobile == null) {
            return result.init(false, OutApiCode.API_0209001);
        }
        if (!RegexUtils.checkMobile(mobile)) {
            return result.init(false, OutApiCode.API_0209002);
        }
        if (positionCode == null) {
            return result.init(false, OutApiCode.API_0209003);
        }
        if (address == null) {
            return result.init(false, OutApiCode.API_0209004);
        }
        if (loginId == null) {
            return result.init(false, OutApiCode.API_0209005);
        }

        Receiver model = new Receiver();
        model.setName(name);
        model.setMobile(mobile);
        model.setPositionCode(positionCode);
        model.setAddress(address);
        model.setMemberId(loginId);
        model.setIsDefault(isDefault == null ? AddressDefault.NO.getValue() : isDefault);

        Receiver data = receiverService.insertReturnModel(model);
        if(data==null)
        {
            return result.init(true, OutApiCode.API_0000001, null);
        }
        return result.init(true, OutApiCode.API_0000000, data);
    }
    
    /**
     * 修改收货地址
     * @param inParam
     * @return
     */
    @ResponseBody
    @RequestMapping("updaterReceiver")
    public OutApiModel updaterReceiver(@RequestBody InApiModel<Receiver> inParam){
    	OutApiModel result = new OutApiModel();
    	Receiver data = inParam.getData();
    	if(data == null){
    		return result.init(false, OutApiCode.API_0209006);
    	}
    	if(data.getId() == null){
    		return result.init(false, OutApiCode.API_0209007);
    	}
    	boolean updateResult = receiverService.updateTrim(data);
    	if(updateResult){
    		return result.init(true, OutApiCode.API_0000000);
    	}
    	return result.init(true, OutApiCode.API_0000004);
    }
    
    /**
     * 修改收货地址
     * @param inParam
     * @return
     */
    @ResponseBody
    @RequestMapping("deleteReceiver")
    public OutApiModel deleteReceiver(@RequestBody InApiModel<Receiver> inParam){
    	OutApiModel result = new OutApiModel();
    	Receiver data = inParam.getData();
    	if(data == null){
    		return result.init(false, OutApiCode.API_0209006);
    	}
    	if(data.getId() == null){
    		return result.init(false, OutApiCode.API_0209007);
    	}
    	boolean updateResult = receiverService.deleteSoft(data.getId());
    	if(updateResult){
    		return result.init(true, OutApiCode.API_0000000);
    	}
    	return result.init(true, OutApiCode.API_0000005);
    }
    
    /**
     * 设置默认的收货地址
     * @param inParam
     * @return
     */
    @ResponseBody
    @RequestMapping("setDefaultReceiver")
    public OutApiModel setDefaultReceiver(@RequestBody InApiModel<Receiver> inParam){
    	OutApiModel result = new OutApiModel();
    	Receiver data = inParam.getData();
    	if(data == null){
    		return result.init(false, OutApiCode.API_0209006);
    	}
    	if(data.getId() == null){
    		return result.init(false, OutApiCode.API_0209007);
    	}
    	if(data.getMemberId() == null){
    		return result.init(false, OutApiCode.API_0209008);
    	}
    	boolean setResult = receiverService.setDefaultReceiver(data.getMemberId(),data.getId());
    	if(setResult){
    		return result.init(true, OutApiCode.API_0000000);
    	}
    	return result.init(true, OutApiCode.API_0209009);
    }
}
