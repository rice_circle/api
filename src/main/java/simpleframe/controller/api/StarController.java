package simpleframe.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import simpleframe.controller.BaseController;
import simpleframe.model.api.OutApiCode;
import simpleframe.model.api.OutApiModel;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.param.StarParam;
import simpleframe.model.result.StarResult;
import simpleframe.service.StarService;

/**
 * 明星相关接口
 *
 * @author:axi
 * @date:2017/7/23
 */
@Controller("apiStarController")
@RequestMapping("/api/star/")
public class StarController extends BaseController {

    @Autowired
    private StarService starService;

    @ResponseBody
    @RequestMapping("getExtendList")
    public OutApiModel getExtendList(String name, Integer loginId, Integer starType, Integer pageNum, Integer pageSize) {
        OutApiModel result = new OutApiModel();
        if (pageNum == null) {
            pageNum = 1;
        }
        if (pageSize == null) {
            return result.init(OutApiCode.API_0214000);
        }
        if (pageSize > 100) {
            return result.init(OutApiCode.API_0214001);
        }
        StarParam param = new StarParam();
        param.setName(name);
        param.setLoginId(loginId);
        param.setStarType(starType);
        PageInfo<StarResult> data = starService.getExtendList(pageNum, pageSize, param);

        if (data == null) {
            return result.init(OutApiCode.API_0000001);
        }
        return result.init(true, OutApiCode.API_0000000, data);
    }

    @ResponseBody
    @RequestMapping("getDetails")
    public OutApiModel getDetails(Integer starId, Integer loginId) {
        OutApiModel result = new OutApiModel();
        if (starId == null) {
            return result.init(OutApiCode.API_0211000);
        }
        StarParam param = new StarParam();
        param.setLoginId(loginId);
        param.setId(starId);
        StarResult data = starService.getDetails(param);

        if (data == null) {
            return result.init(OutApiCode.API_0000001);
        }
        return result.init(true, OutApiCode.API_0000000, data);
    }

}
