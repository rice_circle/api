package simpleframe.core.restful;

/**
 * rest ful api请求接口类
 * @author sunjichang
 * @date 2017年2月22日下午12:39:22
 * @version 1.0
 * @upate 
 */
public interface RESTfulApiInvoker {
	
	public <T> RESTfulResponse<T> sendRequest(RESTfulRequest<T> request);
}
