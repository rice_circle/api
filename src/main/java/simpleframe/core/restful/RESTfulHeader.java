package simpleframe.core.restful;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import simpleframe.utils.StringUtils;

/**
 * header参数
 * @author sunjichang
 * @date 2017年2月22日下午1:07:33
 * @version 1.0
 * @upate 
 */
public class RESTfulHeader {
	//内容格式 
	private static final String HEADER_CONTENT_TYPE = "Content-Type";

	private static final String HEADER_AUTH = "Authorization";

	private static final String RESTRICT_ACCESS = "restrict-access";

	private static final String THUMBNAIL = "thumbnail";

	private static final String SHARE_SECRET = "share-secret";
	//返回体接受格式
	private static final String ACCEPT = "Accept";

	private List<NameValuePair> headers = new ArrayList<NameValuePair>();

	public static RESTfulHeader newInstance() {
		return new RESTfulHeader();
	}

	public RESTfulHeader addHeader(String key, String value) {
		if (StringUtils.isBlank(key) || StringUtils.isBlank(value)) {
			return this;
		}

		headers.add(new BasicNameValuePair(key, value));
		return this;
	}

	public RESTfulHeader addJsonContentHeader() {
		return addHeader(HEADER_CONTENT_TYPE, "application/json");
	}

	public RESTfulHeader addAuthorization(String token) {
		return addHeader(HEADER_AUTH, "Bearer " + token);
	}

	public RESTfulHeader addRestrictAccess() {
		return addHeader(RESTRICT_ACCESS, "true");
	}

	public RESTfulHeader addThumbnail() {
		return addHeader(THUMBNAIL, "true");
	}

	public RESTfulHeader addShareSecret(String secret) {
		return addHeader(SHARE_SECRET, secret);
	}

	public RESTfulHeader addMediaAccept() {
		return addHeader(ACCEPT, "application/octet-stream");
	}

	public List<NameValuePair> getHeaders() {
		return headers;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (NameValuePair header : headers) {
			sb.append("[").append(header.getName()).append(":").append(header.getValue()).append("] ");
		}

		return sb.toString();
	}
}
