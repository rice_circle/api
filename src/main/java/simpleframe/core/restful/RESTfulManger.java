package simpleframe.core.restful;

import java.lang.reflect.Proxy;

import simpleframe.core.restful.impl.JerseyRestAPIManger;

/**
 * RESTful请求管理类
 * @author sunjichang
 * @date 2017年2月22日下午12:57:02
 * @version 1.0
 * @upate 
 */
public class RESTfulManger implements RESTfulApiInvoker{

	// 代理类
	private RESTfulApiInvoker mProxy = null;

	private static class InstanceHolder {
		public static final RESTfulManger INSTANCE = new RESTfulManger();
	}

	private RESTfulManger() {
		Class<RESTfulApiInvoker> target = RESTfulApiInvoker.class;
		mProxy = (RESTfulApiInvoker) Proxy.newProxyInstance(target.getClassLoader(), new Class[] { target },
				new RESTfulProxyHandler(new JerseyRestAPIManger()));
	}

	public static RESTfulManger getInstance() {
		return InstanceHolder.INSTANCE;
	}

	@Override
	public RESTfulResponse sendRequest(RESTfulRequest request) {
		return mProxy.sendRequest(request);
	}
}
