package simpleframe.core.restful;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * REST ful代理类
 * @author sunjichang
 * @date 2017年2月22日下午12:53:50
 * @version 1.0
 * @upate 
 */
public class RESTfulProxyHandler implements InvocationHandler{
	
	//代理对象
	private RESTfulApiInvoker delegate;
	
	public RESTfulProxyHandler(RESTfulApiInvoker delegate){
		this.delegate = delegate;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		return method.invoke(delegate, args);
	}
	
}
