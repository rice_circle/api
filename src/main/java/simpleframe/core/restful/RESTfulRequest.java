package simpleframe.core.restful;

import java.util.Map;

/**
 * RESTful请求管理类
 * @author sunjichang
 * @date 2017年2月22日下午1:37:19
 * @version 1.0
 * @upate 
 */
public class RESTfulRequest<T> {
	
	private Map<String, String> headers = null;
	//请求地址
	private String url;
	//请求方法
	private Method method;
	//https CA证书路径
	private String cacertPath;
	//https CA证书密码
	private String cacertPwd;
	private Class<T> parseClass;
	
	public RESTfulRequest(String url,Method method,Class<T> cls){
		this.url = url;
		this.method = method;
		this.parseClass = cls;
	}

	/**
	 * 获取请求head参数
	 * @return
	 */
	public Map<String,String> getHeaders(){
		return null;
	}
	
	/**
	 * 获取请求参数
	 * @return
	 */
	public Map<String,String> getParams(){
		return null;
	}
	
	/**
	 * 获取body参数
	 * @return
	 */
	public Object getBody(){
		return null;
	}
	
	/**
	 * 获取请求参数解析的编码格式
	 * @return
	 */
	public String getParamsEncoding(){
		return "utf-8";
	}
	
	/**
	 * 获取接受格式
	 * @return
	 */
	public String getAcceptType(){
		return "application/json";
	}
	
	/**
	 * 获取body内容格式
	 * @return
	 */
	public String getBodyContentType() {
        return "application/json; charset=" + getParamsEncoding();
    }
	
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the method
	 */
	public Method getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(Method method) {
		this.method = method;
	}

	/**
	 * @return the cacertPath
	 */
	public String getCacertPath() {
		return cacertPath;
	}

	/**
	 * @param cacertPath the cacertPath to set
	 */
	public void setCacertPath(String cacertPath) {
		this.cacertPath = cacertPath;
	}

	/**
	 * @return the cacertPwd
	 */
	public String getCacertPwd() {
		return cacertPwd;
	}

	/**
	 * @param cacertPwd the cacertPwd to set
	 */
	public void setCacertPwd(String cacertPwd) {
		this.cacertPwd = cacertPwd;
	}
	
	/**
	 * 解析数据
	 * @param content
	 */
	public T parseResponse(String content){
		return null;
	}

	/**
	 * @return the parseClass
	 */
	public Class<T> getParseClass() {
		return parseClass;
	}

	/**
	 * @param parseClass the parseClass to set
	 */
	public void setParseClass(Class<T> parseClass) {
		this.parseClass = parseClass;
	}



	/**
	 * 请求方法
	 * @author sunjichang
	 * @date 2017年2月22日下午1:24:13
	 * @version 1.0
	 * @upate
	 */
	public static enum Method{
		GET,POST,PUT,DELETE,UPDAT
	}
}
