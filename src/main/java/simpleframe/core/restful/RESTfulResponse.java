package simpleframe.core.restful;

/**
 * restful api访问返回结果
 * @author sunjichang
 * @date 2017年2月22日下午12:38:48
 * @version 1.0
 * @upate 
 */
public class RESTfulResponse<T> {
	private String message;
	private int responseStateCode;
	private T responseBody;
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * @return the responseStateCode
	 */
	public int getResponseStateCode() {
		return responseStateCode;
	}
	/**
	 * @param responseStateCode the responseStateCode to set
	 */
	public void setResponseStateCode(int responseStateCode) {
		this.responseStateCode = responseStateCode;
	}
	/**
	 * @return the responseBody
	 */
	public T getResponseBody() {
		return responseBody;
	}
	/**
	 * @param responseBody the responseBody to set
	 */
	public void setResponseBody(T responseBody) {
		this.responseBody = responseBody;
	}
}
