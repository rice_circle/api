package simpleframe.core.restful.impl;

import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.glassfish.jersey.client.JerseyClient;
import org.glassfish.jersey.client.JerseyWebTarget;

import simpleframe.core.restful.RESTfulApiInvoker;
import simpleframe.core.restful.RESTfulRequest;
import simpleframe.core.restful.RESTfulResponse;
import simpleframe.utils.StringUtils;

/**
 * 使用jersey调用RESTful api
 * @author sunjichang
 * @date 2017年2月22日下午8:09:35
 * @version 1.0
 * @upate 
 */
public class JerseyRestAPIManger implements RESTfulApiInvoker{
	private static final Logger logger = Logger.getLogger(JerseyRestAPIManger.class);

	@Override
	public <T> RESTfulResponse<T> sendRequest(RESTfulRequest<T> request) {
		if(request == null){
			return null;
		}
		String cacertFilePath = request.getCacertPath();
        String cacertFilePassword = request.getCacertPwd();
        String url = request.getUrl();
        JerseyClient client = RESTfulClientFactory.getJerseyClient(StringUtils.startsWithIgnoreCase(request.getUrl(), "HTTPS"), 
        		cacertFilePath, cacertFilePassword);
        JerseyWebTarget target = client.target(url);
        Invocation.Builder inBuilder = target.request();
        buildHeader(inBuilder, request.getHeaders());
        Response response;
        Object b = request.getBody();
        switch (request.getMethod()) {
            case POST:
                response = inBuilder.post(Entity.entity(b, MediaType.APPLICATION_JSON), Response.class);
                break;
            case PUT:
                response = inBuilder.put(Entity.entity(b, MediaType.APPLICATION_JSON), Response.class);
                break;
            case GET:
                response = inBuilder.get(Response.class);
                break;
            case DELETE:
                response = inBuilder.delete(Response.class);
                break;
            default:
                throw new RuntimeException("");
        }
        String responseContent = response.readEntity(String.class);
        logger.error("responseContent="+responseContent);
        T t = request.parseResponse(responseContent);
        RESTfulResponse<T> result = new RESTfulResponse<>();
        result.setResponseBody(t);
        result.setResponseStateCode(response.getStatus());
        return result;
	}
	
	private void buildHeader(Invocation.Builder inBuilder, Map<String, String> header) {
		if(null == header){
			return;
		}
		Iterator<String> iterator = header.keySet().iterator();
		while (iterator.hasNext()) {
			String key = iterator.next();
			inBuilder.header(key, header.get(key));
		}
    }

}
