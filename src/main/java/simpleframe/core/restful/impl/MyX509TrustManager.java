package simpleframe.core.restful.impl;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import simpleframe.utils.StringUtils;

/**
 * 
 * @author sunjichang
 * @date 2017年2月22日下午2:00:53
 * @version 1.0
 * @upate 
 */
public class MyX509TrustManager implements X509TrustManager {
	private static final Logger logger = Logger.getLogger(MyX509TrustManager.class);
    X509TrustManager myTrustManager;
    private boolean isNullCacer = false;
    public MyX509TrustManager(String cacertFile, String password) throws Exception {
    	logger.log(Level.DEBUG, "cacertFile="+cacertFile+",isBlank="+StringUtils.isBlank(cacertFile));
    	isNullCacer = StringUtils.isBlank(cacertFile);
    	//证书类型
        KeyStore keyStore = KeyStore.getInstance(StringUtils.isBlank(cacertFile) ? KeyStore.getDefaultType() : "JKS");
        //加载证书
        keyStore.load(StringUtils.isBlank(cacertFile) ? null : new FileInputStream(cacertFile), 
        		StringUtils.isBlank(cacertFile) ? null :password.toCharArray());

        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);

        TrustManager trustManagers[] = trustManagerFactory.getTrustManagers();

        for(TrustManager trustManager : trustManagers) {
            if(trustManager instanceof X509TrustManager) {
                myTrustManager = (X509TrustManager) trustManager;
                return;
            }
        }

        throw new Exception("Couldn't initialize");
    }

    @Override
    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

    }

    @Override
    public void checkServerTrusted(X509Certificate[] certificates, String authType) throws CertificateException {
        if(!isNullCacer){
        	if ((certificates != null) && (certificates.length == 1)) {
                certificates[0].checkValidity();
            } else {
                myTrustManager.checkServerTrusted(certificates, authType);
            }
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return myTrustManager.getAcceptedIssuers();
    }
}