
/**
 *
 * Title：Activity
 * @author axi
 * @version 1.0, 2017年07月20日
 * @since 2017年07月20日
 */

package simpleframe.dao;

import simpleframe.model.param.ActivityParam;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Activity;
import simpleframe.model.result.ActivityResult;

public interface ActivityDao extends BaseDao<Activity,Integer>{

    /**
     * 获取最新的活动数据
     * @author wuxianbing
     * @param pageNum 页码
     * @param pageSize 每页条数
     * @param activity 活动参数
     * @return
     */
    public PageInfo<ActivityResult> getLastList(int pageNum, int pageSize, ActivityParam activity);

    /**
     * 获取活动详情
     * @param id
     * @return
     */
    ActivityResult getDetails(int id);
    
    /**
     * 获取发布的活动列表
     * @param pageNum 页码
     * @param pageSize 每页条数
     * @param activity 参数
     * @return
     */
    public PageInfo<ActivityResult> getPublishActivityList(int pageNum, int pageSize, ActivityParam activity);
}

