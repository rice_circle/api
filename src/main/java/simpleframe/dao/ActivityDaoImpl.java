
/**
 * Title：Activity
 *
 * @author axi
 * @version 1.0, 2017年07月20日
 * @since 2017年07月20日
 */

package simpleframe.dao;


import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.ActivityMapper;
import simpleframe.model.param.ActivityParam;

import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Activity;
import simpleframe.model.result.ActivityResult;

import java.util.List;

@Repository
public class ActivityDaoImpl extends BaseDaoImpl<Activity,Integer> implements ActivityDao {

	private static final Logger logger = Logger.getLogger(ActivityDaoImpl.class);

	@Autowired
	private ActivityMapper activityMapper;

	@Autowired
	public void setActivityMapper(ActivityMapper activityMapper) {
		super.setBaseMapper(activityMapper);
	}

	/**
	 * 获取最新的活动数据
	 *
	 * @param pageNum  页码
	 * @param pageSize 每页条数
	 * @param activity 活动参数
	 * @return
	 * @author wuxianbing
	 */
	@Override
	public PageInfo<ActivityResult> getLastList(int pageNum, int pageSize, ActivityParam activity) {
		PageInfo<ActivityResult> result = null;
		try {
			PageHelper.startPage(pageNum, pageSize);
			List<ActivityResult> list = activityMapper.getLastList(activity);
			result=new PageInfo<>(list);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}

	@Override
	public ActivityResult getDetails(int id) {
		ActivityResult result = null;
		try {
			result = activityMapper.getDetails(id);
		} catch (Error e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	/**
     * 获取发布的活动列表
     * @param pageNum 页码
     * @param pageSize 每页条数
     * @param activity 参数
     * @return
     */
    public PageInfo<ActivityResult> getPublishActivityList(int pageNum, int pageSize, ActivityParam activity){
    	PageInfo<ActivityResult> result = null;
		try {
			PageHelper.startPage(pageNum, pageSize);
			List<ActivityResult> list = activityMapper.getPublishActivityList(activity);
			result=new PageInfo<>(list);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
    }
}

