
/**
 * 
 * Title：Ad
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import simpleframe.model.core.Ad;

public interface AdDao extends BaseDao<Ad,Integer>{

}

