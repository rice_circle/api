
/**
 * 
 * Title：Ad
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.AdMapper;
import simpleframe.model.core.Ad;

@Repository
public class AdDaoImpl extends BaseDaoImpl<Ad,Integer> implements AdDao{

    private static final Logger logger = Logger.getLogger(AdDaoImpl.class);

	@Autowired
    private AdMapper adMapper;

	@Autowired
	public void setAdMapper(AdMapper adMapper) {
		super.setBaseMapper(adMapper);
	}	
}

