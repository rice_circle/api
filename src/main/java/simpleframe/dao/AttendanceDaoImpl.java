
/**
 * 
 * Title：Attendance
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.AttendanceMapper;
import simpleframe.model.core.Attendance;

@Repository
public class AttendanceDaoImpl extends BaseDaoImpl<Attendance,Integer> implements AttendanceDao{

    private static final Logger logger = Logger.getLogger(AttendanceDaoImpl.class);

	@Autowired
    private AttendanceMapper attendanceMapper;

	@Autowired
	public void setAttendanceMapper(AttendanceMapper attendanceMapper) {
		super.setBaseMapper(attendanceMapper);
	}	
}

