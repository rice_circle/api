
/**
 * 
 * Title：Attention
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.AttentionMapper;
import simpleframe.model.core.Attention;

@Repository
public class AttentionDaoImpl extends BaseDaoImpl<Attention,Integer> implements AttentionDao{

    private static final Logger logger = Logger.getLogger(AttentionDaoImpl.class);

	@Autowired
    private AttentionMapper attentionMapper;

	@Autowired
	public void setAttentionMapper(AttentionMapper attentionMapper) {
		super.setBaseMapper(attentionMapper);
	}	
}

