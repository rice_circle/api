package simpleframe.dao;

import java.io.Serializable;
import java.util.List;

import simpleframe.utils.PageUtils.PageInfo;

public interface BaseDao<T, ID extends Serializable> {
	
	/**
	 * 插入实体
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public boolean insert(T model);
	

	/**
	 * 插入返回
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public T insertReturnModel(T model);
		
	/**
	 * 查找实体
	 * 
	 * @author axiwu
	 * @param id
	 * @return
	 */
	public T select(ID id);

	/**
	 * 查找实体
	 * 
	 * @author axiwu
	 * @param model
	 *            （存在多个返回最后的实体）
	 * @return
	 */
	public T selectModel(T model);

	/**
	 * 查找列表
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public List<T> selectList(T model);
	
	/**
	 * 查找全部列表
	 * 
	 * @author axiwu
	 * @return
	 */
	List<T> selectAll();

	/**
	 * 查找确定列表
	 * 
	 * @author axiwu
	 * @param ids
	 * @return
	 */
	public List<T> selectCertainList(ID... ids);

	/**
	 * 查找分页列表
	 * @param model
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	PageInfo<T> selectPage(T model, int pageNum, int pageSize);
	
	/**
	 * 查找数据条数
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public int selectCount(T model);

	/**
	 * 是否存在数据
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public boolean selectExists(T model);


	/**
	 * 更新实体
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public boolean update(T model);

	/**
	 * 更新返回实体
	 * @param model
	 * @return
	 */
	T updateReturnModel(T model);

	/**
	 * 更新实体
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public boolean updateTrim(T model);

	/**
	 * 逻辑删除
	 * 
	 * @author axiwu
	 * @param id
	 * @return
	 */
	boolean deleteSoft(ID id);
	
	/**
	 * 物理删除
	 * 
	 * @author axiwu
	 * @param id
	 * @return
	 */
	boolean deleteHard(ID id);
}
