package simpleframe.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import com.github.pagehelper.PageHelper;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import simpleframe.mapper.BaseMapper;
import simpleframe.model.base.BaseModel;

import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.service.BaseServiceImpl;

public class BaseDaoImpl<T extends BaseModel, ID extends Serializable> implements BaseDao<T, ID> {
	/**
	 * Logger for this class
	 */
	private static Logger logger = Logger.getLogger(BaseServiceImpl.class);

	private Class<T> modelClass;

	public BaseDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) t).getActualTypeArguments();
		Class<T> modelClass = (Class<T>) params[0];
	}
	
	/** baseMapper */
	private BaseMapper<T, ID> baseMapper;

	public void setBaseMapper(BaseMapper<T, ID> baseMapper) {
		this.baseMapper = baseMapper;
	}

	
	@Override
	@Transactional(readOnly = true)
	public T select(ID id) {
		T t = null;
		try {
			t = baseMapper.select(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return t;

	}

	@Override
	@Transactional(readOnly = true)
	public T selectModel(T model) {
		T t = null;
		try {
			List<T> list = baseMapper.selectList(model);
			if (list.size() == 0) {
			} else if (list.size() == 1) {
				t = list.get(0);
			} else {
				t = list.get(list.size() - 1);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return t;
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> selectList(T model) {
		List<T> list = null;

		try {
			list = baseMapper.selectList(model);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return list;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<T> selectAll() {
		List<T> list = null;

		try {
			list = baseMapper.selectList(null);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> selectCertainList(ID... ids) {
		List<T> list = null;
		try {
			list = baseMapper.selectCertainList(ids);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return list;
	}
	
	@Override
	@Transactional(readOnly = true)
	public PageInfo<T> selectPage(T model, int pageNum, int pageSize) {
		PageInfo<T> pageModel = null;
		try {
			PageHelper.startPage(pageNum, pageSize);
			List<T> list=baseMapper.selectList(model);
			pageModel=new PageInfo<>(list);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return pageModel;
	}

	@Override
	@Transactional(readOnly = true)
	public int selectCount(T model) {
		int count = 0;
		try {
			count = baseMapper.selectCount(model);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return count;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean selectExists(T model) {
		boolean flag = false;
		try {
			flag = this.selectModel(model) != null;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return flag;
	}

	@Override
	@Transactional
	public boolean insert(T model) {
		long flag = 0;
		try {
			flag = baseMapper.insert(model);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return flag > 0;
	}

	@Override
	public T insertReturnModel(T model) {
		long flag = 0;
		try {
			flag =baseMapper.insert(model);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		if(flag > 0&&model.getId()>0)
		{
			return model;
		}
		else {
			return null;
		}
	}

	@Override
	@Transactional
	public boolean update(T model) {
		int result = 0;
		try {
			result = baseMapper.update(model);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result > 0;
	}

	@Override
	public T updateReturnModel(T model) {
		int result = 0;
		try {
			result = baseMapper.update(model);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		if (result > 0)
		{
			return model;
		}
		else {
			return null;
		}
	}

	@Override
	@Transactional
	public boolean updateTrim(T model) {
		int result = 0;
		try {
			result = baseMapper.updateTrim(model);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result > 0;
	}

	@Override
	@Transactional
	public boolean deleteSoft(ID id) {
		int result = 0;
		try {
			result = baseMapper.updateDeleted(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result > 0;
	}
	
	@Override
	@Transactional
	public boolean deleteHard(ID id) {
		int result = 0;
		try {
			result = baseMapper.delete(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result > 0;
	}
}