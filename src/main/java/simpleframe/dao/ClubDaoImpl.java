
/**
 * Title：Club
 *
 * @author axi
 * @version 1.0, 2017年07月23日
 * @since 2017年07月23日
 */

package simpleframe.dao;


import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.ClubMapper;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Club;
import simpleframe.model.result.ClubResult;

import java.util.List;

@Repository
public class ClubDaoImpl extends BaseDaoImpl<Club, Integer> implements ClubDao {

    private static final Logger logger = Logger.getLogger(ClubDaoImpl.class);

    @Autowired
    private ClubMapper clubMapper;

    @Autowired
    public void setClubMapper(ClubMapper clubMapper) {
        super.setBaseMapper(clubMapper);
    }

    /**
     * 获取扩展分页列表
     * @param param
     * @return
     */
    @Override
    public PageInfo<ClubResult> getExtendList(Club param, Integer pageNum, Integer pageSize) {
        PageInfo<ClubResult> result = null;
        try {
            PageHelper.startPage(pageNum, pageSize);
            List<ClubResult> list = clubMapper.getExtendList(param);
            result = new PageInfo<>(list);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

	@Override
	public List<Club> selectByMemberId(Integer memberId) {
		return clubMapper.selectByMemberId(memberId);
	}
}

