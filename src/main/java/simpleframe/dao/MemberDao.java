
/**
 * 
 * Title：Member
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import simpleframe.model.core.Member;

public interface MemberDao extends BaseDao<Member,Integer>{

}

