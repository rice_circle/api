
/**
 * 
 * Title：Member
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.MemberMapper;
import simpleframe.model.core.Member;

@Repository
public class MemberDaoImpl extends BaseDaoImpl<Member,Integer> implements MemberDao{

    private static final Logger logger = Logger.getLogger(MemberDaoImpl.class);

	@Autowired
    private MemberMapper memberMapper;

	@Autowired
	public void setMemberMapper(MemberMapper memberMapper) {
		super.setBaseMapper(memberMapper);
	}	
}

