
/**
 * 
 * Title：Orders
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.OrdersMapper;
import simpleframe.model.core.Orders;
import simpleframe.model.result.OrderResult;
import simpleframe.model.result.OrdersResult;

import simpleframe.utils.PageUtils.PageInfo;

import java.util.List;

@Repository
public class OrdersDaoImpl extends BaseDaoImpl<Orders,Integer> implements OrdersDao {

    private static final Logger logger = Logger.getLogger(OrdersDaoImpl.class);

	@Autowired
    private OrdersMapper orderMapper;

	@Autowired
	public void setOrderMapper(OrdersMapper orderMapper) {
		super.setBaseMapper(orderMapper);
	}

    @Override
    public List<OrderResult> getPurchaseRank(Orders param) {
        /**
         * 获取购买排名
         */
        List<OrderResult> result=null;
        try {
            result=orderMapper.getPurchaseRank(param);
        }catch (Exception e)
        {
            logger.info(e.getMessage(),e);
        }
        return result;
    }


    @Override
    public PageInfo<OrdersResult> getExtendList(Orders param, Integer pageNum, Integer pageSize) {
        PageInfo<OrdersResult> result = null;
        try {
            PageHelper.startPage(pageNum, pageSize);
            List<OrdersResult> list = orderMapper.getExtendList(param);
            result = new PageInfo<>(list);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public OrderResult getDetails(Orders param) {
        OrderResult result=null;
        try {
            result=orderMapper.getDetails(param);
        }catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }
    
    /**
     * 根据订单编码查询订单信息
     * @param code 订单编码
     * @return
     */
    @Override
    public Orders getOrderInfoByCode(String code){
    	return orderMapper.getOrderInfoByCode(code);
    }
}

