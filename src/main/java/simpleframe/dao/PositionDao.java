
/**
 * 
 * Title：Position
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.dao;

import simpleframe.model.core.Position;

public interface PositionDao extends BaseDao<Position,Integer>{

}

