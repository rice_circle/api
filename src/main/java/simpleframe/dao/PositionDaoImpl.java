
/**
 * 
 * Title：Position
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.PositionMapper;
import simpleframe.model.core.Position;

@Repository
public class PositionDaoImpl extends BaseDaoImpl<Position,Integer> implements PositionDao{

    private static final Logger logger = Logger.getLogger(PositionDaoImpl.class);

	@Autowired
    private PositionMapper positionMapper;

	@Autowired
	public void setPositionMapper(PositionMapper positionMapper) {
		super.setBaseMapper(positionMapper);
	}	
}

