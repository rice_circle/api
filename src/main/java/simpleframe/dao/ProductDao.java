
/**
 * 
 * Title：Product
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import simpleframe.model.core.Product;

public interface ProductDao extends BaseDao<Product,Integer>{


    /**
     * 修改商品库存
     * @param newModel
     * @return
     */
    boolean changeStockLock(Product newModel);
}

