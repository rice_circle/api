
/**
 * 
 * Title：Product
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import simpleframe.mapper.ProductMapper;
import simpleframe.model.core.Product;

@Repository
public class ProductDaoImpl extends BaseDaoImpl<Product,Integer> implements ProductDao{

    private static final Logger logger = Logger.getLogger(ProductDaoImpl.class);

	@Autowired
    private ProductMapper productMapper;

	@Autowired
	public void setProductMapper(ProductMapper productMapper) {
		super.setBaseMapper(productMapper);
    }

    @Override
    public boolean changeStockLock(Product newModel) {
	    if(newModel==null)
        {
            return false;
        }
	    if(newModel.getLockStock()==null&&newModel.getUsableStock()==null&&newModel.getTotalStock()==null)
        {
            return false;
        }
        int result = 0;
        try {
           result=productMapper.changeStockLock(newModel);
        } catch (Exception e) {
            logger.info(e.getMessage(),e);
        }

        return result>0;
    }
}

