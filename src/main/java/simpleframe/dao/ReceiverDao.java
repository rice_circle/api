
/**
 * 
 * Title：Receiver
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.dao;

import simpleframe.model.core.Receiver;

public interface ReceiverDao extends BaseDao<Receiver,Integer>{

	/**
	 * 根据用户id获取其默认地址
	 * @param memberId 用户id
	 * @return
	 */
	Receiver getDefaultReceiverByMemberId(Integer memberId);
	
	/**
	 * 设置默认地址
	 * @param memberId 用户id
	 * @param isDefault 是否为默认地址
	 * @param id 记录id
	 * @return
	 */
	boolean setDefaultReceiver(Integer memberId,boolean isDefault,Integer id);
}

