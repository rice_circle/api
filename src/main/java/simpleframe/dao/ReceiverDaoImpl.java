
/**
 * 
 * Title：Receiver
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import simpleframe.mapper.ReceiverMapper;
import simpleframe.model.core.Receiver;

@Repository
public class ReceiverDaoImpl extends BaseDaoImpl<Receiver,Integer> implements ReceiverDao{

    private static final Logger logger = Logger.getLogger(ReceiverDaoImpl.class);

	@Autowired
    private ReceiverMapper receiverMapper;

	@Autowired
	public void setReceiverMapper(ReceiverMapper receiverMapper) {
		super.setBaseMapper(receiverMapper);
	}	
	
	/**
	 * 根据用户id获取其默认地址
	 * @param memberId 用户id
	 * @return
	 */
	@Override
	public Receiver getDefaultReceiverByMemberId(Integer memberId){
		return receiverMapper.queryDefaultReceiverByMemberId(memberId);
	}
	
	/**
	 * 设置默认地址
	 * @param memberId 用户id
	 * @param isDefault 是否为默认地址
	 * @param id 记录id
	 * @return
	 */
	@Override
	public boolean setDefaultReceiver(Integer memberId,boolean isDefault,Integer id){
		return receiverMapper.updateDefaultReceiver(memberId, isDefault ? 1 : 0,id);
	}
}

