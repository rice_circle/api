
/**
 * 
 * Title：ShortMessage
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import java.util.List;

import simpleframe.model.core.ShortMessage;

public interface ShortMessageDao extends BaseDao<ShortMessage,Integer>{
	
	/**
	 * 根据手机号查看验证码信息
	 * @param phone 手机号
	 * @return
	 */
	public List<ShortMessage> selectByPhone(String phone);
	
	/**
	 * 根据手机号查看有效的验证码信息
	 * @param phone 手机号
	 * @return
	 */
	public ShortMessage selectEffectiveByPhone(String phone);
}

