
/**
 * 
 * Title：ShortMessage
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import simpleframe.mapper.ShortMessageMapper;
import simpleframe.model.core.ShortMessage;

@Repository
public class ShortMessageDaoImpl extends BaseDaoImpl<ShortMessage,Integer> implements ShortMessageDao{

    private static final Logger logger = Logger.getLogger(ShortMessageDaoImpl.class);

	@Autowired
    private ShortMessageMapper shortMessageMapper;

	@Autowired
	public void setShortMessageMapper(ShortMessageMapper shortMessageMapper) {
		super.setBaseMapper(shortMessageMapper);
	}

	@Override
	public List<ShortMessage> selectByPhone(String phone) {
		return shortMessageMapper.selectByPhone(phone);
	}	
	
	/**
	 * 根据手机号查看有效的验证码信息
	 * @param phone 手机号
	 * @return
	 */
	public ShortMessage selectEffectiveByPhone(String phone){
		List<ShortMessage> result = shortMessageMapper.selectEffectiveByPhone(phone);
		if(result == null){
			return null;
		}
		if(result.size() > 0){
			return result.get(0);
		}
		return null;
	}
}

