
/**
 * 
 * Title：Star
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Star;
import simpleframe.model.param.StarParam;
import simpleframe.model.result.StarResult;

public interface StarDao extends BaseDao<Star,Integer>{

    /**
     * 获取明星列表
     * @param param
     * @return
     */
    PageInfo<StarResult> getExtendList(int pageNum, int pageSize, StarParam param);

    /**
     * 获取明星详情
     * @param param
     * @return
     */
    StarResult getDetails(StarParam param);
}

