
/**
 * 
 * Title：Star
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;


import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.StarMapper;

import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Star;
import simpleframe.model.param.StarParam;
import simpleframe.model.result.StarResult;

import java.util.List;

@Repository
public class StarDaoImpl extends BaseDaoImpl<Star,Integer> implements StarDao{

    private static final Logger logger = Logger.getLogger(StarDaoImpl.class);

	@Autowired
    private StarMapper starMapper;

	@Autowired
	public void setStarMapper(StarMapper starMapper) {
		super.setBaseMapper(starMapper);
	}

    @Override
    public PageInfo<StarResult> getExtendList(int pageNum, int pageSize, StarParam param) {
        PageInfo<StarResult> result = null;
        try {
            PageHelper.startPage(pageNum, pageSize);
            List<StarResult> list = starMapper.getExtendList(param);
            result=new PageInfo<>(list);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public StarResult getDetails(StarParam param) {
        return starMapper.getDetails(param);
    }
}

