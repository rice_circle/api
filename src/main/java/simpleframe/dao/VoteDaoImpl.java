
/**
 * 
 * Title：Vote
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.VoteMapper;
import simpleframe.model.core.Vote;

@Repository
public class VoteDaoImpl extends BaseDaoImpl<Vote,Integer> implements VoteDao{

    private static final Logger logger = Logger.getLogger(VoteDaoImpl.class);

	@Autowired
    private VoteMapper voteMapper;

	@Autowired
	public void setVoteMapper(VoteMapper voteMapper) {
		super.setBaseMapper(voteMapper);
	}	
}

