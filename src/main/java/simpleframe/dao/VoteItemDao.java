
/**
 * 
 * Title：VoteItem
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import simpleframe.model.core.VoteItem;

public interface VoteItemDao extends BaseDao<VoteItem,Integer>{

}

