
/**
 * 
 * Title：VoteItem
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.VoteItemMapper;
import simpleframe.model.core.VoteItem;

@Repository
public class VoteItemDaoImpl extends BaseDaoImpl<VoteItem,Integer> implements VoteItemDao{

    private static final Logger logger = Logger.getLogger(VoteItemDaoImpl.class);

	@Autowired
    private VoteItemMapper voteItemMapper;

	@Autowired
	public void setVoteItemMapper(VoteItemMapper voteItemMapper) {
		super.setBaseMapper(voteItemMapper);
	}	
}

