
/**
 * 
 * Title：VoteRecord
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.apache.log4j.Logger;
import simpleframe.mapper.VoteRecordMapper;
import simpleframe.model.core.VoteRecord;

@Repository
public class VoteRecordDaoImpl extends BaseDaoImpl<VoteRecord,Integer> implements VoteRecordDao{

    private static final Logger logger = Logger.getLogger(VoteRecordDaoImpl.class);

	@Autowired
    private VoteRecordMapper voteRecordMapper;

	@Autowired
	public void setVoteRecordMapper(VoteRecordMapper voteRecordMapper) {
		super.setBaseMapper(voteRecordMapper);
	}	
}

