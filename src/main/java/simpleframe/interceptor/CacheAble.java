package simpleframe.interceptor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CacheAble {
	
	public enum KeyMode{
		/**只有加了@CacheKey的参数,才加入key后缀中*/
		DEFAULT,	
		/**只有基本类型参数,才加入key后缀中,如:String,Integer,Long,Short,Boolean*/
		BASIC,
		/**所有参数都加入key后缀*/
		ALL;		
	}
	
	public String group() default "";//组
	public String key() default "";//缓存key
	public int expire() default 0;//缓存多少秒,默认无限期
	public KeyMode keyMode() default KeyMode.DEFAULT;//key的后缀模式
}