package simpleframe.interceptor;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import simpleframe.interceptor.CacheAble.KeyMode;
import simpleframe.utils.RedisHandleUtils;

import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class CacheAspect {

	@Around("@annotation(cacheAble)")
	public Object cacheableHandle(final ProceedingJoinPoint pjp, CacheAble cacheAble) throws Throwable {

		String group = getGroup(pjp, cacheAble.group());
		String key = group.concat(getCacheKey(pjp, cacheAble));

		Object value = RedisHandleUtils.getCache(key); // 从缓存获取数据
		if (value != null)
			return value; // 如果有数据,则直接返回
		value = (Object) pjp.proceed(); // 跳过缓存,到后端查询数据
		if (cacheAble.expire() <= 0) { // 如果没有设置过期时间,则缓存一个月
			RedisHandleUtils.setPermanentCache(key, value);
		} else { // 否则设置缓存时间
			RedisHandleUtils.setCache(key, value, (long) cacheAble.expire(), TimeUnit.SECONDS);
		}
		RedisHandleUtils.refreshGroupKeys(group, key);
		return value;
	}

	/**
	 * 注解清除缓存
	 * @param pjp
	 * @param cacheEvict
	 * @return
	 * @throws Throwable
	 */
	@Around("@annotation(cacheEvict)")
	public Object cacheEvictHandle(final ProceedingJoinPoint pjp, CacheEvict cacheEvict) throws Throwable {
		Object value = (Object) pjp.proceed();
		String group = getGroup(pjp, cacheEvict.group());
		RedisHandleUtils.deleteGroup(group);
		// 跳过缓存,到后端查询数据
		return value;
	}

	private String getGroup(ProceedingJoinPoint pjp, String group) {
		StringBuilder buf = new StringBuilder();
		if (group != null && group.length() > 0) {
			buf.append(group);
		} else {
			buf.append(pjp.getSignature().getDeclaringTypeName()).append(".").append(pjp.getSignature().getName());
		}
		return buf.toString();

	}

	/**
	 * 获取缓存的key值
	 * 
	 * @param pjp
	 * @param cacheAble
	 * @return
	 */
	private String getCacheKey(ProceedingJoinPoint pjp, CacheAble cacheAble) {
		StringBuilder buf = new StringBuilder();

		if (cacheAble.key().length() > 0) {
			buf.append(".").append(cacheAble.key());
		}

		Object[] args = pjp.getArgs();
		if (cacheAble.keyMode() == KeyMode.DEFAULT) {
			Annotation[][] pas = ((MethodSignature) pjp.getSignature()).getMethod().getParameterAnnotations();
			for (int i = 0; i < pas.length; i++) {
				for (Annotation an : pas[i]) {
					if (an instanceof CacheKey) {
						buf.append(".").append(args[i].toString());
						break;
					}
				}
			}
		} else if (cacheAble.keyMode() == KeyMode.BASIC) {
			for (Object arg : args) {
				if (arg instanceof String) {
					buf.append(".").append(arg);
				} else if (arg instanceof Integer || arg instanceof Long || arg instanceof Short) {
					buf.append(".").append(arg.toString());
				} else if (arg instanceof Boolean) {
					buf.append(".").append(arg.toString());
				}
			}
		} else if (cacheAble.keyMode() == KeyMode.ALL) {
			for (Object arg : args) {
				buf.append(".").append(arg.toString());
			}
		}
		return buf.toString();
	}
}