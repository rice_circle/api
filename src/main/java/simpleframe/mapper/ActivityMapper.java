
/**
 *
 * Title：Activity
 * @author axi
 * @version 1.0, 2017年07月20日
 * @since 2017年07月20日
 */

package simpleframe.mapper;

import java.util.List;

import simpleframe.model.core.Activity;
import simpleframe.model.result.ActivityResult;

public interface ActivityMapper extends BaseMapper<Activity,Integer>{

    /**
     * 获取最新的活动列表
     * @param activity
     * @return
     */
    List<ActivityResult> getLastList(Activity activity);

    /**
     * 获取活动详情
     *
     * @param id 活动Id
     * @return
     * @author wuxianbing
     */
    ActivityResult getDetails(int id);
    
    /**
     * 获取发布列表
     * @param activity
     * @return
     */
    List<ActivityResult> getPublishActivityList(Activity activity);
}

