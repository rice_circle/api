package simpleframe.mapper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface BaseMapper<T,ID extends Serializable> {

	/**
	 * 插入实体
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public int insert(T model);

	/**
	 * 更新实体
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public int update(T model);

	/**
	 * 更新实体,值为null的不更新
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public int updateTrim(T model);

	/**
	 * 查找实体
	 * @author axiwu
	 * @param id
	 * @return
	 */
	public T select(ID id);

	/**
	 * 查找实体
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public T selectModel(T model);

	/**
	 * 查找列表
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public List<T> selectList(T model);

	/**
	 * 查找确定列表
	 * @author axiwu
	 * @param ids
	 * @return
	 */
	public List<T> selectCertainList(ID... ids);

	/**
	 * 查找数据条数
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public int selectCount(T model);

	/**
	 * 逻辑删除
	 * @author axiwu
	 * @param id
	 * @return
	 */
	public int updateDeleted(ID id);

	/**
	 * 物理删除
	 * @author axiwu
	 * @param id
	 * @return
	 */
	public int delete(ID id);
}
