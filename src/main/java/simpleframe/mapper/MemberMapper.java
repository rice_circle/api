
/**
 * 
 * Title：Member
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.mapper;

import java.util.List;

import simpleframe.model.core.Member;

public interface MemberMapper extends BaseMapper<Member,Integer>{

}

