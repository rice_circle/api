
/**
 * 
 * Title：Orders
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.mapper;

import java.util.List;

import simpleframe.model.core.Orders;
import simpleframe.model.result.OrderResult;
import simpleframe.model.result.OrdersResult;
import simpleframe.utils.PageUtils.PageInfo;

public interface OrdersMapper extends BaseMapper<Orders,Integer>{

    /**
     * 获取购买排名
     * @param param
     * @return
     */
    List<OrderResult> getPurchaseRank(Orders param);

    /**
     * 获取订单列表
     * @param inParam
     * @return
     */
    List<OrdersResult> getExtendList(Orders param);

    /**
     * 获取订单详情
     * @param param
     * @return
     */
    OrderResult getDetails(Orders param);
    
    /**
     * 根据订单编码查询订单信息
     * @param code 订单编码
     * @return
     */
    public Orders getOrderInfoByCode(String code);
}

