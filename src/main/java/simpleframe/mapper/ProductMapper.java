
/**
 * 
 * Title：Product
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.mapper;

import java.util.List;

import simpleframe.model.core.Product;

public interface ProductMapper extends BaseMapper<Product,Integer>{

    /**
     * 更改库存
     * @param newModel
     * @return
     */
    int changeStockLock(Product newModel);
}

