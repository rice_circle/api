
/**
 * 
 * Title：Receiver
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.mapper;

import java.util.List;

import simpleframe.model.core.Receiver;

public interface ReceiverMapper extends BaseMapper<Receiver,Integer>{
	
	/**
	 * 根据用户id获取其默认地址
	 * @param memberId 用户id
	 * @return
	 */
	Receiver queryDefaultReceiverByMemberId(Integer memberId);
	
	/**
	 * 设置默认地址
	 * @param memberId 用户id
	 * @param isDefault 是否为默认地址(0--不是 1--是)
	 * @param id 记录id
	 * @return
	 */
	boolean updateDefaultReceiver(Integer memberId,Integer isDefault,Integer id);
}

