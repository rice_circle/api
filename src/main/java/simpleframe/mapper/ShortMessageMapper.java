
/**
 * 
 * Title：ShortMessage
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.mapper;

import java.util.List;

import simpleframe.model.core.ShortMessage;

public interface ShortMessageMapper extends BaseMapper<ShortMessage,Integer>{
	/**
	 * 根据手机号查看验证码信息
	 * @param phone 手机号
	 * @return
	 */
	public List<ShortMessage> selectByPhone(String phone);
	
	/**
	 * 根据手机号查看有效的验证码信息（去掉已删除、已过期、已使用的）
	 * @param phone 手机号
	 * @return
	 */
	public List<ShortMessage> selectEffectiveByPhone(String phone);
}

