
/**
 * 
 * Title：Star
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.mapper;

import java.util.List;

import simpleframe.model.core.Star;
import simpleframe.model.param.StarParam;
import simpleframe.model.result.StarResult;

public interface StarMapper extends BaseMapper<Star,Integer>{

    /**
     * 获取明星列表
     * @param param
     * @return
     */
    List<StarResult> getExtendList(StarParam param);

    /**
     * 获取明星详情
     * @param param
     * @return
     */
    StarResult getDetails(StarParam param);
}

