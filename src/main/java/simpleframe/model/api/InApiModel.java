package simpleframe.model.api;

/**
 * @author:axi
 * @date:2017/8/5
 */
public class InApiModel<T extends Object> {
    /**
     * 登录状态
     */
    private String accessToken;

    /**
     * 版本号
     */
    private String appVersion;

    /**
     * 系统
     */
    private String system;

    /**
     * 系统
     */
    private String systemVersion;


    /**
     * 数据
     */
    private T data;

    public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    
    public Integer getLoginId(){
    	return Integer.valueOf(accessToken);
    }
}
