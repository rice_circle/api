package simpleframe.model.api;

import simpleframe.model.base.BaseModel;

/**
 * @author:axi
 * @date:2017/8/5
 */
public class InApiPageModel<T extends Object> extends InApiModel<T>{
    /**
     * 分页页码
     */
    private Integer pageNum;

    /**
     * 分页大小
     */
    private Integer pageSize;

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
