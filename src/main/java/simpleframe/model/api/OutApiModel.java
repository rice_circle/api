package simpleframe.model.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE)
public class OutApiModel {
	
	/** 成功状态*/
	@JsonProperty
	private boolean success;
	
	/** 返回代码*/
	@JsonProperty
	private String code;
	
	/** 返回消息*/
	@JsonProperty
	private String message;
	
	/** 返回数据*/
	@JsonProperty
	private Object data;
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public OutApiModel init(OutApiCode OutApiCode)
	{
		return init(false,OutApiCode,null);
	}

	public OutApiModel init(boolean success,OutApiCode OutApiCode)
	{
		return init(success,OutApiCode,null);
	}

	public OutApiModel init(boolean success,OutApiCode OutApiCode,Object data)
	{
		this.success=success;
		this.code=OutApiCode.getValue();
		this.message=OutApiCode.getText();
		this.data=data;
		return this;
	}
}
