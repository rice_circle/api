package simpleframe.model.base;

import simpleframe.utils.EnumUtils.BaseEnum;

public enum ActivityType implements BaseEnum<ActivityType,Integer> {

    YINGYUAN(0, "应援"),
    GONGYI(1, "公益"),
    SHANGPIN(2, "商品")

    ;//结束标记
    ActivityType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }

}
