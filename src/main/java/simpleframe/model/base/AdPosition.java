package simpleframe.model.base;

import simpleframe.utils.EnumUtils.BaseEnum;

/**
 * @description:
 * @author:axi
 * @date:2017/7/22
 */
public enum AdPosition implements BaseEnum<AdPosition,String> {

    /**
     * 首页顶部第一栏
     */
    AD_000001("AD_000001", "首页顶部第一栏广告位")

    ;//结束标记
    AdPosition(String value, String text) {
        this.value = value;
        this.text = text;
    }

    private String value;

    private String text;

    @Override
    public String getValue() {

        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
