/*
 * 天虹商场股份有限公司版权所有.
 */
package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * 收获地址默认
 *
 * @author:wuxianbing
 * @date:2017/7/28
 */
public enum AddressDefault implements EnumUtils.BaseEnum<AddressDefault, Integer> {

    NO(0, "否"), YES(1, "是");//结束标记

    AddressDefault(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
