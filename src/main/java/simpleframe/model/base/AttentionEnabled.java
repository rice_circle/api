/*
 * 天虹商场股份有限公司版权所有.
 */
package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * 关注启用
 * @author:wuxianbing
 * @date:2017/7/28
 */
public enum AttentionEnabled implements EnumUtils.BaseEnum<AttentionEnabled, Integer>{
    NO(0, "否"), YES(1, "是");//结束标记

    AttentionEnabled(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
