package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * 关联类型
 * @author:axi
 * @date:2017/7/23
 */
public enum AttentionRelationType implements EnumUtils.BaseEnum<AttentionRelationType,Integer> {

    /**
     * 明星
     */
    STAR(0,"明星")

    /**
     * 粉丝团
     */
    ,CLUB(1,"粉丝团")

    /**
     * 粉丝团
     */
    ,ACTIVITY(2,"活动")


    ;//结束标记
    AttentionRelationType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
