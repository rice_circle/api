package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * 审核状态
 *
 * @author:axi
 * @date:2017/8/2
 */
public enum AuditState implements EnumUtils.BaseEnum<AuditState, Integer> {

    AUDITING(0, "待审核"), AUDITYES(1, "有效"), AUDITNO(2, "无效");//结束标记

    AuditState(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
