package simpleframe.model.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = Visibility.NONE, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE, creatorVisibility = Visibility.NONE)
public abstract class BaseModel<T> implements Serializable {

	/**
	 * @author axiwu
	 */
	private static final long serialVersionUID = -5937891419564118089L;

	public BaseModel()
	{
		this.isDeleted=DeleteType.NORMAL.getValue();
	}

	/** ID */
	@JsonProperty
	private Integer id;

	/**创建时间*/
	private Date createTime;

	/**更新时间*/
	private Date updateTime;

	/**删除标记（0正常,1删除）*/
	private Integer isDeleted;

	/**排序条件 */
	private String order;

	/**
	 * 保存验证组
	 */
	public interface Save extends Default {

	}

	/**
	 * 更新验证组
	 */
	public interface Update extends Default {

	}

	/**排序条件 */
	public String getOrder() {
		return order;
	}
	/**排序条件 */
	public void setOrder(String order) {
		this.order = order;
	}

	/**
	 * 获取ID
	 * 
	 * @return ID
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置ID
	 * 
	 * @param id
	 *            ID
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**获取创建时间*/
	@JsonProperty
	public Date getCreateTime()
	{
		return this.createTime;
	}

	/**设置创建时间*/
	public void setCreateTime(Date createTime)
	{
		this.createTime=createTime;
	}


	/**获取更新时间*/
	@JsonProperty
	public Date getUpdateTime()
	{
		return this.updateTime;
	}

	/**设置更新时间*/
	public void setUpdateTime(Date updateTime)
	{
		this.updateTime=updateTime;
	}


	/**获取删除标记（0正常,1删除）*/
	@JsonProperty
	@NotNull(message="删除标记不能为空",groups = {Default.class,Save.class})
	public Integer getIsDeleted()
	{
		return this.isDeleted;
	}

	/**设置删除标记（0正常,1删除）*/
	public void setIsDeleted(Integer isDeleted)
	{
		this.isDeleted=isDeleted;
	}


	/**
	 * 重写equals方法
	 * 
	 * @param obj
	 *            对象
	 * @return 是否相等
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if (!BaseModel.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		BaseModel other = (BaseModel) obj;
		return getId() != null ? getId().equals(other.getId()) : false;
	}

	/**
	 * 重写hashCode方法
	 * 
	 * @return hashCode
	 */
	@Override
	public int hashCode() {
		int hashCode = 17;
		hashCode += null == getId() ? 0 : getId().hashCode() * 31;
		return hashCode;
	}

	public T deepClone() throws IOException, ClassNotFoundException {
		// 将对象写到流里
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		ObjectOutputStream oo = new ObjectOutputStream(bo);
		oo.writeObject(this);

		// 从流里读出来
		ByteArrayInputStream bi = new ByteArrayInputStream(bo.toByteArray());
		ObjectInputStream oi = new ObjectInputStream(bi);
		return (T) oi.readObject();
	}


}
