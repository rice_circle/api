package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * @author:axi
 * @date:2017/7/24
 */
public enum CetificatedType implements EnumUtils.BaseEnum<CetificatedType, Integer> {
    NO(0, "未认证"), YES(1, "已认证");//结束标记

    CetificatedType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
