package simpleframe.model.base;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import java.math.BigDecimal;

/**
 * @author:axi
 * @date:2017/7/27
 */
public class Constant {

    public static final BigDecimal DELIVERY_FEE=new BigDecimal("15");
}
