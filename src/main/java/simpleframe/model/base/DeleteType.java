package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * @description:
 * @author:axi
 * @date:2017/7/23
 */
public enum DeleteType implements EnumUtils.BaseEnum<DeleteType, Integer> {

    NORMAL(0, "正常")
    ,DELETED(1,"删除")

    ;//结束
    DeleteType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }

}
