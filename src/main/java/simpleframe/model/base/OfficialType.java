package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * 官方状态
 * @author:axi
 * @date:2017/8/2
 */
public enum OfficialType implements EnumUtils.BaseEnum<OfficialType, Integer> {
    NO(0, "否"), YES(1, "是");//结束标记

    OfficialType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
