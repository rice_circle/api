package simpleframe.model.base;

import simpleframe.utils.EnumUtils;

/**
 * 订单状态
 * @author:axi
 * @date:2017/7/27
 */
public enum OrdersStatus implements  EnumUtils.BaseEnum<OrdersStatus, Integer> {
    PAYING(0,"待支付")
    ,PAID(1,"已支付")
    ,CANCEL(2,"已取消")

    ;//结束

    OrdersStatus(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }
}
