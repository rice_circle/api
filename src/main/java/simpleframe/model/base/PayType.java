package simpleframe.model.base;

import simpleframe.utils.EnumUtils.BaseEnum;

/**
 * 支付类型
 * @author sunjichang
 * @date 2017年7月25日下午2:00:48
 * @version 1.0
 * @upate 
 */
public enum PayType implements BaseEnum<PayType, Integer>{
	//支付宝支付
	ALIPAY(0,"alipay")
	;

	PayType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;
	
	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public String getText() {
		return text;
	}
}
