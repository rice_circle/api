package simpleframe.model.base;

import simpleframe.utils.EnumUtils.BaseEnum;

/**
 * 短信使用状态枚举类
 * @author sunjichang
 * @date 2017年7月25日下午1:42:22
 * @version 1.0
 * @upate 
 */
public enum ShortMessageState implements BaseEnum<ShortMessageState, Integer>{
	USED(0,"已使用"),
	UNUSED(1,"未使用"),
	UNCREATE(2,"不存在"),
	EXPIRED(3,"过期"),
	DELETED(4,"已删除"),
	ERROR(5,"错误"),
	NORMAL(6,"正常")
	;

	ShortMessageState(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;
	
	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public String getText() {
		return text;
	}

}
