package simpleframe.model.base;

import simpleframe.utils.EnumUtils.BaseEnum;

/**
 * 短信验证码类型
 * @author sunjichang
 * @date 2017年7月25日上午11:37:40
 * @version 1.0
 * @upate 
 */
public enum ShortMessageType implements BaseEnum<ShortMessageType, Integer>{
	REGISTER(0,"注册"),
	UPDATE_PASSWORD(1,"修改密码")
	;

	ShortMessageType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;
	
	@Override
	public Integer getValue() {
		return value;
	}

	@Override
	public String getText() {
		return text;
	}

}
