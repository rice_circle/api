package simpleframe.model.base;

import simpleframe.utils.EnumUtils.*;

/**
 * @description:
 * @author:axi
 * @date:2017/7/23
 */
public enum StarType implements BaseEnum<StarType, Integer> {

    INDIVIDUAL (0, "独立"),
    TEAM(1, "团体")

    ;//结束标记
    StarType(int value, String text) {
        this.value = value;
        this.text = text;
    }

    private int value;

    private String text;

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String getText() {
        return this.text;
    }

}
