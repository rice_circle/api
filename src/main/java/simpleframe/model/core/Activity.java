/**
 * 
 * Title：Activity
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/** Activity */
public class Activity extends BaseModel<Activity> {

	// 自动生成区域开始
	private static final long serialVersionUID = 5110851501222189217L;

	/** 会员id */
	private Integer memberId;

	/** 粉丝圈id */
	private Integer clubId;

	/** 关联明星id */
	private Integer starId;

	/** 活动名称 */
	private String name;

	/** 封面 */
	private String cover;

	/** 活动详情 */
	private String details;

	/** 关注人数 */
	private Integer attentionNum;

	/** 购买人数 */
	private Integer purchaseNum;

	/** 类型（0应援,1公益,2商品） */
	private Integer activityType;

	/** 目标金额 */
	private BigDecimal targetMoney;

	/** 已筹金额 */
	private BigDecimal raisedMoney;

	/** 开始时间 */
	private Date startTime;

	/** 结束时间 */
	private Date endTime;
	/** 审核状态（0待审核,1审核成功,2审核失败） */
	private Integer auditState;

	/** 获取会员id */
	@JsonProperty
	@NotNull(message = "会员id不能为空", groups = { Default.class, Save.class })
	public Integer getMemberId() {
		return this.memberId;
	}

	/** 设置会员id */
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	/** 获取粉丝圈id */
	@JsonProperty
	@NotNull(message = "粉丝圈id不能为空", groups = { Default.class, Save.class })
	public Integer getClubId() {
		return this.clubId;
	}

	/** 设置粉丝圈id */
	public void setClubId(Integer clubId) {
		this.clubId = clubId;
	}

	/** 获取关联明星id */
	@JsonProperty
	@NotNull(message = "关联明星id不能为空", groups = { Default.class, Save.class })
	public Integer getStarId() {
		return this.starId;
	}

	/** 设置关联明星id */
	public void setStarId(Integer starId) {
		this.starId = starId;
	}

	/** 获取活动名称 */
	@JsonProperty
	@Length(message = "活动名称最大长度不能超过20", max = 20, groups = { Default.class, Save.class })
	@NotEmpty(message = "活动名称不能为空", groups = { Default.class, Save.class })
	public String getName() {
		return this.name;
	}

	/** 设置活动名称 */
	public void setName(String name) {
		this.name = name;
	}

	/** 获取封面 */
	@JsonProperty
	@Length(message = "封面最大长度不能超过20", max = 20, groups = { Default.class, Save.class })
	@NotEmpty(message = "封面不能为空", groups = { Default.class, Save.class })
	public String getCover() {
		return this.cover;
	}

	/** 设置封面 */
	public void setCover(String cover) {
		this.cover = cover;
	}

	/** 获取活动详情 */
	@JsonProperty
	@Length(message = "活动详情最大长度不能超过255", max = 255, groups = { Default.class, Save.class })
	@NotEmpty(message = "活动详情不能为空", groups = { Default.class, Save.class })
	public String getDetails() {
		return this.details;
	}

	/** 设置活动详情 */
	public void setDetails(String details) {
		this.details = details;
	}

	/** 获取关注人数 */
	@JsonProperty
	@NotNull(message = "关注人数不能为空", groups = { Default.class, Save.class })
	public Integer getAttentionNum() {
		return this.attentionNum;
	}

	/** 设置关注人数 */
	public void setAttentionNum(Integer attentionNum) {
		this.attentionNum = attentionNum;
	}

	/** 获取购买人数 */
	@JsonProperty
	@NotNull(message = "购买人数不能为空", groups = { Default.class, Save.class })
	public Integer getPurchaseNum() {
		return this.purchaseNum;
	}

	/** 设置购买人数 */
	public void setPurchaseNum(Integer purchaseNum) {
		this.purchaseNum = purchaseNum;
	}

	/** 获取类型（0应援,1公益,2商品） */
	@JsonProperty
	@NotNull(message = "类型不能为空", groups = { Default.class, Save.class })
	public Integer getActivityType() {
		return this.activityType;
	}

	/** 设置类型（0应援,1公益,2商品） */
	public void setActivityType(Integer activityType) {
		this.activityType = activityType;
	}

	/** 获取目标金额 */
	@JsonProperty
	@NotNull(message = "目标金额不能为空", groups = { Default.class, Save.class })
	public BigDecimal getTargetMoney() {
		return this.targetMoney;
	}

	/** 设置目标金额 */
	public void setTargetMoney(BigDecimal targetMoney) {
		this.targetMoney = targetMoney;
	}

	/** 获取已筹金额 */
	@JsonProperty
	@NotNull(message = "已筹金额不能为空", groups = { Default.class, Save.class })
	public BigDecimal getRaisedMoney() {
		return this.raisedMoney;
	}

	/** 设置已筹金额 */
	public void setRaisedMoney(BigDecimal raisedMoney) {
		this.raisedMoney = raisedMoney;
	}

	/** 获取开始时间 */
	@JsonProperty
	@NotNull(message = "开始时间不能为空", groups = { Default.class, Save.class })
	public Date getStartTime() {
		return this.startTime;
	}

	/** 设置开始时间 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/** 获取结束时间 */
	@JsonProperty
	@NotNull(message = "结束时间不能为空", groups = { Default.class, Save.class })
	public Date getEndTime() {
		return this.endTime;
	}

	/** 设置结束时间 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getAuditState() {
		return auditState;
	}

	public void setAuditState(Integer auditState) {
		this.auditState = auditState;
	}

	// 自动生成区域结束

}
