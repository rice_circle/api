/**
 * 
 * Title：Ad
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Ad*/
 public class Ad extends BaseModel<Ad>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 5541771501222189422L;

  /**广告位置*/
  private String adPosition;

  /**广告类型（0活动,1投票,2外部链接）*/
  private Integer adType;

  /**关联id*/
  private Integer relationId;

  /**图片*/
  private String image;

  /**链接*/
  private String url;


  
  /**获取广告位置*/  @JsonProperty  @Length(message="广告位置最大长度不能超过50",max =50,groups = {Default.class,Save.class})  @NotEmpty(message="广告位置不能为空",groups = {Default.class,Save.class})
  public String getAdPosition()
  {
   return this.adPosition;
  }

  /**设置广告位置*/
  public void setAdPosition(String adPosition)
  {
    this.adPosition=adPosition;
  }

  
  /**获取广告类型（0活动,1投票,2外部链接）*/  @JsonProperty  @NotNull(message="广告类型不能为空",groups = {Default.class,Save.class})
  public Integer getAdType()
  {
   return this.adType;
  }

  /**设置广告类型（0活动,1投票,2外部链接）*/
  public void setAdType(Integer adType)
  {
    this.adType=adType;
  }

  
  /**获取关联id*/  @JsonProperty
  public Integer getRelationId()
  {
   return this.relationId;
  }

  /**设置关联id*/
  public void setRelationId(Integer relationId)
  {
    this.relationId=relationId;
  }

  
  /**获取图片*/  @JsonProperty  @Length(message="图片最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getImage()
  {
   return this.image;
  }

  /**设置图片*/
  public void setImage(String image)
  {
    this.image=image;
  }

  
  /**获取链接*/  @JsonProperty  @Length(message="链接最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getUrl()
  {
   return this.url;
  }

  /**设置链接*/
  public void setUrl(String url)
  {
    this.url=url;
  }

  //自动生成区域结束
 }

