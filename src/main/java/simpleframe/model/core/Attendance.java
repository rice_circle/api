/**
 * 
 * Title：Attendance
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Attendance*/
 public class Attendance extends BaseModel<Attendance>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 7094431501222190071L;

  /**会员id*/
  private Integer memberId;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  //自动生成区域结束
 }

