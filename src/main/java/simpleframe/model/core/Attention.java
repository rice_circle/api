/**
 * 
 * Title：Attention
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Attention*/
 public class Attention extends BaseModel<Attention>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 1724061501233388683L;

  /**会员id*/
  private Integer memberId;

  /**类型（0明星,1粉丝团,2活动）*/
  private Integer relationType;

  /**关联id*/
  private Integer relationId;

  /**是否启用*/
  private Integer isEnabled;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取类型（0明星,1粉丝团,2活动）*/  @JsonProperty  @NotNull(message="类型不能为空",groups = {Default.class,Save.class})
  public Integer getRelationType()
  {
   return this.relationType;
  }

  /**设置类型（0明星,1粉丝团,2活动）*/
  public void setRelationType(Integer relationType)
  {
    this.relationType=relationType;
  }

  
  /**获取关联id*/  @JsonProperty  @NotNull(message="关联id不能为空",groups = {Default.class,Save.class})
  public Integer getRelationId()
  {
   return this.relationId;
  }

  /**设置关联id*/
  public void setRelationId(Integer relationId)
  {
    this.relationId=relationId;
  }

  
  /**获取是否启用*/  @JsonProperty  @NotNull(message="是否启用不能为空",groups = {Default.class,Save.class})
  public Integer getIsEnabled()
  {
   return this.isEnabled;
  }

  /**设置是否启用*/
  public void setIsEnabled(Integer isEnabled)
  {
    this.isEnabled=isEnabled;
  }

  //自动生成区域结束
 }

