/**
 * 
 * Title：Club
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Club*/
 public class Club extends BaseModel<Club>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 7067241501222190383L;

  /**会员id*/
  private Integer memberId;

  /**粉丝团名称*/
  private String name;

  /**粉丝团类型（0大吧,1个站,2饭拍,3饭汇）*/
  private Integer type;

  /**关注明星*/
  private Integer starId;

  /**认证类型（0粉丝团,1娱乐机构）*/
  private Integer certifyType;

  /**负责人名称*/
  private String principalName;

  /**负责人职务*/
  private String principalPosition;

  /**手机号码*/
  private String mobile;

  /**微博*/
  private String weibo;

  /**微信*/
  private String weixin;

  /**qq*/
  private String qq;

  /**身份证正面*/
  private String idcardFront;

  /**身份证背面*/
  private String idcardBack;

  /**审核状态（0待审核,1有效,2无效）*/
  private Integer auditState;

  /**是否官方（0不是,1是）*/
  private Integer isOfficial;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取粉丝团名称*/  @JsonProperty  @Length(message="粉丝团名称最大长度不能超过20",max =20,groups = {Default.class,Save.class})  @NotEmpty(message="粉丝团名称不能为空",groups = {Default.class,Save.class})
  public String getName()
  {
   return this.name;
  }

  /**设置粉丝团名称*/
  public void setName(String name)
  {
    this.name=name;
  }

  
  /**获取粉丝团类型（0大吧,1个站,2饭拍,3饭汇）*/  @JsonProperty  @NotNull(message="粉丝团类型不能为空",groups = {Default.class,Save.class})
  public Integer getType()
  {
   return this.type;
  }

  /**设置粉丝团类型（0大吧,1个站,2饭拍,3饭汇）*/
  public void setType(Integer type)
  {
    this.type=type;
  }

  
  /**获取关注明星*/  @JsonProperty
  public Integer getStarId()
  {
   return this.starId;
  }

  /**设置关注明星*/
  public void setStarId(Integer starId)
  {
    this.starId=starId;
  }

  
  /**获取认证类型（0粉丝团,1娱乐机构）*/  @JsonProperty  @NotNull(message="认证类型不能为空",groups = {Default.class,Save.class})
  public Integer getCertifyType()
  {
   return this.certifyType;
  }

  /**设置认证类型（0粉丝团,1娱乐机构）*/
  public void setCertifyType(Integer certifyType)
  {
    this.certifyType=certifyType;
  }

  
  /**获取负责人名称*/  @JsonProperty  @Length(message="负责人名称最大长度不能超过20",max =20,groups = {Default.class,Save.class})  @NotEmpty(message="负责人名称不能为空",groups = {Default.class,Save.class})
  public String getPrincipalName()
  {
   return this.principalName;
  }

  /**设置负责人名称*/
  public void setPrincipalName(String principalName)
  {
    this.principalName=principalName;
  }

  
  /**获取负责人职务*/  @JsonProperty  @Length(message="负责人职务最大长度不能超过20",max =20,groups = {Default.class,Save.class})  @NotEmpty(message="负责人职务不能为空",groups = {Default.class,Save.class})
  public String getPrincipalPosition()
  {
   return this.principalPosition;
  }

  /**设置负责人职务*/
  public void setPrincipalPosition(String principalPosition)
  {
    this.principalPosition=principalPosition;
  }

  
  /**获取手机号码*/  @JsonProperty  @Length(message="手机号码最大长度不能超过11",max =11,groups = {Default.class,Save.class})  @NotEmpty(message="手机号码不能为空",groups = {Default.class,Save.class})
  public String getMobile()
  {
   return this.mobile;
  }

  /**设置手机号码*/
  public void setMobile(String mobile)
  {
    this.mobile=mobile;
  }

  
  /**获取微博*/  @JsonProperty  @Length(message="微博最大长度不能超过50",max =50,groups = {Default.class,Save.class})
  public String getWeibo()
  {
   return this.weibo;
  }

  /**设置微博*/
  public void setWeibo(String weibo)
  {
    this.weibo=weibo;
  }

  
  /**获取微信*/  @JsonProperty  @Length(message="微信最大长度不能超过50",max =50,groups = {Default.class,Save.class})
  public String getWeixin()
  {
   return this.weixin;
  }

  /**设置微信*/
  public void setWeixin(String weixin)
  {
    this.weixin=weixin;
  }

  
  /**获取qq*/  @JsonProperty  @Length(message="qq最大长度不能超过50",max =50,groups = {Default.class,Save.class})
  public String getQq()
  {
   return this.qq;
  }

  /**设置qq*/
  public void setQq(String qq)
  {
    this.qq=qq;
  }

  
  /**获取身份证正面*/  @JsonProperty  @Length(message="身份证正面最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="身份证正面不能为空",groups = {Default.class,Save.class})
  public String getIdcardFront()
  {
   return this.idcardFront;
  }

  /**设置身份证正面*/
  public void setIdcardFront(String idcardFront)
  {
    this.idcardFront=idcardFront;
  }

  
  /**获取身份证背面*/  @JsonProperty  @Length(message="身份证背面最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="身份证背面不能为空",groups = {Default.class,Save.class})
  public String getIdcardBack()
  {
   return this.idcardBack;
  }

  /**设置身份证背面*/
  public void setIdcardBack(String idcardBack)
  {
    this.idcardBack=idcardBack;
  }

  
  /**获取审核状态（0待审核,1有效,2无效）*/  @JsonProperty  @NotNull(message="审核状态不能为空",groups = {Default.class,Save.class})
  public Integer getAuditState()
  {
   return this.auditState;
  }

  /**设置审核状态（0待审核,1有效,2无效）*/
  public void setAuditState(Integer auditState)
  {
    this.auditState=auditState;
  }

  
  /**获取是否官方（0不是,1是）*/  @JsonProperty  @NotNull(message="是否官方不能为空",groups = {Default.class,Save.class})
  public Integer getIsOfficial()
  {
   return this.isOfficial;
  }

  /**设置是否官方（0不是,1是）*/
  public void setIsOfficial(Integer isOfficial)
  {
    this.isOfficial=isOfficial;
  }

  //自动生成区域结束
 }

