/**
 * 
 * Title：Member
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import com.fasterxml.jackson.annotation.JsonFormat;
import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Member*/
 public class Member extends BaseModel<Member>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 7498171501222190596L;

  /**手机*/
  private String mobile;

  /**密码*/
  private String password;

  /**昵称*/
  private String nickName;

  /**性别（0女,1男）*/
  private Integer sex;

  /**生日*/
  private Date birthday;

  /**个性签名*/
  private String motto;

  /**积分*/
  private Integer points;

  /**头像*/
  private String portrait;

  /**是否认证（0待认证,1已认证）*/
  private Integer isCertificated;


  
  /**获取手机*/
  @JsonProperty
  @Length(message="手机最大长度不能超过11",max =11,groups = {Default.class,Save.class})
  @NotEmpty(message="手机不能为空",groups = {Default.class,Save.class})
  public String getMobile()
  {
   return this.mobile;
  }

  /**设置手机*/
  public void setMobile(String mobile)
  {
    this.mobile=mobile;
  }

  
  /**获取密码*/
  @JsonProperty
  @Length(message="密码最大长度不能超过50",max =50,groups = {Default.class,Save.class})
  @NotEmpty(message="密码不能为空",groups = {Default.class,Save.class})
  public String getPassword()
  {
   return this.password;
  }

  /**设置密码*/
  public void setPassword(String password)
  {
    this.password=password;
  }

  
  /**获取昵称*/
  @JsonProperty
  @Length(message="昵称最大长度不能超过20",max =20,groups = {Default.class,Save.class})
  public String getNickName()
  {
   return this.nickName;
  }

  /**设置昵称*/
  public void setNickName(String nickName)
  {
    this.nickName=nickName;
  }

  
  /**获取性别（0女,1男）*/
  @JsonProperty
  public Integer getSex()
  {
   return this.sex;
  }

  /**设置性别（0女,1男）*/
  public void setSex(Integer sex)
  {
    this.sex=sex;
  }

  
  /**获取生日*/
  @JsonProperty
  @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
  public Date getBirthday()
  {
   return this.birthday;
  }

  /**设置生日*/
  public void setBirthday(Date birthday)
  {
    this.birthday=birthday;
  }

  
  /**获取个性签名*/
  @JsonProperty
  @Length(message="个性签名最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getMotto()
  {
   return this.motto;
  }

  /**设置个性签名*/
  public void setMotto(String motto)
  {
    this.motto=motto;
  }

  
  /**获取积分*/
  @JsonProperty
  public Integer getPoints()
  {
   return this.points;
  }

  /**设置积分*/
  public void setPoints(Integer points)
  {
    this.points=points;
  }

  /**获取头像*/
  @JsonProperty
  @Length(message="头像最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getPortrait()
  {
   return this.portrait;
  }

  /**设置头像*/
  public void setPortrait(String portrait)
  {
    this.portrait=portrait;
  }

  
  /**获取是否认证（0待认证,1已认证）*/
  @JsonProperty
  @NotNull(message="是否认证不能为空",groups = {Default.class,Save.class})
  public Integer getIsCertificated()
  {
   return this.isCertificated;
  }

  /**设置是否认证（0待认证,1已认证）*/
  public void setIsCertificated(Integer isCertificated)
  {
    this.isCertificated=isCertificated;
  }

  //自动生成区域结束
 }

