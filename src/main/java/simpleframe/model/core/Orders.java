/**
 * 
 * Title：Orders
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Orders*/
 public class Orders extends BaseModel<Orders>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 6533431501987814443L;

  /**订单编码*/
  private String code;

  /**会员id*/
  private Integer memberId;

  /**活动id*/
  private Integer activityId;

  /**商品id*/
  private Integer productId;

  /**活动名称*/
  private String activityName;

  /**商品名称*/
  private String productName;

  /**单价*/
  private BigDecimal price;

  /**购买数量*/
  private Integer num;

  /**商品总额*/
  private BigDecimal totalProductMoney;

  /**订单总额*/
  private BigDecimal totalOrderMoney;

  /**收货人姓名*/
  private String receiverName;

  /**手机号码*/
  private String mobile;

  /**位置编码*/
  private String positionCode;

  /**详细地址*/
  private String address;

  /**备注*/
  private String remark;

  /**状态（0待支付,1已支付,2已取消）*/
  private Integer status;


  
  /**获取订单编码*/
  @JsonProperty
  @Length(message="订单编码最大长度不能超过50",max =50,groups = {Default.class,Save.class})
  @NotEmpty(message="订单编码不能为空",groups = {Default.class,Save.class})
  public String getCode()
  {
   return this.code;
  }

  /**设置订单编码*/
  public void setCode(String code)
  {
    this.code=code;
  }

  
  /**获取会员id*/
  @JsonProperty
  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取活动id*/
  @JsonProperty
  @NotNull(message="活动id不能为空",groups = {Default.class,Save.class})
  public Integer getActivityId()
  {
   return this.activityId;
  }

  /**设置活动id*/
  public void setActivityId(Integer activityId)
  {
    this.activityId=activityId;
  }

  
  /**获取商品id*/
  @JsonProperty
  @NotNull(message="商品id不能为空",groups = {Default.class,Save.class})
  public Integer getProductId()
  {
   return this.productId;
  }

  /**设置商品id*/
  public void setProductId(Integer productId)
  {
    this.productId=productId;
  }

  
  /**获取活动名称*/
  @JsonProperty
  @Length(message="活动名称最大长度不能超过20",max =20,groups = {Default.class,Save.class})
  @NotEmpty(message="活动名称不能为空",groups = {Default.class,Save.class})
  public String getActivityName()
  {
   return this.activityName;
  }

  /**设置活动名称*/
  public void setActivityName(String activityName)
  {
    this.activityName=activityName;
  }

  
  /**获取商品名称*/
  @JsonProperty
  @Length(message="商品名称最大长度不能超过20",max =20,groups = {Default.class,Save.class})
  @NotEmpty(message="商品名称不能为空",groups = {Default.class,Save.class})
  public String getProductName()
  {
   return this.productName;
  }

  /**设置商品名称*/
  public void setProductName(String productName)
  {
    this.productName=productName;
  }

  
  /**获取单价*/
  @JsonProperty
  @NotNull(message="单价不能为空",groups = {Default.class,Save.class})
  public BigDecimal getPrice()
  {
   return this.price;
  }

  /**设置单价*/
  public void setPrice(BigDecimal price)
  {
    this.price=price;
  }

  
  /**获取购买数量*/
  @JsonProperty
  @NotNull(message="购买数量不能为空",groups = {Default.class,Save.class})
  public Integer getNum()
  {
   return this.num;
  }

  /**设置购买数量*/
  public void setNum(Integer num)
  {
    this.num=num;
  }

  
  /**获取商品总额*/
  @JsonProperty
  @NotNull(message="商品总额不能为空",groups = {Default.class,Save.class})
  public BigDecimal getTotalProductMoney()
  {
   return this.totalProductMoney;
  }

  /**设置商品总额*/
  public void setTotalProductMoney(BigDecimal totalProductMoney)
  {
    this.totalProductMoney=totalProductMoney;
  }

  
  /**获取订单总额*/
  @JsonProperty
  @NotNull(message="订单总额不能为空",groups = {Default.class,Save.class})
  public BigDecimal getTotalOrderMoney()
  {
   return this.totalOrderMoney;
  }

  /**设置订单总额*/
  public void setTotalOrderMoney(BigDecimal totalOrderMoney)
  {
    this.totalOrderMoney=totalOrderMoney;
  }

  
  /**获取收货人姓名*/
  @JsonProperty
  @Length(message="收货人姓名最大长度不能超过20",max =20,groups = {Default.class,Save.class})
  public String getReceiverName()
  {
   return this.receiverName;
  }

  /**设置收货人姓名*/
  public void setReceiverName(String receiverName)
  {
    this.receiverName=receiverName;
  }

  
  /**获取手机号码*/
  @JsonProperty
  @Length(message="手机号码最大长度不能超过11",max =11,groups = {Default.class,Save.class})
  public String getMobile()
  {
   return this.mobile;
  }

  /**设置手机号码*/
  public void setMobile(String mobile)
  {
    this.mobile=mobile;
  }

  
  /**获取位置编码*/
  @JsonProperty
  @Length(message="位置编码最大长度不能超过12",max =12,groups = {Default.class,Save.class})
  public String getPositionCode()
  {
   return this.positionCode;
  }

  /**设置位置编码*/
  public void setPositionCode(String positionCode)
  {
    this.positionCode=positionCode;
  }

  
  /**获取详细地址*/
  @JsonProperty
  @Length(message="详细地址最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getAddress()
  {
   return this.address;
  }

  /**设置详细地址*/
  public void setAddress(String address)
  {
    this.address = address;
  }

  
  /**获取备注*/
  @JsonProperty
  @Length(message="备注最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getRemark()
  {
   return this.remark;
  }

  /**设置备注*/
  public void setRemark(String remark)
  {
    this.remark=remark;
  }

  
  /**获取状态（0待支付,1已支付,2已取消）*/
  @JsonProperty
  @NotNull(message="状态不能为空",groups = {Default.class,Save.class})
  public Integer getStatus()
  {
   return this.status;
  }

  /**设置状态（0待支付,1已支付,2已取消）*/
  public void setStatus(Integer status)
  {
    this.status=status;
  }

  //自动生成区域结束
 }

