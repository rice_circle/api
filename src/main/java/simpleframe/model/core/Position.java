/**
 * 
 * Title：Position
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Position*/
 public class Position extends BaseModel<Position>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 2375961501987814911L;

  /**父级地址编号*/
  private String parentCode;

  /**地址编码*/
  private String code;

  /**地址名称*/
  private String name;


  
  /**获取父级地址编号*/  @JsonProperty  @Length(message="父级地址编号最大长度不能超过12",max =12,groups = {Default.class,Save.class})  @NotEmpty(message="父级地址编号不能为空",groups = {Default.class,Save.class})
  public String getParentCode()
  {
   return this.parentCode;
  }

  /**设置父级地址编号*/
  public void setParentCode(String parentCode)
  {
    this.parentCode=parentCode;
  }

  
  /**获取地址编码*/  @JsonProperty  @Length(message="地址编码最大长度不能超过12",max =12,groups = {Default.class,Save.class})  @NotEmpty(message="地址编码不能为空",groups = {Default.class,Save.class})
  public String getCode()
  {
   return this.code;
  }

  /**设置地址编码*/
  public void setCode(String code)
  {
    this.code=code;
  }

  
  /**获取地址名称*/  @JsonProperty  @Length(message="地址名称最大长度不能超过64",max =64,groups = {Default.class,Save.class})  @NotEmpty(message="地址名称不能为空",groups = {Default.class,Save.class})
  public String getName()
  {
   return this.name;
  }

  /**设置地址名称*/
  public void setName(String name)
  {
    this.name=name;
  }

  //自动生成区域结束
 }

