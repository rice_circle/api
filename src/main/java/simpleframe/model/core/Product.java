/**
 * 
 * Title：Product
 * @author axi
 * @version 1.0, 2017年07月29日 
 * @since 2017年07月29日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Product*/
 public class Product extends BaseModel<Product>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 4305251501319386109L;

  /**会员id*/
  private Integer memberId;

  /**粉丝圈id*/
  private Integer clubId;

  /**活动id*/
  private Integer activityId;

  /**商品名称*/
  private String name;

  /**价格*/
  private BigDecimal price;

  /**库存数*/
  private Integer totalStock;

  /***/
  private Integer usableStock;

  /***/
  private Integer lockStock;

  /**图片*/
  private String image;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取粉丝圈id*/  @JsonProperty  @NotNull(message="粉丝圈id不能为空",groups = {Default.class,Save.class})
  public Integer getClubId()
  {
   return this.clubId;
  }

  /**设置粉丝圈id*/
  public void setClubId(Integer clubId)
  {
    this.clubId=clubId;
  }

  
  /**获取活动id*/  @JsonProperty  @NotNull(message="活动id不能为空",groups = {Default.class,Save.class})
  public Integer getActivityId()
  {
   return this.activityId;
  }

  /**设置活动id*/
  public void setActivityId(Integer activityId)
  {
    this.activityId=activityId;
  }

  
  /**获取商品名称*/  @JsonProperty  @Length(message="商品名称最大长度不能超过20",max =20,groups = {Default.class,Save.class})  @NotEmpty(message="商品名称不能为空",groups = {Default.class,Save.class})
  public String getName()
  {
   return this.name;
  }

  /**设置商品名称*/
  public void setName(String name)
  {
    this.name=name;
  }

  
  /**获取价格*/  @JsonProperty  @NotNull(message="价格不能为空",groups = {Default.class,Save.class})
  public BigDecimal getPrice()
  {
   return this.price;
  }

  /**设置价格*/
  public void setPrice(BigDecimal price)
  {
    this.price=price;
  }

  
  /**获取库存数*/  @JsonProperty  @NotNull(message="库存数不能为空",groups = {Default.class,Save.class})
  public Integer getTotalStock()
  {
   return this.totalStock;
  }

  /**设置库存数*/
  public void setTotalStock(Integer totalStock)
  {
    this.totalStock=totalStock;
  }

  
  /**获取*/  @JsonProperty  @NotNull(message="不能为空",groups = {Default.class,Save.class})
  public Integer getUsableStock()
  {
   return this.usableStock;
  }

  /**设置*/
  public void setUsableStock(Integer usableStock)
  {
    this.usableStock=usableStock;
  }

  
  /**获取*/  @JsonProperty  @NotNull(message="不能为空",groups = {Default.class,Save.class})
  public Integer getLockStock()
  {
   return this.lockStock;
  }

  /**设置*/
  public void setLockStock(Integer lockStock)
  {
    this.lockStock=lockStock;
  }

  
  /**获取图片*/  @JsonProperty  @Length(message="图片最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="图片不能为空",groups = {Default.class,Save.class})
  public String getImage()
  {
   return this.image;
  }

  /**设置图片*/
  public void setImage(String image)
  {
    this.image=image;
  }

  //自动生成区域结束
 }

