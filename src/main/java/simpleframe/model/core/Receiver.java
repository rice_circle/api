/**
 * 
 * Title：Receiver
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Receiver*/
 public class Receiver extends BaseModel<Receiver>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 3928621501987815566L;

  /**会员id*/
  private Integer memberId;

  /**收货人姓名*/
  private String name;

  /**手机号码*/
  private String mobile;

  /**位置编码*/
  private String positionCode;

  /**详细地址*/
  private String address;

  /**是否默认（0非默认,1默认）*/
  private Integer isDefault;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取收货人姓名*/  @JsonProperty  @Length(message="收货人姓名最大长度不能超过20",max =20,groups = {Default.class,Save.class})  @NotEmpty(message="收货人姓名不能为空",groups = {Default.class,Save.class})
  public String getName()
  {
   return this.name;
  }

  /**设置收货人姓名*/
  public void setName(String name)
  {
    this.name=name;
  }

  
  /**获取手机号码*/  @JsonProperty  @Length(message="手机号码最大长度不能超过11",max =11,groups = {Default.class,Save.class})  @NotEmpty(message="手机号码不能为空",groups = {Default.class,Save.class})
  public String getMobile()
  {
   return this.mobile;
  }

  /**设置手机号码*/
  public void setMobile(String mobile)
  {
    this.mobile=mobile;
  }

  
  /**获取位置编码*/  @JsonProperty  @Length(message="位置编码最大长度不能超过12",max =12,groups = {Default.class,Save.class})  @NotEmpty(message="位置编码不能为空",groups = {Default.class,Save.class})
  public String getPositionCode()
  {
   return this.positionCode;
  }

  /**设置位置编码*/
  public void setPositionCode(String positionCode)
  {
    this.positionCode=positionCode;
  }

  
  /**获取详细地址*/  @JsonProperty  @Length(message="详细地址最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getAddress()
  {
   return this.address;
  }

  /**设置详细地址*/
  public void setAddress(String address)
  {
    this.address=address;
  }

  
  /**获取是否默认（0非默认,1默认）*/  @JsonProperty  @NotNull(message="是否默认不能为空",groups = {Default.class,Save.class})
  public Integer getIsDefault()
  {
   return this.isDefault;
  }

  /**设置是否默认（0非默认,1默认）*/
  public void setIsDefault(Integer isDefault)
  {
    this.isDefault=isDefault;
  }

  //自动生成区域结束
 }

