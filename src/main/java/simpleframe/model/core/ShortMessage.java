/**
 * 
 * Title：ShortMessage
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**ShortMessage*/
 public class ShortMessage extends BaseModel<ShortMessage>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 2882591501222191176L;
  /**
   * 未使用
   */
  public static final int UNUSED = 0;
  /**
   * 已使用
   */
  public static final int USED = 1;
  
  /**短信类型（0注册验证码）*/
  private Integer messageType;

  /**手机号码*/
  private String mobile;

  /**内容*/
  private String content;

  /**是否使用（0未使用，1已使用*/
  private Integer isUsed;

  /**到期时间*/
  private Date expireTime;

  /**ip*/
  private String ip;


  
  /**获取短信类型（0注册验证码）*/  @JsonProperty  @NotNull(message="短信类型不能为空",groups = {Default.class,Save.class})
  public Integer getMessageType()
  {
   return this.messageType;
  }

  /**设置短信类型（0注册验证码）*/
  public void setMessageType(Integer messageType)
  {
    this.messageType=messageType;
  }

  
  /**获取手机号码*/  @JsonProperty  @Length(message="手机号码最大长度不能超过11",max =11,groups = {Default.class,Save.class})  @NotEmpty(message="手机号码不能为空",groups = {Default.class,Save.class})
  public String getMobile()
  {
   return this.mobile;
  }

  /**设置手机号码*/
  public void setMobile(String mobile)
  {
    this.mobile=mobile;
  }

  
  /**获取内容*/  @JsonProperty  @Length(message="内容最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="内容不能为空",groups = {Default.class,Save.class})
  public String getContent()
  {
   return this.content;
  }

  /**设置内容*/
  public void setContent(String content)
  {
    this.content=content;
  }

  
  /**获取是否使用*/  @JsonProperty  @NotNull(message="是否使用不能为空",groups = {Default.class,Save.class})
  public Integer getIsUsed()
  {
   return this.isUsed;
  }

  /**设置是否使用*/
  public void setIsUsed(Integer isUsed)
  {
    this.isUsed=isUsed;
  }

  
  /**获取到期时间*/  @JsonProperty  @NotNull(message="到期时间不能为空",groups = {Default.class,Save.class})
  public Date getExpireTime()
  {
   return this.expireTime;
  }

  /**设置到期时间*/
  public void setExpireTime(Date expireTime)
  {
    this.expireTime=expireTime;
  }

  
  /**获取ip*/  @JsonProperty  @Length(message="ip最大长度不能超过50",max =50,groups = {Default.class,Save.class})  @NotEmpty(message="ip不能为空",groups = {Default.class,Save.class})
  public String getIp()
  {
   return this.ip;
  }

  /**设置ip*/
  public void setIp(String ip)
  {
    this.ip=ip;
  }

  //自动生成区域结束
 }

