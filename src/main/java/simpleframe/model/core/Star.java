/**
 * 
 * Title：Star
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Star*/
 public class Star extends BaseModel<Star>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 5481271501987816221L;

  /**明星名称*/
  private String name;

  /**是否团体（0不是,1是）*/
  private Integer isTeam;

  /**隶属团体,非团体明星默认为空*/
  private Integer teamId;

  /**类型（0华语,1日韩,2欧美）*/
  private Integer starType;

  /**头像*/
  private String portrait;

  /**封面*/
  private String cover;

  /**简介*/
  private String briefInfo;

  /**经纪公司*/
  private String agency;

  /**粉丝昵称*/
  private String fansName;

  /**关注人数*/
  private Integer attentionNum;


  
  /**获取明星名称*/  @JsonProperty  @Length(message="明星名称最大长度不能超过20",max =20,groups = {Default.class,Save.class})  @NotEmpty(message="明星名称不能为空",groups = {Default.class,Save.class})
  public String getName()
  {
   return this.name;
  }

  /**设置明星名称*/
  public void setName(String name)
  {
    this.name=name;
  }

  
  /**获取是否团体（0不是,1是）*/  @JsonProperty  @NotNull(message="是否团体不能为空",groups = {Default.class,Save.class})
  public Integer getIsTeam()
  {
   return this.isTeam;
  }

  /**设置是否团体（0不是,1是）*/
  public void setIsTeam(Integer isTeam)
  {
    this.isTeam=isTeam;
  }

  
  /**获取隶属团体,非团体明星默认为空*/  @JsonProperty
  public Integer getTeamId()
  {
   return this.teamId;
  }

  /**设置隶属团体,非团体明星默认为空*/
  public void setTeamId(Integer teamId)
  {
    this.teamId=teamId;
  }

  
  /**获取类型（0华语,1日韩,2欧美）*/  @JsonProperty  @NotNull(message="类型不能为空",groups = {Default.class,Save.class})
  public Integer getStarType()
  {
   return this.starType;
  }

  /**设置类型（0华语,1日韩,2欧美）*/
  public void setStarType(Integer starType)
  {
    this.starType=starType;
  }

  
  /**获取头像*/  @JsonProperty  @Length(message="头像最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="头像不能为空",groups = {Default.class,Save.class})
  public String getPortrait()
  {
   return this.portrait;
  }

  /**设置头像*/
  public void setPortrait(String portrait)
  {
    this.portrait=portrait;
  }

  
  /**获取封面*/  @JsonProperty  @Length(message="封面最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="封面不能为空",groups = {Default.class,Save.class})
  public String getCover()
  {
   return this.cover;
  }

  /**设置封面*/
  public void setCover(String cover)
  {
    this.cover=cover;
  }

  
  /**获取简介*/  @JsonProperty  @Length(message="简介最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="简介不能为空",groups = {Default.class,Save.class})
  public String getBriefInfo()
  {
   return this.briefInfo;
  }

  /**设置简介*/
  public void setBriefInfo(String briefInfo)
  {
    this.briefInfo=briefInfo;
  }

  
  /**获取经纪公司*/  @JsonProperty  @Length(message="经纪公司最大长度不能超过100",max =100,groups = {Default.class,Save.class})
  public String getAgency()
  {
   return this.agency;
  }

  /**设置经纪公司*/
  public void setAgency(String agency)
  {
    this.agency=agency;
  }

  
  /**获取粉丝昵称*/  @JsonProperty  @Length(message="粉丝昵称最大长度不能超过20",max =20,groups = {Default.class,Save.class})
  public String getFansName()
  {
   return this.fansName;
  }

  /**设置粉丝昵称*/
  public void setFansName(String fansName)
  {
    this.fansName=fansName;
  }

  
  /**获取关注人数*/  @JsonProperty  @NotNull(message="关注人数不能为空",groups = {Default.class,Save.class})
  public Integer getAttentionNum()
  {
   return this.attentionNum;
  }

  /**设置关注人数*/
  public void setAttentionNum(Integer attentionNum)
  {
    this.attentionNum=attentionNum;
  }

  //自动生成区域结束
 }

