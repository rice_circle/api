/**
 * 
 * Title：Vote
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**Vote*/
 public class Vote extends BaseModel<Vote>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 790271501222191562L;

  /**会员id*/
  private Integer memberId;

  /**粉丝圈id*/
  private Integer clubId;

  /**投票标题*/
  private String title;

  /**详情*/
  private String details;

  /**总票数*/
  private Integer voteNum;

  /**开始时间*/
  private Date startTime;

  /**结束时间*/
  private Date endTime;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取粉丝圈id*/  @JsonProperty  @NotNull(message="粉丝圈id不能为空",groups = {Default.class,Save.class})
  public Integer getClubId()
  {
   return this.clubId;
  }

  /**设置粉丝圈id*/
  public void setClubId(Integer clubId)
  {
    this.clubId=clubId;
  }

  
  /**获取投票标题*/  @JsonProperty  @Length(message="投票标题最大长度不能超过50",max =50,groups = {Default.class,Save.class})  @NotEmpty(message="投票标题不能为空",groups = {Default.class,Save.class})
  public String getTitle()
  {
   return this.title;
  }

  /**设置投票标题*/
  public void setTitle(String title)
  {
    this.title=title;
  }

  
  /**获取详情*/  @JsonProperty  @Length(message="详情最大长度不能超过255",max =255,groups = {Default.class,Save.class})  @NotEmpty(message="详情不能为空",groups = {Default.class,Save.class})
  public String getDetails()
  {
   return this.details;
  }

  /**设置详情*/
  public void setDetails(String details)
  {
    this.details=details;
  }

  
  /**获取总票数*/  @JsonProperty  @NotNull(message="总票数不能为空",groups = {Default.class,Save.class})
  public Integer getVoteNum()
  {
   return this.voteNum;
  }

  /**设置总票数*/
  public void setVoteNum(Integer voteNum)
  {
    this.voteNum=voteNum;
  }

  
  /**获取开始时间*/  @JsonProperty  @NotNull(message="开始时间不能为空",groups = {Default.class,Save.class})
  public Date getStartTime()
  {
   return this.startTime;
  }

  /**设置开始时间*/
  public void setStartTime(Date startTime)
  {
    this.startTime=startTime;
  }

  
  /**获取结束时间*/  @JsonProperty  @NotNull(message="结束时间不能为空",groups = {Default.class,Save.class})
  public Date getEndTime()
  {
   return this.endTime;
  }

  /**设置结束时间*/
  public void setEndTime(Date endTime)
  {
    this.endTime=endTime;
  }

  //自动生成区域结束
 }

