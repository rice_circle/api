/**
 * 
 * Title：VoteItem
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**VoteItem*/
 public class VoteItem extends BaseModel<VoteItem>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 3977141501222191940L;

  /**会员id*/
  private Integer memberId;

  /**投票id*/
  private Integer voteId;

  /**关联明星id*/
  private Integer starId;

  /**图片*/
  private String image;

  /**内容*/
  private String content;

  /**票数*/
  private Integer voteNum;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取投票id*/  @JsonProperty  @NotNull(message="投票id不能为空",groups = {Default.class,Save.class})
  public Integer getVoteId()
  {
   return this.voteId;
  }

  /**设置投票id*/
  public void setVoteId(Integer voteId)
  {
    this.voteId=voteId;
  }

  
  /**获取关联明星id*/  @JsonProperty  @NotNull(message="关联明星id不能为空",groups = {Default.class,Save.class})
  public Integer getStarId()
  {
   return this.starId;
  }

  /**设置关联明星id*/
  public void setStarId(Integer starId)
  {
    this.starId=starId;
  }

  
  /**获取图片*/  @JsonProperty  @Length(message="图片最大长度不能超过255",max =255,groups = {Default.class,Save.class})
  public String getImage()
  {
   return this.image;
  }

  /**设置图片*/
  public void setImage(String image)
  {
    this.image=image;
  }

  
  /**获取内容*/  @JsonProperty  @Length(message="内容最大长度不能超过50",max =50,groups = {Default.class,Save.class})  @NotEmpty(message="内容不能为空",groups = {Default.class,Save.class})
  public String getContent()
  {
   return this.content;
  }

  /**设置内容*/
  public void setContent(String content)
  {
    this.content=content;
  }

  
  /**获取票数*/  @JsonProperty  @NotNull(message="票数不能为空",groups = {Default.class,Save.class})
  public Integer getVoteNum()
  {
   return this.voteNum;
  }

  /**设置票数*/
  public void setVoteNum(Integer voteNum)
  {
    this.voteNum=voteNum;
  }

  //自动生成区域结束
 }

