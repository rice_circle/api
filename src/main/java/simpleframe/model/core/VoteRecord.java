/**
 * 
 * Title：VoteRecord
 * @author axi
 * @version 1.0, 2017年07月28日 
 * @since 2017年07月28日 
 */

package simpleframe.model.core;

import simpleframe.model.base.BaseModel;

import java.util.Date;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

 /**VoteRecord*/
 public class VoteRecord extends BaseModel<VoteRecord>
 {
    
  //自动生成区域开始
  private static final long serialVersionUID = 5556981501222192276L;

  /**会员id*/
  private Integer memberId;

  /**投票id*/
  private Integer voteId;

  /**投票选项id*/
  private Integer voteItemId;


  
  /**获取会员id*/  @JsonProperty  @NotNull(message="会员id不能为空",groups = {Default.class,Save.class})
  public Integer getMemberId()
  {
   return this.memberId;
  }

  /**设置会员id*/
  public void setMemberId(Integer memberId)
  {
    this.memberId=memberId;
  }

  
  /**获取投票id*/  @JsonProperty  @NotNull(message="投票id不能为空",groups = {Default.class,Save.class})
  public Integer getVoteId()
  {
   return this.voteId;
  }

  /**设置投票id*/
  public void setVoteId(Integer voteId)
  {
    this.voteId=voteId;
  }

  
  /**获取投票选项id*/  @JsonProperty  @NotNull(message="投票选项id不能为空",groups = {Default.class,Save.class})
  public Integer getVoteItemId()
  {
   return this.voteItemId;
  }

  /**设置投票选项id*/
  public void setVoteItemId(Integer voteItemId)
  {
    this.voteItemId=voteItemId;
  }

  //自动生成区域结束
 }

