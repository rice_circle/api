package simpleframe.model.param;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import simpleframe.model.core.Activity;
import simpleframe.model.core.Product;

/**
 * @author:axi
 * @date:2017/8/4
 */
public class ActivityParam extends Activity {

	@JsonProperty
    private Integer loginId;

	@JsonProperty
    private List<Product> productList;
	
	@JsonProperty
	private Integer pageNum;
	
	@JsonProperty
	private Integer pageSize;

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

	public Integer getPageNum() {
		return pageNum;
	}

	public void setPageNum(Integer pageNum) {
		this.pageNum = pageNum;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
    
    
}
