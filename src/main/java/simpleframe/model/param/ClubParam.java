package simpleframe.model.param;

import simpleframe.model.core.Club;

/**
 * 登录id
 * @author:axi
 * @date:2017/8/1
 */
public class ClubParam extends Club{
    private Integer loginId;

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }
}
