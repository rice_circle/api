package simpleframe.model.param;

/**
 * @author jichang.sun
 * 用来接受登录id的参数类
 */
public class LoginIdParam {
	private Integer loginId;

	public Integer getLoginId() {
		return loginId;
	}

	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}
}
