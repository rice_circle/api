package simpleframe.model.param;

import com.fasterxml.jackson.annotation.JsonProperty;
import simpleframe.model.core.Member;

/**
 * @author:axi
 * @date:2017/8/9
 */
public class MemberParam extends Member {

    /**
     * 短信验证码
     */
    @JsonProperty
    private String checkCode;

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }
}
