package simpleframe.model.param;

import simpleframe.model.core.Star;

/**
 * 明星参数表
 * @author:axi
 * @date:2017/7/29
 */
public class StarParam extends Star {
    private Integer loginId;

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }
}
