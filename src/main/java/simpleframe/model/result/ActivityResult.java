package simpleframe.model.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import simpleframe.model.core.Activity;
import simpleframe.model.core.Product;
import simpleframe.model.core.Star;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description:
 * @author:axi
 * @date:2017/7/21
 */
public class ActivityResult extends Activity {

    /**
     * 用户头像
     */
    @JsonProperty
    private String memberPortrait;

    /**
     * 粉丝圈名称
     */
    @JsonProperty
    private String clubName;

    /**
     * 粉丝团关注数
     */
    @JsonProperty
    private Integer clubAttentionNum;

    /**
     * 活动关注数
     */
    @JsonProperty
    private Integer activityAttentionNum;

    /**
     * 关联明星
     */
    @JsonProperty
    private List<Star> relationStar;


    /**
     * 总购买数
     */
    @JsonProperty
    private Integer saleNum;

    /**
     * 最小价
     */
    @JsonProperty
    private BigDecimal minPrice;

    /**
     * 最大价
     */
    @JsonProperty
    private BigDecimal maxPrice;

    /**
     * 商品列表
     */
    @JsonProperty
    private List<Product> productList;

    /**
     * 用户是否关注
     */
    @JsonProperty
    private Integer attentioned;

    /**
     * 用户头像
     */
    public String getMemberPortrait() {
        return memberPortrait;
    }

    /**
     * 用户头像
     */
    public void setMemberPortrait(String memberPortrait) {
        this.memberPortrait = memberPortrait;
    }

    /**
     * 粉丝圈名称
     */
    public String getClubName() {
        return clubName;
    }

    /**
     * 粉丝圈名称
     */
    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    /**
     * 粉丝团关注数
     */
    public int getClubAttentionNum() {
        return clubAttentionNum;
    }

    /**
     * 粉丝团关注数
     */
    public void setClubAttentionNum(Integer clubAttentionNum) {
        this.clubAttentionNum = clubAttentionNum;
    }

    /**
     * 活动关注数
     */
    public long getActivityAttentionNum() {
        return activityAttentionNum;
    }

    /**
     * 活动关注数
     */
    public void setActivityAttentionNum(Integer activityAttentionNum) {
        this.activityAttentionNum = activityAttentionNum;
    }

    /**
     * 关联明星
     */
    public List<Star> getRelationStar() {
        return relationStar;
    }

    /**
     * 关联明星
     */
    public void setRelationStar(List<Star> relationStar) {
        this.relationStar = relationStar;
    }

    /**
     * 总销量
     */
    public Integer getSaleNum() {
        return saleNum;
    }

    /**
     * 总购买数
     */
    public void setSaleNum(Integer saleNum) {
        this.saleNum = saleNum;
    }

    /**
     * 最小价
     */
    public BigDecimal getMinPrice() {
        return minPrice;
    }

    /**
     * 最小价
     */
    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    /**
     * 最大价
     */
    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    /**
     * 最大价
     */
    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    /**
     * 商品列表
     * @return
     */
    public List<Product> getProductList() {
        return productList;
    }

    /**
     * 商品列表
     * @param productList
     */
    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void setAttentioned(Integer attentioned) {
        this.attentioned = attentioned;
    }
}
