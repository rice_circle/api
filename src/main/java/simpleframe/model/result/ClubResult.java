package simpleframe.model.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import simpleframe.model.core.Club;

/**
 * @author:axi
 * @date:2017/7/31
 */
public class ClubResult extends Club{

    /**
     * 用户是否关注
     */
    @JsonProperty
    private Integer attentioned;

    /**
     * 关注数目
     */
    @JsonProperty
    private Integer attentionNum;

    /**
     * 用户头像
     */
    @JsonProperty
    private String memberPortrait;

    public String getMemberPortrait() {
        return memberPortrait;
    }

    public void setMemberPortrait(String memberPortrait) {
        this.memberPortrait = memberPortrait;
    }

    public Integer getAttentionNum() {
        return attentionNum;
    }

    public void setAttentionNum(Integer attentionNum) {
        this.attentionNum = attentionNum;
    }

    public Integer getAttentioned() {
        return attentioned;
    }

    public void setAttentioned(Integer attentioned) {
        this.attentioned = attentioned;
    }
}
