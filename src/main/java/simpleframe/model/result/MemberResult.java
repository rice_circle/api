/*
 * 天虹商场股份有限公司版权所有.
 */
package simpleframe.model.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import simpleframe.model.core.Member;

/**
 * 会员结果实体
 * @author:wuxianbing
 * @date:2017/8/9
 */
public class MemberResult extends Member {

    @JsonProperty
    private Integer isAttendStar;

    public Integer getIsAttendStar() {
        return isAttendStar;
    }

    public void setIsAttendStar(Integer isAttendStar) {
        this.isAttendStar = isAttendStar;
    }


}
