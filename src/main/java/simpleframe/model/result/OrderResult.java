package simpleframe.model.result;

import org.springframework.data.redis.connection.SortParameters;
import simpleframe.model.core.Orders;

/**
 * @author:axi
 * @date:2017/7/23
 */
public class OrderResult extends Orders {

    /**
     * 会员名称
     */
    private String memberName;

    /**
     * 会员头像
     */
    private String memberPortrait;

    /**
     * 获取会员名称
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     * 设置会员名称
     */
    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    /**
     * 获取会员头像
     */
    public String getMemberPortrait() {
        return memberPortrait;
    }

    /**
     * 设置会员头像
     */
    public void setMemberPortrait(String memberPortrait) {
        this.memberPortrait = memberPortrait;
    }
}
