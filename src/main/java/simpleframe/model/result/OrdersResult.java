package simpleframe.model.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import simpleframe.model.core.Orders;

/**
 * @author:axi
 * @date:2017/8/6
 */
public class OrdersResult extends Orders{
    @JsonProperty
    private String nickName;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
