package simpleframe.model.result;

/**
 * 短信接口返回实体类
 * 
 * @author sunjichang
 * @date 2017年7月30日下午4:42:31
 * @version 1.0
 * @upate
 */
public class ShortMessageResult {
	// 手机号
	private String mobile;
	// 验证码
	private String verifyCode;
	// 有效时间(单位为秒)
	private int validateActiveTime;

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the verifyCode
	 */
	public String getVerifyCode() {
		return verifyCode;
	}

	/**
	 * @param verifyCode
	 *            the verifyCode to set
	 */
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	/**
	 * @return the validateActiveTime
	 */
	public int getValidateActiveTime() {
		return validateActiveTime;
	}

	/**
	 * @param validateActiveTime
	 *            the validateActiveTime to set
	 */
	public void setValidateActiveTime(int validateActiveTime) {
		this.validateActiveTime = validateActiveTime;
	}

}
