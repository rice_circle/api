package simpleframe.model.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import simpleframe.model.core.Star;

/**
 * 明星结果表
 * @author:axi
 * @date:2017/7/29
 */
public class StarResult extends Star {

    @JsonProperty
    private Integer attentioned;

    public Integer getAttentioned() {
        return attentioned;
    }

    public void setAttentioned(Integer attentioned) {
        this.attentioned = attentioned;
    }
}
