
/**
 * Title：Activity
 *
 * @author axi
 * @version 1.0, 2017年07月20日
 * @since 2017年07月20日
 */

package simpleframe.service;

import simpleframe.model.param.ActivityParam;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Activity;
import simpleframe.model.result.ActivityResult;

public interface ActivityService extends BaseService<Activity, Integer> {

    /**
     * 获取最新的活动数据
     *
     * @param pageSize  数据条数
     * @param activity  活动
     * @return
     * @author wuxianbing
     */
    PageInfo<ActivityResult> getLastList(int pageNum, int pageSize, ActivityParam activity);

    /**
     * 获取活动详细信息
     *
     * @param id  活动id
     * @return
     * @author wuxianbing
     */
    ActivityResult getDetails(int id);
    
    /**
     * 获取发布的活动列表
     * @param pageNum 当前查询页
     * @param pageSize 每页显示的条数
     * @param activity 参数
     * @return
     */
    PageInfo<ActivityResult> getPublishActivityList(int pageNum, int pageSize, ActivityParam activity);
}

