
/**
 *
 * Title：Activity
 * @author axi
 * @version 1.0, 2017年07月20日
 * @since 2017年07月20日
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.ActivityDao;
import simpleframe.model.param.ActivityParam;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Activity;
import simpleframe.model.result.ActivityResult;

@Service("activityServiceImpl")
public class ActivityServiceImpl extends BaseServiceImpl<Activity,Integer> implements ActivityService{

	@Autowired
	private ActivityDao activityDao;

	@Autowired
	public void setActivityDao(ActivityDao activityDao) {
		super.setBaseDao(activityDao);
	}

	/**
	 * 获取最新的活动数据
	 * @author wuxianbing
	 * @param pageNum 页码
	 * @param pageSize 每页条数
	 * @param activity 活动参数
	 * @return
	 */
	@Override
	public PageInfo<ActivityResult> getLastList(int pageNum, int pageSize, ActivityParam activity) {
		return activityDao.getLastList(pageNum,pageSize,activity);

	}

	@Override
	public ActivityResult getDetails(int id) {
		return activityDao.getDetails(id);
	}
	
	/**
     * 获取发布的活动列表
     * @param pageNum 当前查询页
     * @param pageSize 每页显示的条数
     * @param activity 参数
     * @return
     */
	@Override
	public PageInfo<ActivityResult> getPublishActivityList(int pageNum, int pageSize, ActivityParam activity){
		return activityDao.getPublishActivityList(pageNum,pageSize,activity);
    }
}

