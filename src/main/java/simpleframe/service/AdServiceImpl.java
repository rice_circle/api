
/**
 * 
 * Title：Ad
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.AdDao;
import simpleframe.model.core.Ad;

@Service("adServiceImpl")
public class AdServiceImpl extends BaseServiceImpl<Ad,Integer> implements AdService{

	@Autowired
    private AdDao adDao;

	@Autowired
	public void setAdDao(AdDao adDao) {
		super.setBaseDao(adDao);
	}	
}

