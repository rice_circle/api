
/**
 * 
 * Title：Attendance
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.AttendanceDao;
import simpleframe.model.core.Attendance;

public interface AttendanceService extends BaseService<Attendance,Integer>{

}

