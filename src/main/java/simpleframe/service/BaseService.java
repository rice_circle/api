package simpleframe.service;

import java.io.Serializable;
import java.util.List;

import simpleframe.utils.PageUtils.PageInfo;

/**
 * Service - 基类
 * 
 * @version 1.0
 */
public interface BaseService<T, ID extends Serializable> {
	/**
	 * 查找实体
	 * 
	 * @author axiwu
	 * @param id
	 * @return
	 */
	public T select(ID id);

	/**
	 * 查找实体
	 * 
	 * @author axiwu
	 * @param model
	 *            （存在多个返回最后的实体）
	 * @return
	 */
	public T selectModel(T model);

	/**
	 * 查找列表
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public List<T> selectList(T model);

	/**
	 * 查找确定列表
	 * 
	 * @author axiwu
	 * @param ids
	 * @return
	 */
	public List<T> selectCertainList(ID... ids);

	
	/**
	 * 查询分页
	 * @author axiwu
	 * @param model
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<T> selectPage(T model, int pageNum, int pageSize);
	
	/**
	 * 查找数据条数
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public int selectCount(T model);

	/**
	 * 是否存在数据
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public boolean selectExists(T model);

	/**
	 * 插入实体
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public boolean insert(T model);

	/**
	 * 插入返回实体
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public T insertReturnModel(T model);

	/**
	 * 更新实体
	 * 
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public boolean update(T model);


	/**
	 * 更新返回实体
	 *
	 * @author axiwu
	 * @param model
	 * @return
	 */
	public T  updateReturnModel(T model);

	/**
	 * 更新实体，值为null不更新
	 * @author axiwu
	 * @param model
	 * @return
	 */
	boolean updateTrim(T model);

	/**
	 * 逻辑删除
	 * 
	 * @author axiwu
	 * @param id
	 * @return
	 */
	boolean deleteSoft(ID id);

	/**
	 * 物理删除
	 * 
	 * @author axiwu
	 * @param id
	 * @return
	 */
	boolean deleteHard(ID id);
}