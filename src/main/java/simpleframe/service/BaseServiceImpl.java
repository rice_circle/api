package simpleframe.service;

import java.io.Serializable;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import simpleframe.dao.BaseDao;
import simpleframe.utils.PageUtils.PageInfo;

@Transactional
public class BaseServiceImpl<T, ID extends Serializable> implements BaseService<T, ID> {
	/**
	 * Logger for this class
	 */
	private static Logger logger = Logger.getLogger(BaseServiceImpl.class);

	private Class<T> modelClass;

	/** baseDao */
	private BaseDao<T, ID> baseDao;

	public void setBaseDao(BaseDao<T, ID> baseDao) {
		this.baseDao = baseDao;
	}

	@Override
	@Transactional(readOnly = true)
	public T select(ID id) {
		return baseDao.select(id);

	}

	@Override
	@Transactional(readOnly = true)
	public T selectModel(T model) {
		return baseDao.selectModel(model);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> selectList(T model) {
		return baseDao.selectList(model);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> selectCertainList(ID... ids) {
		return baseDao.selectCertainList(ids);
	}
	
	@Override
	@Transactional(readOnly = true)
	public PageInfo<T> selectPage(T model, int pageNum, int pageSize) {
		return baseDao.selectPage(model, pageNum, pageSize);
	}

	@Override
	@Transactional(readOnly = true)
	public int selectCount(T model) {
		return baseDao.selectCount(model);
	}

	@Override
	@Transactional(readOnly = true)
	public boolean selectExists(T model) {
		return baseDao.selectExists(model);
	}

	@Override
	@Transactional
	public boolean insert(T model) {
		return baseDao.insert(model);
	}
	
	@Override
	public T insertReturnModel(T model) {
		return baseDao.insertReturnModel(model);
	}

	@Override
	@Transactional
	public boolean update(T model) {
		return baseDao.update(model);
	}

	@Override
	@Transactional
	public T  updateReturnModel(T model)
	{
		return baseDao.updateReturnModel(model);
	}


	@Override
	@Transactional
	public boolean updateTrim(T model) {
		return baseDao.updateTrim(model);
	}

	@Override
	@Transactional
	public boolean deleteSoft(ID id) {
		return baseDao.deleteSoft(id);
	}
	
	@Override
	@Transactional
	public boolean deleteHard(ID id) {
		return baseDao.deleteHard(id);
	}
}