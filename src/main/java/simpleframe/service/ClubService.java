
/**
 * Title：Club
 *
 * @author axi
 * @version 1.0, 2017年07月23日
 * @since 2017年07月23日
 */

package simpleframe.service;

import java.util.List;

import simpleframe.model.core.Club;
import simpleframe.model.result.ClubResult;
import simpleframe.utils.PageUtils.PageInfo;

public interface ClubService extends BaseService<Club, Integer> {

    /**
     * 获取扩展分页列表
     * @param param
     * @return
     */
    PageInfo<ClubResult> getExtendList(Club param, Integer pageNum, Integer pageSize);
    
    /**
     * 根据用户id查询建团信息
     * @param memberId 用户id
     * @return
     */
    List<Club> selectByMemberId(Integer memberId);
}

