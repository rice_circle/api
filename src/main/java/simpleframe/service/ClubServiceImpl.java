
/**
 * Title：Club
 *
 * @author axi
 * @version 1.0, 2017年07月23日
 * @since 2017年07月23日
 */

package simpleframe.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.ClubDao;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Club;
import simpleframe.model.result.ClubResult;

@Service("clubServiceImpl")
public class ClubServiceImpl extends BaseServiceImpl<Club, Integer> implements ClubService {

    @Autowired
    private ClubDao clubDao;

    @Autowired
    public void setClubDao(ClubDao clubDao) {
        super.setBaseDao(clubDao);
    }

    /**
     * 获取扩展分页列表
     * @param param
     * @return
     */
    @Override
    public PageInfo<ClubResult> getExtendList(Club param, Integer pageNum, Integer pageSize) {
        return clubDao.getExtendList(param, pageNum, pageSize);
    }
    
     /**
     * 根据用户id查询建团信息
     * @param memberId 用户id
     * @return
     */
    public List<Club> selectByMemberId(Integer memberId){
    	return clubDao.selectByMemberId(memberId);
    }
}

