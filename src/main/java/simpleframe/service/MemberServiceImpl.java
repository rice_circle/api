
/**
 * 
 * Title：Member
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.MemberDao;
import simpleframe.model.core.Member;

@Service("memberServiceImpl")
public class MemberServiceImpl extends BaseServiceImpl<Member,Integer> implements MemberService{

	@Autowired
    private MemberDao memberDao;

	@Autowired
	public void setMemberDao(MemberDao memberDao) {
		super.setBaseDao(memberDao);
	}	
}

