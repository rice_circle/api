
/**
 * 
 * Title：Orders
 * @author axi
 * @version 1.0, 2017年07月23日
 * @since 2017年07月23日
 */

package simpleframe.service;

import java.util.List;

import simpleframe.model.core.Orders;
import simpleframe.model.result.OrderResult;
import simpleframe.model.result.OrdersResult;
import simpleframe.utils.PageUtils.PageInfo;

public interface OrdersService extends BaseService<Orders,Integer>{

    /**
     * 获取购买排名
     * @param param
     * @return
     */
    List<OrderResult> getPurchaseRank(Orders param);

    /**
     * 生成订单编码
     * @return
     * @param activityType
     */
    String generateCode(Integer activityType);
    
    /**
     * 根据订单编码查询订单信息
     * @param code 订单编码
     * @return
     */
    Orders getOrderInfoByCode(String code);

    /**
     * 获取订单列表
     * @param inParam
     * @param pageNum
     *@param pageSize @return
     */
    PageInfo<OrdersResult> getExtendList(Orders inParam, Integer pageNum, Integer pageSize);

    /**
     * 获取订单详情
     * @param param
     * @return
     */
    OrderResult getDetails(Orders param);
}

