
/**
 * Title：Orders
 *
 * @author axi
 * @version 1.0, 2017年07月23日
 * @since 2017年07月23日
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.OrdersDao;
import simpleframe.model.base.ActivityType;
import simpleframe.model.core.Orders;
import simpleframe.model.result.OrderResult;
import simpleframe.model.result.OrdersResult;
import simpleframe.utils.EnumUtils;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.utils.RandomUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service("orderServiceImpl")
public class OrdersServiceImpl extends BaseServiceImpl<Orders, Integer> implements OrdersService {

    @Autowired
    private OrdersDao orderDao;

    @Autowired
    public void setOrderDao(OrdersDao orderDao) {
        super.setBaseDao(orderDao);
    }

    @Override
    public List<OrderResult> getPurchaseRank(Orders param) {
        return orderDao.getPurchaseRank(param);
    }

    @Override
    public String generateCode(Integer activityType) {
        ActivityType enumItem = EnumUtils.getEnumItem(ActivityType.class, activityType);
        StringBuffer strBuffer = new StringBuffer();
        switch (enumItem) {
            case YINGYUAN:
                strBuffer.append("Y");
                break;
            case GONGYI:
                strBuffer.append("G");
                break;
            case SHANGPIN:
                strBuffer.append("S");
                break;
        }
        Date date = new Date();
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        strBuffer.append(format.format(date));
        strBuffer.append(RandomUtils.getRandom(3));
        return strBuffer.toString();
    }

    @Override
    public PageInfo<OrdersResult> getExtendList(Orders param, Integer pageNum, Integer pageSize) {
        return orderDao.getExtendList(param,pageNum,pageSize);
    }

    @Override
    public OrderResult getDetails(Orders param) {
        return orderDao.getDetails(param);
    }
    
    /**
     * 根据订单编码查询订单信息
     * @param code 订单编码
     * @return
     */
    @Override
    public Orders getOrderInfoByCode(String code){
    	return orderDao.getOrderInfoByCode(code);
    }
}

