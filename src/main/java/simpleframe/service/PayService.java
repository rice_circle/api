package simpleframe.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

/**
 * 支付service接口层
 * @author sunjichang
 * @date 2017年7月18日下午9:51:13
 * @version 1.0
 * @upate
 */
public interface PayService extends BaseService<String,Integer>{
	
	/**
	 * 获取支付订单签名信息
	 * @param payType 支付类型(微信、支付宝等)
	 * @param orderId 订单id
	 * @return
	 */
	public String obtainOrderSingInfo(String payType,Integer orderId);
	
	/**
	 * 支付宝异步回调验证
	 * @param request 
	 * @return
	 */
	public String verifyAlipayNotify(HttpServletRequest request);
}
