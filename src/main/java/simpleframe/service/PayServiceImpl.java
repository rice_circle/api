package simpleframe.service;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.model.base.PayType;
import simpleframe.model.core.Orders;
import simpleframe.third.alipay.AlipayManager;

/**
 * 支付service实现层
 * @author sunjichang
 * @date 2017年7月18日下午9:52:35
 * @version 1.0
 * @upate 
 */
@Service("payServiceImpl")
public class PayServiceImpl extends BaseServiceImpl<String, Integer> implements PayService{
	private static final Logger logger = Logger.getLogger(PayServiceImpl.class);
	//规格service
	@Autowired
	private ProductService productService;
	//订单service
	@Autowired
	private OrdersService orderService;
	/**
	 * 获取支付订单签名信息
	 * @param payType 支付类型(微信、支付宝等)
	 * @param orderId 订单id
	 * @return
	 */
	public String obtainOrderSingInfo(String payType,Integer orderId){
		//根据订单id获取商品信息
		Orders order = orderService.select(orderId);
		if(order == null){
			order = new Orders();
			order.setCode(orderId+"");
			order.setActivityName("支付宝测试商品");
			order.setProductName("支付宝测试型号");
			order.setTotalOrderMoney(new BigDecimal(0.01));
//			return null;
		}
		if(PayType.ALIPAY.getText().equals(payType)){
			//订单id
			String orderIdStr = order.getCode();
			//商品名称
			String subject = order.getActivityName();
			//规格
			String specifications = order.getProductName();
			//
			String price = order.getTotalOrderMoney().toString();
			String result = AlipayManager.getInstance().createAlipayOrderInfo(orderIdStr, subject, specifications, price);
			logger.log(Level.DEBUG, "result="+result);
			return result;
		}
		return "";
	}
	
	/**
	 * 支付宝异步回调验证
	 * @param request 
	 * @return
	 */
	public String verifyAlipayNotify(HttpServletRequest request){
		AlipayManager alipay = AlipayManager.getInstance();
		boolean verifyResult = alipay.verifySyncNotify(request);
		if(!verifyResult){
			return AlipayManager.NOTIFY_VERIFY_FAIL;
		}
		Map<String,String> params = alipay.parseNotifyParam(request);
		//获取交易状态
		String tradeStatus = params.get("trade_status");
		//商户订单号
		String outTradeNo = params.get("out_trade_no");
		//卖家支付宝用户号
		String sellerId = params.get("seller_id");
		//卖家支付宝账号
		String sellerEmail = params.get("seller_email");
		//订单所属的appid
		String appId = params.get("app_id");
		//订单金额
		double totalAmount = Double.parseDouble(params.get("total_amount"));
		//订单标题
		String subject = params.get("subject");
		//商品描述
		String body = params.get("body");
//		try {
//			tradeStatus = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//交易失败
		if(!AlipayManager.TRADE_SUCCESS.equals(tradeStatus) || 
				!AlipayManager.TRADE_SUCCESS.equals(tradeStatus)){
			return AlipayManager.NOTIFY_VERIFY_FAIL;
		}
		//判断本次交易的账号是否有改动,如果跟服务器存储的账号不一致，代表交易失败
		if(!alipay.validSellerEmail(sellerEmail)){
			return AlipayManager.NOTIFY_VERIFY_FAIL;
		}
		//如果回调中的appid与服务器存储的appid不一致，代表交易失败
		if(alipay.validAppId(appId)){
			return AlipayManager.NOTIFY_VERIFY_FAIL;
		}
		//根据订单编码查询订单是否存在，如果不存在，说明信息不一致，交易失败
		Orders order = orderService.getOrderInfoByCode(outTradeNo);
		if(null == order){
			return AlipayManager.NOTIFY_VERIFY_FAIL;
		}
		if(!order.getActivityName().equals(subject) || !order.getProductName().equals(body)){
			return AlipayManager.NOTIFY_VERIFY_FAIL;
		}
		//判断总金额是否相同
		if(!new BigDecimal(totalAmount).equals(order.getTotalProductMoney())){
			return AlipayManager.NOTIFY_VERIFY_FAIL;
		}
		//减少库存
		boolean result = productService.deductStock(order.getProductId(), order.getNum());
		if(result){
			return AlipayManager.NOTIFY_VERIFY_SUCCESS;
		}
		return AlipayManager.NOTIFY_VERIFY_FAIL;
	}
}
