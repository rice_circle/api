
/**
 * 
 * Title：Position
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.PositionDao;
import simpleframe.model.core.Position;

public interface PositionService extends BaseService<Position,Integer>{

}

