
/**
 * 
 * Title：Position
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.PositionDao;
import simpleframe.model.core.Position;

@Service("positionServiceImpl")
public class PositionServiceImpl extends BaseServiceImpl<Position,Integer> implements PositionService{

	@Autowired
    private PositionDao positionDao;

	@Autowired
	public void setPositionDao(PositionDao positionDao) {
		super.setBaseDao(positionDao);
	}	
}

