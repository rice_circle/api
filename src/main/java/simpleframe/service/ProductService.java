
/**
 * 
 * Title：Product
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.ProductDao;
import simpleframe.model.core.Product;

public interface ProductService extends BaseService<Product,Integer>{

    /**
     * 锁定库存
     * @param productId
     * @param num
     * @return
     */
    boolean lockStock(int productId, int num);

    /**
     * 库存取消锁定
     * @param productId
     * @param num
     * @return
     */
    boolean unlockStock(int productId, int num);

    /**
     * 扣减库存
     * @param productId
     * @param num
     * @return
     */
    boolean deductStock(int productId, int num);
}

