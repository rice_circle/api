
/**
 * 
 * Title：Product
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simpleframe.dao.ProductDao;
import simpleframe.model.core.Product;

@Service("productServiceImpl")
public class ProductServiceImpl extends BaseServiceImpl<Product,Integer> implements ProductService{

	@Autowired
    private ProductDao productDao;

	@Autowired
	public void setProductDao(ProductDao productDao) {
		super.setBaseDao(productDao);
	}

	@Override
	public boolean lockStock(int productId, int num) {
		if(productId<=0||num<=0)
		{
			return false;
		}

		Product oldModel=productDao.select(productId);
		if(oldModel==null)
		{
			return false;
		}

		if(oldModel.getUsableStock()<num)
		{
			return false;
		}

		Product newModel=new Product();
		newModel.setId(oldModel.getId());
		newModel.setUsableStock(-num);
		newModel.setLockStock(num);

		boolean result= productDao.changeStockLock(newModel);
		return result;
	}

	@Override
	public boolean unlockStock(int productId, int num) {
		if(productId<=0||num<=0)
		{
			return false;
		}

		Product oldModel=productDao.select(productId);
		if(oldModel==null)
		{
			return false;
		}

		if(oldModel.getLockStock()<num)
		{
			return false;
		}

		Product newModel=new Product();
		newModel.setId(oldModel.getId());
		newModel.setUsableStock(num);
		newModel.setLockStock(-num);

		boolean result= productDao.changeStockLock(newModel);
		return result;
	}

	@Override
	public boolean deductStock(int productId, int num)
	{
		if(productId<=0||num<=0)
		{
			return false;
		}

		Product oldModel=productDao.select(productId);
		if(oldModel==null)
		{
			return false;
		}

		if(oldModel.getLockStock()<num)
		{
			return false;
		}

		Product newModel=new Product();
		newModel.setId(oldModel.getId());
		newModel.setTotalStock(-num);
		newModel.setLockStock(-num);


		boolean result= productDao.changeStockLock(newModel);
		return result;
	}
}

