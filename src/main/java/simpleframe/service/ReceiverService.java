
/**
 * 
 * Title：Receiver
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.ReceiverDao;
import simpleframe.model.core.Receiver;

public interface ReceiverService extends BaseService<Receiver,Integer>{
	
	/**
	 * 根据用户id获取其默认地址
	 * @param memberId 用户id
	 * @return
	 */
	Receiver getDefaultReceiverByMemberId(Integer memberId);

	/**
	 * 设置默认地址
	 * @param memberId 用户id
	 * @param id 记录id
	 * @return
	 */
	boolean setDefaultReceiver(Integer memberId,Integer id);
}

