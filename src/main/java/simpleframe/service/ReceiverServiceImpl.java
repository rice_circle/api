
/**
 * 
 * Title：Receiver
 * @author axi
 * @version 1.0, 2017年08月06日 
 * @since 2017年08月06日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.ReceiverDao;
import simpleframe.model.core.Receiver;

@Service("receiverServiceImpl")
public class ReceiverServiceImpl extends BaseServiceImpl<Receiver,Integer> implements ReceiverService{

	@Autowired
    private ReceiverDao receiverDao;

	@Autowired
	public void setReceiverDao(ReceiverDao receiverDao) {
		super.setBaseDao(receiverDao);
	}	
	
	/**
	 * 根据用户id获取其默认地址
	 * @param memberId 用户id
	 * @return
	 */
	@Override
	public Receiver getDefaultReceiverByMemberId(Integer memberId){
		return receiverDao.getDefaultReceiverByMemberId(memberId);
	}
	
	/**
	 * 设置默认地址
	 * @param memberId 用户id
	 * @param id 记录id
	 * @return
	 */
	@Override
	public boolean setDefaultReceiver(Integer memberId,Integer id){
		Receiver defaultReceiver = receiverDao.getDefaultReceiverByMemberId(memberId);
		if(defaultReceiver != null){
			receiverDao.setDefaultReceiver(memberId, false, defaultReceiver.getId());
		}
		return receiverDao.setDefaultReceiver(memberId, true,id);
	}
}

