
/**
 * 
 * Title：ShortMessage
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import simpleframe.model.base.ShortMessageState;
import simpleframe.model.core.ShortMessage;

public interface ShortMessageService extends BaseService<ShortMessage,Integer>{
	/**
	 * 发送验证码
	 * @param phone 手机号
	 * @param type 验证码类型
	 * @return
	 */
	public ShortMessage sendShortMessage(String phone,Integer type);
	
	/**
	 * 短信验证码是否有效
	 * @param phone 手机号
	 * @param code 验证码
	 * @return
	 */
	public ShortMessageState validShortMessage(String phone,String code);
	
}

