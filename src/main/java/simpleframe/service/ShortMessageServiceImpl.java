
/**
 * 
 * Title：ShortMessage
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import simpleframe.config.PropertyPlaceholder;
import simpleframe.core.restful.RESTfulResponse;
import simpleframe.dao.ShortMessageDao;
import simpleframe.model.base.ShortMessageState;
import simpleframe.model.core.ShortMessage;
import simpleframe.third.sms.ShortMessageManger;
import simpleframe.third.sms.ShortMessageThirdModell;
import simpleframe.utils.DateUtils;
import simpleframe.utils.IpUtils;
import simpleframe.utils.RandomUtils;
import simpleframe.utils.StringUtils;

@Service("shortMessageServiceImpl")
public class ShortMessageServiceImpl extends BaseServiceImpl<ShortMessage,Integer> implements ShortMessageService{
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ShortMessageServiceImpl.class);

	@Autowired
    private ShortMessageDao shortMessageDao;
	
	private ShortMessageManger shortMsgManager;
	
	public ShortMessageServiceImpl(){
		shortMsgManager = ShortMessageManger.getInstance();
	}

	@Autowired
	public void setShortMessageDao(ShortMessageDao shortMessageDao) {
		super.setBaseDao(shortMessageDao);
	}

	/**
	 * 发送验证码
	 */
	@Override
	public ShortMessage sendShortMessage(String phone, Integer type) {
		//生成四位的随机验证码
		String code = RandomUtils.obtainPureNumberRandom(3);
		//获取返回的code信息
		RESTfulResponse<ShortMessageThirdModell> result = shortMsgManager.sendVerifyCode(phone,code);
		ShortMessageThirdModell info = result.getResponseBody();
		RequestAttributes requestattributes = RequestContextHolder.currentRequestAttributes();
		//获取ip地址
		String ip = null;
		if(requestattributes != null){
		    HttpServletRequest request = ((ServletRequestAttributes)requestattributes).getRequest();
		    ip = IpUtils.getRemoteIp(request);
		    logger.log(Level.DEBUG, "ip"+ip);
		}
		if(StringUtils.isBlank(ip)){
			return new ShortMessage();
		}
		//创建一个ShrotMessage 实体类，进行数据库操作
		ShortMessage model = new ShortMessage();
		boolean insertResult = false;
		if(ShortMessageThirdModell.STATUS_SUCCESS.equals(info.getStatusCode())){
			//设置验证码
			model.setContent(code);
			Date currentDate = DateUtils.currentDate();
			//设置创建日期
			model.setCreateTime(currentDate);
			//设置过期时间
			model.setExpireTime(DateUtils.dateAddMinute(currentDate, 
					Integer.parseInt(PropertyPlaceholder.getProperty("sms.effective.time").toString())));
			//设置当前code是否已经使用过
			model.setIsUsed(0);
			//设置验证码类型
			model.setMessageType(type);
			//设置手机号
			model.setMobile(phone);
			//设置ip地址
			model.setIp(ip);
			model.setIsDeleted(0);
			insertResult = shortMessageDao.insert(model);
		}
		if(insertResult){
			return model;
		}
		return new ShortMessage();
	}

	/**
	 * 判断验证是否有效
	 */
	@Override
	public ShortMessageState validShortMessage(String phone,String code) {
		ShortMessage msg = shortMessageDao.selectEffectiveByPhone(phone);
		//不存在
		if(msg == null || msg.getIsDeleted() == 1){
			return ShortMessageState.UNCREATE;
		}
		//验证码已使用
		if(msg.getIsUsed() == ShortMessage.USED){
			return ShortMessageState.USED;
		}
		Date now = new Date(System.currentTimeMillis());
		//过期
		if(DateUtils.dateCompare(now, msg.getExpireTime()) > 0){
			//修改验证码状态为已读
			msg.setIsUsed(ShortMessage.USED);
			shortMessageDao.update(msg);
			return ShortMessageState.EXPIRED;
		}
		if(!msg.getContent().equals(code)){
			//修改验证码状态为已读
			msg.setIsUsed(ShortMessage.USED);
			shortMessageDao.update(msg);
			return ShortMessageState.ERROR;
		}
		//修改验证码状态为已读
		msg.setIsUsed(ShortMessage.USED);
		shortMessageDao.update(msg);
		return ShortMessageState.NORMAL;
	}	
}

