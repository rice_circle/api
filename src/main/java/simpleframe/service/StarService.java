
/**
 * 
 * Title：Star
 * @author axi
 * @version 1.0, 2017年07月20日 
 * @since 2017年07月20日 
 */

package simpleframe.service;

import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.core.Star;
import simpleframe.model.param.StarParam;
import simpleframe.model.result.StarResult;

import java.util.List;

public interface StarService extends BaseService<Star,Integer>{

    /**
     * 获取关联明星
     * @param relationId
     * @return
     */
    List<Star> getRelationList(Integer relationId);

    /**
     * 获取明星查找列表
     * @param param
     * @return
     */
    PageInfo<StarResult> getExtendList(int pageNum, int pageSize, StarParam param);

    /**
     * 获取明星详情
     * @param param
     * @return
     */
    StarResult getDetails(StarParam param);
}

