
/**
 * Title：Star
 *
 * @author axi
 * @version 1.0, 2017年07月20日
 * @since 2017年07月20日
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.StarDao;
import simpleframe.model.base.DeleteType;
import simpleframe.utils.PageUtils.PageInfo;
import simpleframe.model.base.StarType;
import simpleframe.model.core.Star;
import simpleframe.model.param.StarParam;
import simpleframe.model.result.StarResult;

import java.util.ArrayList;
import java.util.List;

@Service("starServiceImpl")
public class StarServiceImpl extends BaseServiceImpl<Star, Integer> implements StarService {

    @Autowired
    private StarDao starDao;

    @Autowired
    public void setStarDao(StarDao starDao) {
        super.setBaseDao(starDao);
    }

    @Override
    public List<Star> getRelationList(Integer starId) {
        List<Star> starList = null;

        Star star = starDao.select(starId);
        if (star == null) {
            return starList;
        }

        // 添加直接关联明星
        starList = new ArrayList<>();
        starList.add(star);

        // 添加明星团体下的明星
        if (star.getIsTeam().intValue() == StarType.TEAM.getValue()) {
            Star param = new Star();
            param.setTeamId(starId);
            param.setIsDeleted(DeleteType.NORMAL.getValue());
            List<Star> subStarList = starDao.selectList(param);
            if (subStarList != null && subStarList.size() > 0) {
                starList.addAll(subStarList);
            }
        }
        return starList;
    }

    @Override
    public PageInfo<StarResult> getExtendList(int pageNum, int pageSize, StarParam param) {
        return starDao.getExtendList(pageNum,pageSize,param);
    }

    @Override
    public StarResult getDetails(StarParam param) {
        return starDao.getDetails(param);
    }

}


