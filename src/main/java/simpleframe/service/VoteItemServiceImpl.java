
/**
 * 
 * Title：VoteItem
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.VoteItemDao;
import simpleframe.model.core.VoteItem;

@Service("voteItemServiceImpl")
public class VoteItemServiceImpl extends BaseServiceImpl<VoteItem,Integer> implements VoteItemService{

	@Autowired
    private VoteItemDao voteItemDao;

	@Autowired
	public void setVoteItemDao(VoteItemDao voteItemDao) {
		super.setBaseDao(voteItemDao);
	}	
}

