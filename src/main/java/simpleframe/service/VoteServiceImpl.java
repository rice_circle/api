
/**
 * 
 * Title：Vote
 * @author axi
 * @version 1.0, 2017年07月23日 
 * @since 2017年07月23日 
 */

package simpleframe.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simpleframe.dao.VoteDao;
import simpleframe.model.core.Vote;

@Service("voteServiceImpl")
public class VoteServiceImpl extends BaseServiceImpl<Vote,Integer> implements VoteService{

	@Autowired
    private VoteDao voteDao;

	@Autowired
	public void setVoteDao(VoteDao voteDao) {
		super.setBaseDao(voteDao);
	}	
}

