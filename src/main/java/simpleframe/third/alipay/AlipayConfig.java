package simpleframe.third.alipay;

import simpleframe.config.PropertyPlaceholder;

/**
 * 支付宝支付信息配置界面
 * 
 * @author sunjichang
 * @date 2017年7月17日下午3:45:41
 * @version 1.0
 * @upate
 */
public class AlipayConfig {
	/**
	 * 支付宝服务器地址
	 */
	private String serviceUrl;
	/**
	 * 支付宝回调地址
	 */
	private String notifyUrl;
	/**
	 * 签约合作者身份ID
	 */
	private String partner;
	/**
	 * app id
	 */
	private String appId;
	/**
	 * app 私有key
	 */
	private String appPrivateKey;
	/**
	 * 数据格式(json或者其他)
	 */
	private String dataFormat;
	/**
	 * 数据字符集(utf-8或者其他)
	 */
	private String charset;
	/**
	 * 阿里云配置的公钥
	 */
	private String alipayPublicKey;
	/**
	 * 加密方式(RSA2或者其他)
	 */
	private String signType;
	/**
	 * 订单过期时间
	 */
	private String timeoutExpress;
	/**
	 * 销售产品码，商家和支付宝签约的产品码
	 */
	private String productCode;
	/**
	 * 加密方式(AES等)
	 */
	private String encryptionType;
	/**
	 * 加密的密钥
	 */
	private String encryptionKey;
	/**
	 * 支付宝账号
	 */
	private String sellerEmail;
	public AlipayConfig() {
		serviceUrl = PropertyPlaceholder.getProperty("alipay.service.url").toString();
		notifyUrl = PropertyPlaceholder.getProperty("alipay.notify.url").toString();
		partner = PropertyPlaceholder.getProperty("alipay.partner").toString();
		appId = PropertyPlaceholder.getProperty("alipay.app.id").toString();
		appPrivateKey = PropertyPlaceholder.getProperty("alipay.app.private.key").toString();
		dataFormat = PropertyPlaceholder.getProperty("alipay.data.format").toString();
		charset = PropertyPlaceholder.getProperty("alipay.charset").toString();
		alipayPublicKey = PropertyPlaceholder.getProperty("alipay.public.key").toString();
		signType = PropertyPlaceholder.getProperty("alipay.sign.type").toString();
		timeoutExpress = PropertyPlaceholder.getProperty("alipay.timeout.express").toString();
		productCode = PropertyPlaceholder.getProperty("alipay.product.code").toString();
		encryptionType = PropertyPlaceholder.getProperty("alipay.encryption.type").toString();
		encryptionKey = PropertyPlaceholder.getProperty("alipay.encryption.key").toString();
		sellerEmail = PropertyPlaceholder.getProperty("alipay.seller.email").toString();
	}

	/**
	 * @return the notifyUrl
	 */
	public String getNotifyUrl() {
		return notifyUrl;
	}

	/**
	 * @param notifyUrl
	 *            the notifyUrl to set
	 */
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	/**
	 * @return the partner
	 */
	public String getPartner() {
		return partner;
	}

	/**
	 * @param partner
	 *            the partner to set
	 */
	public void setPartner(String partner) {
		this.partner = partner;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		return appId;
	}

	/**
	 * @param appId
	 *            the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}

	/**
	 * @return the appPrivateKey
	 */
	public String getAppPrivateKey() {
		return appPrivateKey;
	}

	/**
	 * @param appPrivateKey
	 *            the appPrivateKey to set
	 */
	public void setAppPrivateKey(String appPrivateKey) {
		this.appPrivateKey = appPrivateKey;
	}

	/**
	 * @return the dataFormat
	 */
	public String getDataFormat() {
		return dataFormat;
	}

	/**
	 * @param dataFormat
	 *            the dataFormat to set
	 */
	public void setDataFormat(String dataFormat) {
		this.dataFormat = dataFormat;
	}

	/**
	 * @return the charset
	 */
	public String getCharset() {
		return charset;
	}

	/**
	 * @param charset
	 *            the charset to set
	 */
	public void setCharset(String charset) {
		this.charset = charset;
	}

	/**
	 * @return the alipayPublicKey
	 */
	public String getAlipayPublicKey() {
		return alipayPublicKey;
	}

	/**
	 * @param alipayPublicKey
	 *            the alipayPublicKey to set
	 */
	public void setAlipayPublicKey(String alipayPublicKey) {
		this.alipayPublicKey = alipayPublicKey;
	}

	/**
	 * @return the signType
	 */
	public String getSignType() {
		return signType;
	}

	/**
	 * @param signType
	 *            the signType to set
	 */
	public void setSignType(String signType) {
		this.signType = signType;
	}

	/**
	 * @return the timeoutExpress
	 */
	public String getTimeoutExpress() {
		return timeoutExpress;
	}

	/**
	 * @param timeoutExpress
	 *            the timeoutExpress to set
	 */
	public void setTimeoutExpress(String timeoutExpress) {
		this.timeoutExpress = timeoutExpress;
	}

	/**
	 * @return the serviceUrl
	 */
	public String getServiceUrl() {
		return serviceUrl;
	}

	/**
	 * @param serviceUrl
	 *            the serviceUrl to set
	 */
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode
	 *            the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the encryptionType
	 */
	public String getEncryptionType() {
		return encryptionType;
	}

	/**
	 * @param encryptionType the encryptionType to set
	 */
	public void setEncryptionType(String encryptionType) {
		this.encryptionType = encryptionType;
	}

	/**
	 * @return the encryptionKey
	 */
	public String getEncryptionKey() {
		return encryptionKey;
	}

	/**
	 * @param encryptionKey the encryptionKey to set
	 */
	public void setEncryptionKey(String encryptionKey) {
		this.encryptionKey = encryptionKey;
	}

	public String getSellerEmail() {
		return sellerEmail;
	}

	public void setSellerEmail(String sellerEmail) {
		this.sellerEmail = sellerEmail;
	}
}
