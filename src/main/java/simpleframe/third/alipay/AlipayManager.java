package simpleframe.third.alipay;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.web.context.ContextLoader;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;

import simpleframe.utils.StringUtils;

/**
 * 支付宝相关处理类
 * @author sunjichang
 * @date 2017年7月17日下午9:37:06
 * @version 1.0
 * @upate 
 */
public class AlipayManager {
	private static final Logger logger = Logger.getLogger(AlipayManager.class);
	//实例对象
	private static AlipayManager mInstance = null;
	//操作客户端类
	private AlipayClient mAlipayClient = null;
	//配置类
	private AlipayConfig mAlipayConfig = null;
	//交易完成（触发通知）
	public static final String TRADE_FINISHED = "TRADE_FINISHED";	
	//支付成功（触发通知）
	public static final String TRADE_SUCCESS  = "TRADE_SUCCESS";
	//交易创建（不触发通知）
	public static final String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
	//交易关闭（触发通知）
	public static final String TRADE_CLOSED = "TRADE_CLOSED";
	//支付宝异步回调验证成功
	public static final String NOTIFY_VERIFY_SUCCESS = "success";
	//支付宝异步回调验证失败
	public static final String NOTIFY_VERIFY_FAIL = "failure";
	private static class InstanceHolder{
		private static AlipayManager INSTANCE = new AlipayManager();
	} 
	
	/**
	 * 获取实例对象
	 * @return
	 */
	public static AlipayManager getInstance(){
		if(null == mInstance){
			mInstance = InstanceHolder.INSTANCE;
		}
		return mInstance;
	}
	
	/**
	 * 是有的构造函数
	 */
	private AlipayManager(){
		mAlipayConfig = new AlipayConfig();
		try {
			String rootPath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") ;
			logger.log(Level.DEBUG, "rootPath="+rootPath);
			//私钥
			String privateKey = AlipayRSAUtil.getPrivateKey(rootPath+mAlipayConfig.getAppPrivateKey());
			//公钥
			String alipayPulicKey = AlipayRSAUtil.getPublicKey(rootPath+mAlipayConfig.getAlipayPublicKey());
			//加密密钥
			String encryptionKey = mAlipayConfig.getEncryptionKey();
			if(StringUtils.isBlank(encryptionKey)){
				mAlipayClient = new DefaultAlipayClient(mAlipayConfig.getServiceUrl(), mAlipayConfig.getAppId(),
					privateKey, mAlipayConfig.getDataFormat(), mAlipayConfig.getCharset(), alipayPulicKey, 
					mAlipayConfig.getSignType());
			}else{
				mAlipayClient = new DefaultAlipayClient(mAlipayConfig.getServiceUrl(), mAlipayConfig.getAppId(),
						privateKey, mAlipayConfig.getDataFormat(), mAlipayConfig.getCharset(), alipayPulicKey, 
						mAlipayConfig.getSignType(),mAlipayConfig.getEncryptionKey(),mAlipayConfig.getEncryptionType());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 创建支付宝支付订单信息(调用支付宝提供的sdk)
	 * @param orderId 订单id(必填)
	 * @param subject 商品名称(必填)
	 * @param body 商品详情 
	 * @param price 商品金额(必填)
	 * @return
	 */
	public String createAlipayOrderInfo(String orderId,String subject, String body, String price){
		//实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
		AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
		//SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
		AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
		model.setBody(body);
		model.setSubject(subject);
		model.setOutTradeNo(orderId);
		model.setTimeoutExpress(mAlipayConfig.getTimeoutExpress());
		model.setTotalAmount(price);
		model.setProductCode(mAlipayConfig.getProductCode());
		request.setBizModel(model);
		request.setNotifyUrl(mAlipayConfig.getNotifyUrl());
		request.setNeedEncrypt(StringUtils.isBlank(mAlipayConfig.getEncryptionKey()));
		try {
	        //这里和普通的接口调用不同，使用的是sdkExecute
	        AlipayTradeAppPayResponse response = mAlipayClient.sdkExecute(request);
	        System.out.println(response.getBody());//就是orderString 可以直接给客户端请求，无需再做处理。
	        return response.getBody();
	    } catch (AlipayApiException e) {
	        e.printStackTrace();
	    }
		return null;
	}
	
	/**
	 * 支付宝异步通知验证
	 * @param request
	 * @return
	 */
	public boolean verifySyncNotify(HttpServletRequest request){
		Map<String,String> params = parseNotifyParam(request);
		logger.log(Level.DEBUG, "param="+params.toString());
		
		//调用SDK验证签名
		//公钥
		String rootPath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") ;
		try {
			String alipayPulicKey = AlipayRSAUtil.getPublicKey(rootPath+mAlipayConfig.getAlipayPublicKey());
			String charset = mAlipayConfig.getCharset();
			String sigType = mAlipayConfig.getSignType();
			return AlipaySignature.rsaCheckV1(params, alipayPulicKey, charset, sigType);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		} catch (AlipayApiException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * 获取支付宝异步通知参数
	 * @param request
	 * @return
	 */
	public Map<String,String> parseNotifyParam(HttpServletRequest request){
		Map<String,String> params = new HashMap<String,String>();
		//将异步通知中收到的待验证所有参数都存放到map中
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
		    String name = (String) iter.next();
		    String[] values = (String[]) requestParams.get(name);
		    String valueStr = "";
		    for (int i = 0; i < values.length; i++) {
		        valueStr = (i == values.length - 1) ? valueStr + values[i]
		                    : valueStr + values[i] + ",";
		    }
		    //乱码解决，这段代码在出现乱码时使用。
		    //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
		    params.put(name, valueStr);
		}
		return params;
	}
	
	/**
	 * 校验通知中的seller_id 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
	 * @param sellerId
	 * @return
	 */
	public boolean validSellerId(String sellerId){
		return true;
	}
	
	/**
	 * 校验通知中的seller_email 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
	 * @param sellerId
	 * @return
	 */
	public boolean validSellerEmail(String sellerEmail){
		return mAlipayConfig.getSellerEmail().equals(sellerEmail);
	}
	
	/**
	 * 验证app_id是否为该商户本身
	 * @param appId 
	 * @return
	 */
	public boolean validAppId(String appId){
		return mAlipayConfig.getAppId().equals(appId);
	}
}
