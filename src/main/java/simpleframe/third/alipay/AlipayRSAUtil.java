package simpleframe.third.alipay;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * alipay公钥和私钥读取工具
 * @author sunjichang
 * @date 2017年7月17日下午8:42:21
 * @version 1.0
 * @upate
 */
public class AlipayRSAUtil {

	/**
	 * 读取文件内容
	 * @param filename 文件目录
	 * @return
	 * @throws IOException
	 */
	private static String getKey(String filename) throws IOException {
		// Read key from file
		String strKeyPEM = "";
		BufferedReader br = new BufferedReader(new FileReader(filename));
		String line;
		while ((line = br.readLine()) != null) {
			strKeyPEM += line + "\n";
		}
		br.close();
		return strKeyPEM;
	}

	/**
	 * 获取私钥内容
	 * @param filename 文件名
	 * @return 私钥内容
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public static String getPrivateKey(String filename) throws IOException, GeneralSecurityException {
		String privateKeyPEM = getKey(filename);
		//return getPrivateKeyFromString(privateKeyPEM);
		return privateKeyPEM;
	}

	/**
	 * 处理私钥，去掉头部、尾部以及头部空格信息
	 * @param content 私钥内容 
	 * @return 返回去掉头部、尾部以及头部空格信息的私钥
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public static String getPrivateKeyFromString(String content) throws IOException, GeneralSecurityException {
		String privateKeyPEM = content;
		privateKeyPEM = privateKeyPEM.replace("-----BEGIN PRIVATE KEY-----\n", "");
		privateKeyPEM = privateKeyPEM.replace("-----END PRIVATE KEY-----", "");
		return privateKeyPEM;
//		byte[] encoded = Base64.decodeBase64(privateKeyPEM);
//		KeyFactory kf = KeyFactory.getInstance("RSA");
//		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
//		RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keySpec);
//		return privKey;
	}

	/**
	 * 获取公钥
	 * @param filename 公钥存储路径
	 * @return 公钥内容
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public static String getPublicKey(String filename) throws IOException, GeneralSecurityException {
		String publicKeyPEM = getKey(filename);
		//return getPublicKeyFromString(publicKeyPEM);
		return publicKeyPEM;
	}

	/**
	 * 处理公钥，去掉头部、尾部以及头部空格信息
	 * @param content 公钥内容 
	 * @return 返回去掉头部、尾部以及头部空格信息的公钥
	 * @throws IOException
	 * @throws GeneralSecurityException
	 */
	public static String getPublicKeyFromString(String content) throws IOException, GeneralSecurityException {
		String publicKeyPEM = content;
		publicKeyPEM = publicKeyPEM.replace("-----BEGIN PUBLIC KEY-----\n", "");
		publicKeyPEM = publicKeyPEM.replace("-----END PUBLIC KEY-----", "");
		return publicKeyPEM;
//		byte[] encoded = Base64.decodeBase64(publicKeyPEM);
//		KeyFactory kf = KeyFactory.getInstance("RSA");
//		RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(new PKCS8EncodedKeySpec(encoded));
//		return pubKey;
	}
}
