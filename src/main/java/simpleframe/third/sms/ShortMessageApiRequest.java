package simpleframe.third.sms;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import simpleframe.config.PropertyPlaceholder;
import simpleframe.core.restful.RESTfulRequest;
import simpleframe.dao.ShortMessageDaoImpl;
import simpleframe.utils.StringUtils;

public class ShortMessageApiRequest<T> extends RESTfulRequest<T>{
	
	private static final Logger logger = Logger.getLogger(ShortMessageApiRequest.class);
	
	public ShortMessageApiRequest(String url, Method method,Class<T> cls) {
		super(url, method,cls);
	}
	
	/**
	 * 获取json格式化的参数
	 * @return
	 */
	public Object getJsonParams(){
		return null;
	}

	@Override
	public Object getBody() {
		Map<String,String> map = getParams();
		if(map == null){
			return getJsonParams();
		}
		Iterator<String> i = map.keySet().iterator();
		ObjectNode node = JsonNodeFactory.instance.objectNode();
		while(i.hasNext()){
			String key = i.next();
			node.put(key, map.get(key));
		}
		return node;
	}

	/* (non-Javadoc)
	 * @see com.sjc.connection.core.restful.RESTfulRequest#getHeaders()
	 */
	@Override
	public Map<String, String> getHeaders() {
		HashMap<String, String> header = new HashMap<String,String>();
		header.put("Accept", "application/json");
		header.put("Content-Type", "application/json;charset=utf-8;");
		return header;
	}

	/* (non-Javadoc)
	 * @see simpleframe.core.restful.RESTfulRequest#parseResponse(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T parseResponse(String content) {
		if(StringUtils.isBlank(content)){
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		try {
			return (T)mapper.readValue(content, getParseClass());
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
