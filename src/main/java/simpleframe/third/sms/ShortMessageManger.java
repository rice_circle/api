package simpleframe.third.sms;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import simpleframe.core.restful.RESTfulManger;
import simpleframe.core.restful.RESTfulRequest;
import simpleframe.core.restful.RESTfulResponse;
import simpleframe.utils.DateUtils;
import simpleframe.utils.DigestUtils;


/**
 * 调用第三方平台发送验证码
 * @author sunjichang
 * @date 2017年7月13日下午10:36:12
 * @version 1.0
 * @upate 
 */
public class ShortMessageManger {
	private static final Logger logger = Logger.getLogger(ShortMessageManger.class);
	private static ShortMessageManger instance = null;
	//基本地址
	private final String BASE_URL = "https://app.cloopen.com:8883/";
	//短信发送地址
	private final String SMS_URL = "2013-12-26/Accounts/%s/SMS/TemplateSMS?sig=%S";
	//account_sid
	private static final String ACCOUNT_SID = "8a216da85bf14b6a015bff36d51d058b";
	//auth token
	private static final String AUTH_TOKEN = "a24dcbf3a0dd4a648cb06871602b06ce";
	//app id
	private static final String APP_ID = "8a216da85bf14b6a015bff36d5bc058f";
	//短信模版id
	private static final String TEMPLATE_ID= "176875";
	private static class InstanceHolder {
		public static final ShortMessageManger INSTANCE = new ShortMessageManger();
	}
	
	private ShortMessageManger(){
		
	}
	
	public static ShortMessageManger getInstance(){
		instance = InstanceHolder.INSTANCE;
		return instance;
	}
	
	/**
	 * 获取验证码
	 * @param to 手机号，多个用逗号隔开
	 * @param code 验证码
	 * @return
	 */
	public RESTfulResponse<ShortMessageThirdModell> sendVerifyCode(final String to,final String code){
		//获取时间戳
		final String timestamp = DateUtils.dateConverterStringByFormat("yyyyMMddHHmmss");//"20170715214847";//
		//生成签名串
		StringBuffer sb = new StringBuffer();
		sb.append(ACCOUNT_SID).append(AUTH_TOKEN).append(timestamp);
		String sigParameter = DigestUtils.signMD5(sb.toString(),"UTF-8").toUpperCase();
		logger.log(Level.DEBUG, "sigParameter="+sigParameter);
		StringBuffer urlSB = new StringBuffer();
		urlSB.append(BASE_URL).append(SMS_URL);
		String url = String.format(urlSB.toString(), ACCOUNT_SID,sigParameter);
		logger.log(Level.DEBUG, "url="+url);
		ShortMessageApiRequest<ShortMessageThirdModell> request = new ShortMessageApiRequest<ShortMessageThirdModell>(url, 
				RESTfulRequest.Method.POST,ShortMessageThirdModell.class){
			
			/* (non-Javadoc)
			 * 
			 */
			@Override
			public String getJsonParams() {
				JsonObject object = new JsonObject();
				//短信接收端手机号码集合，用英文逗号分开，每批发送的手机号数量不得超过200个
				object.addProperty("to", to);
				//应用Id
				object.addProperty("appId", APP_ID);
				//短信模版id
				object.addProperty("templateId", TEMPLATE_ID);
				JsonArray arr = new JsonArray();
				arr.add(code);
				object.add("datas", arr);
				logger.log(Level.DEBUG, code);
				return object.toString();
			}

			/* (non-Javadoc)
			 * 
			 */
			@Override
			public Map<String, String> getHeaders() {
				Map<String, String> header = super.getHeaders();
				//生成签名串
				StringBuffer sb = new StringBuffer();
				sb.append(ACCOUNT_SID).append(":").append(timestamp);
				try {
					BASE64Encoder encoder = new BASE64Encoder();
					String auth = encoder.encode(sb.toString().getBytes("utf-8"));
					header.put("Authorization", auth);
					logger.log(Level.DEBUG, "orgin="+sb.toString()+":auth="+auth);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return header;
			}
			
		};
		return RESTfulManger.getInstance().sendRequest(request);
	}
}
