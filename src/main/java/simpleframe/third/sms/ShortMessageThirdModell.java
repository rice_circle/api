package simpleframe.third.sms;

/**
 * 用来解析调用第三方短信发送平台返回结果的model
 * 
 * @author sunjichang
 * @date 2017年7月13日下午9:04:00
 * @version 1.0
 * @upate
 */
public class ShortMessageThirdModell {
	/**
	 * 成功code码
	 */
	public static final String STATUS_SUCCESS = "000000";
	
	/**
	 * 状态码"000000"表示请求发送成功。不是"000000"，表示请求发送失败
	 */
	private String statusCode;

	/**
	 * 返回信息
	 */
	private TemplateSMS templateSMS;

	/**
	 * 状态信息
	 */
	private String statusMsg;

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the statusMsg
	 */
	public String getStatusMsg() {
		return statusMsg;
	}

	/**
	 * @param statusMsg
	 *            the statusMsg to set
	 */
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	/**
	 * @return the templateSMS
	 */
	public TemplateSMS getTemplateSMS() {
		return templateSMS;
	}

	/**
	 * @param templateSMS
	 *            the templateSMS to set
	 */
	public void setTemplateSMS(TemplateSMS templateSMS) {
		this.templateSMS = templateSMS;
	}

	/**
	 * 短信模版返回信息
	 * 
	 * @author sunjichang
	 * @date 2017年7月16日上午10:31:39
	 * @version 1.0
	 * @upate
	 */
	public static class TemplateSMS {
		/**
		 * 验证码
		 */
		private String smsMessageSid;
		/**
		 * 短信创建时间
		 */
		private String dateCreated;

		/**
		 * @return the smsMessageSid
		 */
		public String getSmsMessageSid() {
			return smsMessageSid;
		}

		/**
		 * @param smsMessageSid
		 *            the smsMessageSid to set
		 */
		public void setSmsMessageSid(String smsMessageSid) {
			this.smsMessageSid = smsMessageSid;
		}

		/**
		 * @return the dateCreated
		 */
		public String getDateCreated() {
			return dateCreated;
		}

		/**
		 * @param dateCreated
		 *            the dateCreated to set
		 */
		public void setDateCreated(String dateCreated) {
			this.dateCreated = dateCreated;
		}
	}
}
