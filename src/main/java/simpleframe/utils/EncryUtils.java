package simpleframe.utils;

import org.apache.commons.lang3.StringUtils;

import java.security.MessageDigest;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class EncryUtils {

    /**
     * 生成RSAUtil签名
     */
    public static String handleRSA(TreeMap<String, Object> map, String privateKey) {
        StringBuffer sbuffer = new StringBuffer();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            sbuffer.append(entry.getValue());
        }
        String signTemp = sbuffer.toString();

        String sign = "";
        if (StringUtils.isNotEmpty(privateKey)) {
            sign = EncryRSAUtils.sign(signTemp, privateKey);
        }
        return sign;
    }

    /**
     * 对结果进行验签
     *
     * @param data        业务数据密文
     * @param encrypt_key aesKey加密后的密文
     * @param publickKey  公钥
     * @param privateKey  商户自己的私钥
     * @return 验签是否通过
     * @throws Exception
     */
    public static boolean checkDecryptAndSign(String data, String encrypt_key,
                                              String publickKey, String privateKey) throws Exception {

        /** 1.使用privatekey解开aesEncrypt。 */
        String EncryAESUtilsKey = "";
        try {
            EncryAESUtilsKey = EncryRSAUtils.decrypt(encrypt_key, privateKey);
        } catch (Exception e) {
            /** EncryAESUtils密钥解密失败 */
            e.printStackTrace();
            return false;
        }

        /** 2.用aeskey解开data。取得data明文 */
        String realData = EncryAESUtils.decryptFromBase64(data, EncryAESUtilsKey);

        TreeMap<String, String> map = JsonUtils.toObject(realData, new TreeMap<String, String>().getClass());

        /** 3.取得data明文sign。 */
        String sign = StringUtils.trimToEmpty(map.get("sign"));

        /** 4.对map中的值进行验证 */
        StringBuffer signData = new StringBuffer();
        Iterator<Entry<String, String>> iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Entry<String, String> entry = iter.next();

            /** 把sign参数隔过去 */
            if (StringUtils.equals((String) entry.getKey(), "sign")) {
                continue;
            }
            signData.append(entry.getValue() == null ? "" : entry.getValue());
        }

        /** 5. result为true时表明验签通过 */
        //String str = '"'+"CUSTOMER"+'"'+"1004002057817701200070NEWTZTBINDPAY160422_141536221204573TO_VALIDATE";
        System.out.println("signData.toString()===" + signData.toString());
        System.out.println("sign===" + sign);
        System.out.println("yibaoPublickKey===" + publickKey);
        boolean result = EncryRSAUtils.checkSign(signData.toString(), sign,
                publickKey);

        return result;
    }

    /**
     * 生成hmac
     */
    public static String handleHmac(TreeMap<String, String> map, String hmacKey) {
        StringBuffer sbuffer = new StringBuffer();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            sbuffer.append(entry.getValue());
        }
        String hmacTemp = sbuffer.toString();

        String hmac = "";
        if (StringUtils.isNotEmpty(hmacKey)) {
            hmac = DigestUtils.hmacSHASign(hmacTemp, hmacKey, DigestUtils.ENCODE);
        }
        return hmac;
    }


    // 对字符串进行md5加密
    public static String md5(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());

            byte[] b = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < b.length; i++) {
                int v = (int) b[i];
                v = v < 0 ? 0x100 + v : v;
                String cc = Integer.toHexString(v);
                if (cc.length() == 1) {
                    sb.append('0');
                }
                sb.append(cc);
            }

            return sb.toString();
        } catch (Exception e) {
        }
        return "";
    }

    // 对字符串进行sha256加密
    public static String sha256(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(str.getBytes());

            byte[] b = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < b.length; i++) {
                int v = (int) b[i];
                v = v < 0 ? 0x100 + v : v;
                String cc = Integer.toHexString(v);
                if (cc.length() == 1)
                    sb.append('0');
                sb.append(cc);
            }

            return sb.toString();
        } catch (Exception e) {
        }
        return "";
    }

    // 对字符串进行sha1加密
    public static String sha1(String str) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(str.getBytes());

            byte[] b = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < b.length; i++) {
                int v = (int) b[i];
                v = v < 0 ? 0x100 + v : v;
                String cc = Integer.toHexString(v);
                if (cc.length() == 1)
                    sb.append('0');
                sb.append(cc);
            }

            return sb.toString();
        } catch (Exception e) {
        }
        return "";
    }

}
