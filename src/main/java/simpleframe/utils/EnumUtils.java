package simpleframe.utils;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.reflect.MethodUtils;

public class EnumUtils {
	
	public static interface BaseEnum<E extends Enum<E>,V extends Object> {
		V getValue();
		String getText();
	}

	public static <T extends BaseEnum<?,?>> Map<String, String> parseEnumToMap(Class<T> ref) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		T[] ts = ref.getEnumConstants();
		for (T t : ts) {
			Object text = getInvokeValue(t, "getText");
			Enum<?> tempEnum = (Enum<?>) t;
			if (text == null) {
				text = tempEnum.name();
			}
			Object value = getInvokeValue(t, "getValue");

			if (value == null) {
				value=tempEnum.ordinal();
			}

			map.put(value.toString(), text.toString());

		}
		return map;
	}

	static <T> Object getInvokeValue(T t, String methodName) {
		Method method = MethodUtils.getAccessibleMethod(t.getClass(), methodName);
		if (null == method) {
			return null;
		}
		try {
			return method.invoke(t);
		} catch (Exception e) {
			return null;
		}
	}

	public static <T> T getEnumItem(Class<T> ref, Object i) {
		T returnT = null;
		if (ref.isEnum()) {
			T[] ts = ref.getEnumConstants();
			String tempI = (i == null ? "" : i.toString());
			for (T t : ts) {
				Enum<?> tempEnum = (Enum<?>) t;
				String value = getInvokeValue(t, "getValue").toString();
				if (value == null) {
					value = String.valueOf(tempEnum.ordinal());
				}
				if (tempI.equals(value)) {
					returnT = t;
					break;
				}
			}
		}
		return returnT;
	}
}