package simpleframe.utils;

import java.io.File;

public class FileUtils {

	public static String getSyncPath(String dirName) {
		String filePath = FileUtils.class.getClassLoader().getResource("").getPath();
		File file = new File(filePath);
		filePath = file.getParentFile().getParentFile().getPath();
		filePath = filePath + "/sync/" + dirName+"/";
		return filePath;
	}

	public static String generateSyncPath(String dirName) {

		// 如果文件不存在则创建
		// 打开文件，写入解密文本
		String filePath = FileUtils.class.getClassLoader().getResource("").getPath();
		File file = new File(filePath);
		filePath = file.getParentFile().getParentFile().getPath();
		filePath = filePath + "/sync/" + dirName;
		file = new File(filePath);
		if (!file.exists()) {
			file.mkdirs();
		}

		return file.getPath()+"/";
	}
}