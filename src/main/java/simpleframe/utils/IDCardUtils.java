package simpleframe.utils;

public class IDCardUtils {

	public static int getSex(String idCard) {
		String lastValue = idCard.substring(idCard.length() - 1, idCard.length()).trim().toLowerCase();
		int sex = 0;
		if (lastValue.equals("x") || lastValue.equals("e")) {
			sex = 1;
		} else {
			sex = Integer.parseInt(lastValue) % 2;
		}
		return sex;
	}
}
