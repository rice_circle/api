package simpleframe.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomUtils {
	
	public static Random random = new Random();

	public static String getRandom(int length) {
		StringBuilder ret = new StringBuilder();
		for (int i = 0; i < length; i++) {
			boolean isChar = (random.nextInt(2) % 2 == 0);// 输出字母还是数字
			if (isChar) { // 字符串
				int choice = random.nextInt(2) % 2 == 0 ? 65 : 97; // 取得大写字母还是小写字母
				ret.append((char) (choice + random.nextInt(26)));
			} else { // 数字
				ret.append(Integer.toString(random.nextInt(10)));
			}
		}
		return ret.toString();
	}
	
	/**
	 * 按照给定的位数，获取纯数字的随机数
	 * @param length 位数
	 * @return
	 */
	public static String obtainPureNumberRandom(int length){
		int result = (int)((Math.random()*9+1)*Math.pow(10, length));
		return String.format("%s", result);
	}
	
	public static String getOrderId()
	{
		Date date = new Date();
		DateFormat format = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String orderId = "SDB_" + format.format(date) + RandomStringUtils.randomAlphanumeric(4);//SDB_yyyyMMddHHmmssSSS_xxxx
		return orderId;
	}


}
