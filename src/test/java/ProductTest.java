/*
 * 天虹商场股份有限公司版权所有.
 */

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import simpleframe.service.ProductService;

/**
 * @author:wuxianbing
 * @date:2017/8/7
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml","classpath:spring-mybatis.xml"})
public class ProductTest {

    private static Logger logger = Logger.getLogger(ProductTest.class);

    @Autowired
    private ProductService productService;

    @Test
    public void lockStock() {
        int productId = 1;
        int num = 1;

       boolean result= productService.deductStock(productId,num);
       logger.info(result?"成功":"失败");

    }
}
